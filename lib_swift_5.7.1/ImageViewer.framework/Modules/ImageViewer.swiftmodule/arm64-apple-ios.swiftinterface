// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.7.1 effective-4.2 (swiftlang-5.7.1.135.3 clang-1400.0.29.51)
// swift-module-flags: -target arm64-apple-ios8.0 -enable-objc-interop -enable-library-evolution -swift-version 4.2 -enforce-exclusivity=checked -Onone -module-name ImageViewer
// swift-module-flags-ignorable: -enable-bare-slash-regex
import AVFoundation
import CoreGraphics
import Foundation
import Swift
import UIKit
import _Concurrency
import _StringProcessing
extension UIKit.UIScreen {
  @_Concurrency.MainActor(unsafe) public class var hasNotch: Swift.Bool {
    get
  }
}
extension UIKit.UIView {
  @_Concurrency.MainActor(unsafe) public var boundsCenter: CoreFoundation.CGPoint {
    get
  }
}
public typealias ImageCompletion = (UIKit.UIImage?) -> Swift.Void
public typealias FetchImageBlock = (@escaping ImageViewer.ImageCompletion) -> Swift.Void
public typealias ItemViewControllerBlock = (_ index: Swift.Int, _ itemCount: Swift.Int, _ fetchImageBlock: (@escaping ImageViewer.ImageCompletion) -> Swift.Void, _ configuration: ImageViewer.GalleryConfiguration, _ isInitialController: Swift.Bool) -> UIKit.UIViewController
public enum GalleryItem {
  case image(fetchImageBlock: ImageViewer.FetchImageBlock)
  case video(fetchPreviewImageBlock: ImageViewer.FetchImageBlock, videoURL: Foundation.URL)
  case custom(fetchImageBlock: ImageViewer.FetchImageBlock, itemViewControllerBlock: ImageViewer.ItemViewControllerBlock)
}
public protocol DisplaceableView {
  var image: UIKit.UIImage? { get }
  var bounds: CoreFoundation.CGRect { get }
  var center: CoreFoundation.CGPoint { get }
  var boundsCenter: CoreFoundation.CGPoint { get }
  var contentMode: UIKit.UIView.ContentMode { get }
  var isHidden: Swift.Bool { get set }
  func convert(_ point: CoreFoundation.CGPoint, to view: UIKit.UIView?) -> CoreFoundation.CGPoint
}
public protocol GalleryDisplacedViewsDataSource : AnyObject {
  func provideDisplacementItem(atIndex index: Swift.Int) -> ImageViewer.DisplaceableView?
}
@objc @_inheritsConvenienceInitializers @_hasMissingDesignatedInitializers @_Concurrency.MainActor(unsafe) open class VideoScrubber : UIKit.UIControl {
  @_Concurrency.MainActor(unsafe) @objc required dynamic public init?(coder aDecoder: Foundation.NSCoder)
  @objc deinit
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func layoutSubviews()
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func observeValue(forKeyPath keyPath: Swift.String?, of object: Any?, change: [Foundation.NSKeyValueChangeKey : Any]?, context: Swift.UnsafeMutableRawPointer?)
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func tintColorDidChange()
}
public protocol ItemView {
  var image: UIKit.UIImage? { get set }
}
@_Concurrency.MainActor(unsafe) open class ItemBaseController<T> : UIKit.UIViewController, ImageViewer.ItemController, UIKit.UIGestureRecognizerDelegate, UIKit.UIScrollViewDelegate where T : UIKit.UIView, T : ImageViewer.ItemView {
  @_Concurrency.MainActor(unsafe) public var itemView: T
  @_Concurrency.MainActor(unsafe) weak public var delegate: ImageViewer.ItemControllerDelegate?
  @_Concurrency.MainActor(unsafe) weak public var displacedViewsDataSource: ImageViewer.GalleryDisplacedViewsDataSource?
  @_Concurrency.MainActor(unsafe) final public let index: Swift.Int
  @_Concurrency.MainActor(unsafe) public var isInitialController: Swift.Bool
  @_Concurrency.MainActor(unsafe) public init(index: Swift.Int, itemCount: Swift.Int, fetchImageBlock: @escaping ImageViewer.FetchImageBlock, configuration: ImageViewer.GalleryConfiguration, isInitialController: Swift.Bool = false)
  @available(*, unavailable)
  @_Concurrency.MainActor(unsafe) @objc required dynamic public init?(coder aDecoder: Foundation.NSCoder)
  @objc deinit
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func viewDidLoad()
  @_Concurrency.MainActor(unsafe) public func fetchImage()
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func viewWillAppear(_ animated: Swift.Bool)
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func viewDidAppear(_ animated: Swift.Bool)
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func viewWillDisappear(_ animated: Swift.Bool)
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func viewDidLayoutSubviews()
  @_Concurrency.MainActor(unsafe) @objc public func viewForZooming(in scrollView: UIKit.UIScrollView) -> UIKit.UIView?
  @_Concurrency.MainActor(unsafe) @objc public func scrollViewDidZoom(_ scrollView: UIKit.UIScrollView)
  @_Concurrency.MainActor(unsafe) public func presentItem(alongsideAnimation: () -> Swift.Void, completion: @escaping () -> Swift.Void)
  @_Concurrency.MainActor(unsafe) public func dismissItem(alongsideAnimation: () -> Swift.Void, completion: @escaping () -> Swift.Void)
  @_Concurrency.MainActor(unsafe) @objc public func gestureRecognizerShouldBegin(_ gestureRecognizer: UIKit.UIGestureRecognizer) -> Swift.Bool
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func observeValue(forKeyPath keyPath: Swift.String?, of object: Any?, change: [Foundation.NSKeyValueChangeKey : Any]?, context: Swift.UnsafeMutableRawPointer?)
  @_Concurrency.MainActor(unsafe) public func closeDecorationViews(_ duration: Foundation.TimeInterval)
}
@objc @_Concurrency.MainActor(unsafe) open class GalleryViewController : UIKit.UIPageViewController, ImageViewer.ItemControllerDelegate {
  @_Concurrency.MainActor(unsafe) open var headerView: UIKit.UIView?
  @_Concurrency.MainActor(unsafe) open var footerView: UIKit.UIView?
  @_Concurrency.MainActor(unsafe) public var currentIndex: Swift.Int
  @_Concurrency.MainActor(unsafe) open var launchedCompletion: (() -> Swift.Void)?
  @_Concurrency.MainActor(unsafe) open var landedPageAtIndexCompletion: ((Swift.Int) -> Swift.Void)?
  @_Concurrency.MainActor(unsafe) open var closedCompletion: (() -> Swift.Void)?
  @_Concurrency.MainActor(unsafe) open var programmaticallyClosedCompletion: (() -> Swift.Void)?
  @_Concurrency.MainActor(unsafe) open var swipedToDismissCompletion: (() -> Swift.Void)?
  @available(*, unavailable)
  @_Concurrency.MainActor(unsafe) @objc required dynamic public init?(coder: Foundation.NSCoder)
  @_Concurrency.MainActor(unsafe) public init(startIndex: Swift.Int, itemsDataSource: ImageViewer.GalleryItemsDataSource, itemsDelegate: ImageViewer.GalleryItemsDelegate? = nil, displacedViewsDataSource: ImageViewer.GalleryDisplacedViewsDataSource? = nil, configuration: ImageViewer.GalleryConfiguration = [])
  @objc deinit
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func viewDidLoad()
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func viewDidAppear(_ animated: Swift.Bool)
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func viewDidLayoutSubviews()
  @_Concurrency.MainActor(unsafe) open func page(toIndex index: Swift.Int)
  @_Concurrency.MainActor(unsafe) open func reload(atIndex index: Swift.Int)
  @_Concurrency.MainActor(unsafe) open func close()
  @_Concurrency.MainActor(unsafe) public func itemControllerWillAppear(_ controller: ImageViewer.ItemController)
  @_Concurrency.MainActor(unsafe) public func itemControllerWillDisappear(_ controller: ImageViewer.ItemController)
  @_Concurrency.MainActor(unsafe) public func itemControllerDidAppear(_ controller: ImageViewer.ItemController)
  @_Concurrency.MainActor(unsafe) open func itemControllerDidSingleTap(_ controller: ImageViewer.ItemController)
  @_Concurrency.MainActor(unsafe) open func itemControllerDidLongPress(_ controller: ImageViewer.ItemController, in item: ImageViewer.ItemView)
  @_Concurrency.MainActor(unsafe) public func itemController(_ controller: ImageViewer.ItemController, didSwipeToDismissWithDistanceToEdge distance: CoreFoundation.CGFloat)
  @_Concurrency.MainActor(unsafe) public func itemControllerDidFinishSwipeToDismissSuccessfully()
}
public protocol GalleryItemsDataSource : AnyObject {
  func itemCount() -> Swift.Int
  func provideGalleryItem(_ index: Swift.Int) -> ImageViewer.GalleryItem
}
extension UIKit.UIColor {
  open func shadeDarker() -> UIKit.UIColor
}
public typealias GalleryConfiguration = [ImageViewer.GalleryConfigurationItem]
public enum GalleryConfigurationItem {
  case pagingMode(ImageViewer.GalleryPagingMode)
  case imageDividerWidth(CoreFoundation.CGFloat)
  case closeButtonMode(ImageViewer.ButtonMode)
  case seeAllCloseButtonMode(ImageViewer.ButtonMode)
  case thumbnailsButtonMode(ImageViewer.ButtonMode)
  case deleteButtonMode(ImageViewer.ButtonMode)
  case shareButtonMode(ImageViewer.ButtonMode)
  case closeLayout(ImageViewer.ButtonLayout)
  case seeAllCloseLayout(ImageViewer.ButtonLayout)
  case thumbnailsLayout(ImageViewer.ButtonLayout)
  case deleteLayout(ImageViewer.ButtonLayout)
  case spinnerStyle(UIKit.UIActivityIndicatorView.Style)
  case spinnerColor(UIKit.UIColor)
  case headerViewLayout(ImageViewer.HeaderLayout)
  case footerViewLayout(ImageViewer.FooterLayout)
  case statusBarHidden(Swift.Bool)
  case hideDecorationViewsOnLaunch(Swift.Bool)
  case toggleDecorationViewsBySingleTap(Swift.Bool)
  case activityViewByLongPress(Swift.Bool)
  case presentationStyle(ImageViewer.GalleryPresentationStyle)
  case maximumZoomScale(CoreFoundation.CGFloat)
  case doubleTapToZoomDuration(Foundation.TimeInterval)
  case blurPresentDuration(Foundation.TimeInterval)
  case blurPresentDelay(Foundation.TimeInterval)
  case colorPresentDuration(Foundation.TimeInterval)
  case colorPresentDelay(Foundation.TimeInterval)
  case decorationViewsPresentDelay(Foundation.TimeInterval)
  case blurDismissDuration(Foundation.TimeInterval)
  case blurDismissDelay(Foundation.TimeInterval)
  case colorDismissDuration(Foundation.TimeInterval)
  case colorDismissDelay(Foundation.TimeInterval)
  case itemFadeDuration(Foundation.TimeInterval)
  case decorationViewsFadeDuration(Foundation.TimeInterval)
  case rotationDuration(Foundation.TimeInterval)
  case displacementDuration(Foundation.TimeInterval)
  case reverseDisplacementDuration(Foundation.TimeInterval)
  case displacementKeepOriginalInPlace(Swift.Bool)
  case displacementTimingCurve(UIKit.UIView.AnimationCurve)
  case displacementTransitionStyle(ImageViewer.GalleryDisplacementStyle)
  case displacementInsetMargin(CoreFoundation.CGFloat)
  case overlayColor(UIKit.UIColor)
  case overlayBlurStyle(UIKit.UIBlurEffect.Style)
  case overlayBlurOpacity(CoreFoundation.CGFloat)
  case overlayColorOpacity(CoreFoundation.CGFloat)
  case swipeToDismissThresholdVelocity(CoreFoundation.CGFloat)
  case swipeToDismissMode(ImageViewer.GallerySwipeToDismissMode)
  case rotationMode(ImageViewer.GalleryRotationMode)
  case continuePlayVideoOnEnd(Swift.Bool)
  case videoAutoPlay(Swift.Bool)
  case videoControlsColor(UIKit.UIColor)
}
public enum GalleryRotationMode {
  case applicationBased
  case always
  public static func == (a: ImageViewer.GalleryRotationMode, b: ImageViewer.GalleryRotationMode) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public enum ButtonMode {
  case none
  case builtIn
  case custom(UIKit.UIButton)
}
public enum GalleryPagingMode {
  case standard
  case carousel
  public static func == (a: ImageViewer.GalleryPagingMode, b: ImageViewer.GalleryPagingMode) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public enum GalleryDisplacementStyle {
  case normal
  case springBounce(CoreFoundation.CGFloat)
}
public enum GalleryPresentationStyle {
  case fade
  case displacement
  public static func == (a: ImageViewer.GalleryPresentationStyle, b: ImageViewer.GalleryPresentationStyle) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public struct GallerySwipeToDismissMode : Swift.OptionSet {
  public init(rawValue: Swift.Int)
  public let rawValue: Swift.Int
  public static let never: ImageViewer.GallerySwipeToDismissMode
  public static let horizontal: ImageViewer.GallerySwipeToDismissMode
  public static let vertical: ImageViewer.GallerySwipeToDismissMode
  public static let always: ImageViewer.GallerySwipeToDismissMode
  public typealias ArrayLiteralElement = ImageViewer.GallerySwipeToDismissMode
  public typealias Element = ImageViewer.GallerySwipeToDismissMode
  public typealias RawValue = Swift.Int
}
public protocol ItemControllerDelegate : AnyObject {
  func itemController(_ controller: ImageViewer.ItemController, didSwipeToDismissWithDistanceToEdge distance: CoreFoundation.CGFloat)
  func itemControllerDidFinishSwipeToDismissSuccessfully()
  func itemControllerDidSingleTap(_ controller: ImageViewer.ItemController)
  func itemControllerDidLongPress(_ controller: ImageViewer.ItemController, in item: ImageViewer.ItemView)
  func itemControllerWillAppear(_ controller: ImageViewer.ItemController)
  func itemControllerWillDisappear(_ controller: ImageViewer.ItemController)
  func itemControllerDidAppear(_ controller: ImageViewer.ItemController)
}
extension UIKit.UIViewController {
  @_Concurrency.MainActor(unsafe) public func presentImageGallery(_ gallery: ImageViewer.GalleryViewController, completion: (() -> Swift.Void)? = {})
}
extension UIKit.UIImageView : ImageViewer.ItemView {
}
public protocol ItemController : AnyObject {
  var index: Swift.Int { get }
  var isInitialController: Swift.Bool { get set }
  var delegate: ImageViewer.ItemControllerDelegate? { get set }
  var displacedViewsDataSource: ImageViewer.GalleryDisplacedViewsDataSource? { get set }
  func fetchImage()
  func presentItem(alongsideAnimation: () -> Swift.Void, completion: @escaping () -> Swift.Void)
  func dismissItem(alongsideAnimation: () -> Swift.Void, completion: @escaping () -> Swift.Void)
  func closeDecorationViews(_ duration: Foundation.TimeInterval)
}
public typealias MarginLeft = CoreFoundation.CGFloat
public typealias MarginRight = CoreFoundation.CGFloat
public typealias MarginTop = CoreFoundation.CGFloat
public typealias MarginBottom = CoreFoundation.CGFloat
public enum ButtonLayout {
  case pinLeft(ImageViewer.MarginTop, ImageViewer.MarginLeft)
  case pinRight(ImageViewer.MarginTop, ImageViewer.MarginRight)
}
public enum HeaderLayout {
  case pinLeft(ImageViewer.MarginTop, ImageViewer.MarginLeft)
  case pinRight(ImageViewer.MarginTop, ImageViewer.MarginRight)
  case pinBoth(ImageViewer.MarginTop, ImageViewer.MarginLeft, ImageViewer.MarginRight)
  case center(ImageViewer.MarginTop)
}
public enum FooterLayout {
  case pinLeft(ImageViewer.MarginBottom, ImageViewer.MarginLeft)
  case pinRight(ImageViewer.MarginBottom, ImageViewer.MarginRight)
  case pinBoth(ImageViewer.MarginBottom, ImageViewer.MarginLeft, ImageViewer.MarginRight)
  case center(ImageViewer.MarginBottom)
}
public protocol GalleryItemsDelegate : AnyObject {
  func removeGalleryItem(at index: Swift.Int)
  func shareGalleryItem(at index: Swift.Int)
}
extension ImageViewer.GalleryRotationMode : Swift.Equatable {}
extension ImageViewer.GalleryRotationMode : Swift.Hashable {}
extension ImageViewer.GalleryPagingMode : Swift.Equatable {}
extension ImageViewer.GalleryPagingMode : Swift.Hashable {}
extension ImageViewer.GalleryPresentationStyle : Swift.Equatable {}
extension ImageViewer.GalleryPresentationStyle : Swift.Hashable {}
