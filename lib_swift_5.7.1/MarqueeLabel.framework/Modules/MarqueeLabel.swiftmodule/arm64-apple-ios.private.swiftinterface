// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.7.1 (swiftlang-5.7.1.135.3 clang-1400.0.29.51)
// swift-module-flags: -target arm64-apple-ios8.0 -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name MarqueeLabel
// swift-module-flags-ignorable: -enable-bare-slash-regex
import QuartzCore
import Swift
import UIKit
import _Concurrency
import _StringProcessing
@objc @_inheritsConvenienceInitializers @IBDesignable @_Concurrency.MainActor(unsafe) open class MarqueeLabel : UIKit.UILabel, QuartzCore.CAAnimationDelegate {
  public enum MarqueeType : Swift.CaseIterable {
    case left
    case leftRight
    case right
    case rightLeft
    case continuous
    case continuousReverse
    public static func == (a: MarqueeLabel.MarqueeLabel.MarqueeType, b: MarqueeLabel.MarqueeLabel.MarqueeType) -> Swift.Bool
    public func hash(into hasher: inout Swift.Hasher)
    public typealias AllCases = [MarqueeLabel.MarqueeLabel.MarqueeType]
    public static var allCases: [MarqueeLabel.MarqueeLabel.MarqueeType] {
      get
    }
    public var hashValue: Swift.Int {
      get
    }
  }
  @_Concurrency.MainActor(unsafe) open var type: MarqueeLabel.MarqueeLabel.MarqueeType {
    get
    set
  }
  @_Concurrency.MainActor(unsafe) open var scrollSequence: [MarqueeLabel.MarqueeStep]?
  @_Concurrency.MainActor(unsafe) open var animationCurve: UIKit.UIView.AnimationCurve
  @objc @IBInspectable @_Concurrency.MainActor(unsafe) open var labelize: Swift.Bool {
    @objc get
    @objc set
  }
  @objc @IBInspectable @_Concurrency.MainActor(unsafe) open var holdScrolling: Swift.Bool {
    @objc get
    @objc set
  }
  @objc @IBInspectable @_Concurrency.MainActor(unsafe) public var forceScrolling: Swift.Bool {
    @objc get
    @objc set
  }
  @objc @IBInspectable @_Concurrency.MainActor(unsafe) open var tapToScroll: Swift.Bool {
    @objc get
    @objc set
  }
  @_Concurrency.MainActor(unsafe) open var isPaused: Swift.Bool {
    get
  }
  @_Concurrency.MainActor(unsafe) open var awayFromHome: Swift.Bool {
    get
  }
  @_Concurrency.MainActor(unsafe) open var animationPosition: CoreFoundation.CGFloat? {
    get
  }
  public enum SpeedLimit {
    case rate(CoreFoundation.CGFloat)
    case duration(CoreFoundation.CGFloat)
  }
  @_Concurrency.MainActor(unsafe) open var speed: MarqueeLabel.MarqueeLabel.SpeedLimit {
    get
    set
  }
  @objc @available(*, deprecated, message: "Use speed property instead")
  @IBInspectable @_Concurrency.MainActor(unsafe) open var scrollDuration: CoreFoundation.CGFloat {
    @objc get
    @objc set
  }
  @objc @available(*, deprecated, message: "Use speed property instead")
  @IBInspectable @_Concurrency.MainActor(unsafe) open var scrollRate: CoreFoundation.CGFloat {
    @objc get
    @objc set
  }
  @objc @IBInspectable @_Concurrency.MainActor(unsafe) open var leadingBuffer: CoreFoundation.CGFloat {
    @objc get
    @objc set
  }
  @objc @IBInspectable @_Concurrency.MainActor(unsafe) open var trailingBuffer: CoreFoundation.CGFloat {
    @objc get
    @objc set
  }
  @objc @IBInspectable @_Concurrency.MainActor(unsafe) open var fadeLength: CoreFoundation.CGFloat {
    @objc get
    @objc set
  }
  @objc @IBInspectable @_Concurrency.MainActor(unsafe) open var animationDelay: CoreFoundation.CGFloat
  @_Concurrency.MainActor(unsafe) public var animationDuration: CoreFoundation.CGFloat {
    get
  }
  @_Concurrency.MainActor(unsafe) open class func restartLabelsOfController(_ controller: UIKit.UIViewController)
  @_Concurrency.MainActor(unsafe) open class func controllerViewWillAppear(_ controller: UIKit.UIViewController)
  @_Concurrency.MainActor(unsafe) open class func controllerViewDidAppear(_ controller: UIKit.UIViewController)
  @_Concurrency.MainActor(unsafe) open class func controllerLabelsLabelize(_ controller: UIKit.UIViewController)
  @_Concurrency.MainActor(unsafe) open class func controllerLabelsAnimate(_ controller: UIKit.UIViewController)
  @_Concurrency.MainActor(unsafe) public init(frame: CoreFoundation.CGRect, rate: CoreFoundation.CGFloat, fadeLength fade: CoreFoundation.CGFloat)
  @_Concurrency.MainActor(unsafe) public init(frame: CoreFoundation.CGRect, duration: CoreFoundation.CGFloat, fadeLength fade: CoreFoundation.CGFloat)
  @_Concurrency.MainActor(unsafe) @objc required dynamic public init?(coder aDecoder: Foundation.NSCoder)
  @_Concurrency.MainActor(unsafe) @objc convenience override dynamic public init(frame: CoreFoundation.CGRect)
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func awakeFromNib()
  @available(iOS 8.0, *)
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func prepareForInterfaceBuilder()
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func layoutSubviews()
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func willMove(toWindow newWindow: UIKit.UIWindow?)
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func didMoveToWindow()
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func sizeThatFits(_ size: CoreFoundation.CGSize) -> CoreFoundation.CGSize
  @_Concurrency.MainActor(unsafe) open func sizeThatFits(_ size: CoreFoundation.CGSize, withBuffers: Swift.Bool) -> CoreFoundation.CGSize
  @_Concurrency.MainActor(unsafe) open func textLayoutSize() -> CoreFoundation.CGSize
  @_Concurrency.MainActor(unsafe) open func labelShouldScroll() -> Swift.Bool
  @_Concurrency.MainActor(unsafe) @objc public func animationDidStop(_ anim: QuartzCore.CAAnimation, finished flag: Swift.Bool)
  @_Concurrency.MainActor(unsafe) @objc override dynamic open class var layerClass: Swift.AnyClass {
    @objc get
  }
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func draw(_ layer: QuartzCore.CALayer, in ctx: CoreGraphics.CGContext)
  @objc @_Concurrency.MainActor(unsafe) public func restartForViewController(_ notification: Foundation.Notification)
  @objc @_Concurrency.MainActor(unsafe) public func labelizeForController(_ notification: Foundation.Notification)
  @objc @_Concurrency.MainActor(unsafe) public func animateForController(_ notification: Foundation.Notification)
  @_Concurrency.MainActor(unsafe) public func triggerScrollStart()
  @objc @_Concurrency.MainActor(unsafe) public func restartLabel()
  @available(*, deprecated, message: "Use the shutdownLabel function instead")
  @_Concurrency.MainActor(unsafe) public func resetLabel()
  @objc @_Concurrency.MainActor(unsafe) public func shutdownLabel()
  @_Concurrency.MainActor(unsafe) public func pauseLabel()
  @_Concurrency.MainActor(unsafe) public func unpauseLabel()
  @objc @_Concurrency.MainActor(unsafe) public func labelWasTapped(_ recognizer: UIKit.UIGestureRecognizer)
  @_Concurrency.MainActor(unsafe) open func textCoordinateForFramePoint(_ point: CoreFoundation.CGPoint) -> CoreFoundation.CGPoint?
  @_Concurrency.MainActor(unsafe) open func labelWillBeginScroll()
  @_Concurrency.MainActor(unsafe) open func labelReturnedToHome(_ finished: Swift.Bool)
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func forBaselineLayout() -> UIKit.UIView
  @_Concurrency.MainActor(unsafe) @objc override dynamic open var forLastBaselineLayout: UIKit.UIView {
    @objc get
  }
  @_Concurrency.MainActor(unsafe) @objc override dynamic open var text: Swift.String? {
    @objc get
    @objc set
  }
  @_Concurrency.MainActor(unsafe) @objc override dynamic open var attributedText: Foundation.NSAttributedString? {
    @objc get
    @objc set
  }
  @_Concurrency.MainActor(unsafe) @objc override dynamic open var font: UIKit.UIFont! {
    @objc get
    @objc set
  }
  @_Concurrency.MainActor(unsafe) @objc override dynamic open var textColor: UIKit.UIColor! {
    @objc get
    @objc set
  }
  @_Concurrency.MainActor(unsafe) @objc override dynamic open var backgroundColor: UIKit.UIColor? {
    @objc get
    @objc set
  }
  @_Concurrency.MainActor(unsafe) @objc override dynamic open var shadowColor: UIKit.UIColor? {
    @objc get
    @objc set
  }
  @_Concurrency.MainActor(unsafe) @objc override dynamic open var shadowOffset: CoreFoundation.CGSize {
    @objc get
    @objc set
  }
  @_Concurrency.MainActor(unsafe) @objc override dynamic open var highlightedTextColor: UIKit.UIColor? {
    @objc get
    @objc set
  }
  @_Concurrency.MainActor(unsafe) @objc override dynamic open var isHighlighted: Swift.Bool {
    @objc get
    @objc set
  }
  @_Concurrency.MainActor(unsafe) @objc override dynamic open var isEnabled: Swift.Bool {
    @objc get
    @objc set
  }
  @_Concurrency.MainActor(unsafe) @objc override dynamic open var numberOfLines: Swift.Int {
    @objc get
    @objc set
  }
  @_Concurrency.MainActor(unsafe) @objc override dynamic open var baselineAdjustment: UIKit.UIBaselineAdjustment {
    @objc get
    @objc set
  }
  @_Concurrency.MainActor(unsafe) @objc override dynamic open var intrinsicContentSize: CoreFoundation.CGSize {
    @objc get
  }
  @_Concurrency.MainActor(unsafe) @objc override dynamic open var tintColor: UIKit.UIColor! {
    @objc get
    @objc set
  }
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func tintColorDidChange()
  @_Concurrency.MainActor(unsafe) @objc override dynamic open var contentMode: UIKit.UIView.ContentMode {
    @objc get
    @objc set
  }
  @_Concurrency.MainActor(unsafe) @objc override dynamic open var isAccessibilityElement: Swift.Bool {
    @objc get
    @objc set
  }
  @objc deinit
}
public protocol MarqueeStep {
  var timeStep: CoreFoundation.CGFloat { get }
  var timingFunction: UIKit.UIView.AnimationCurve { get }
  var edgeFades: MarqueeLabel.EdgeFade { get }
}
public struct ScrollStep : MarqueeLabel.MarqueeStep {
  public enum Position {
    case home
    case away
    case partial(CoreFoundation.CGFloat)
  }
  public let timeStep: CoreFoundation.CGFloat
  public let timingFunction: UIKit.UIView.AnimationCurve
  public let position: MarqueeLabel.ScrollStep.Position
  public let edgeFades: MarqueeLabel.EdgeFade
  public init(timeStep: CoreFoundation.CGFloat, timingFunction: UIKit.UIView.AnimationCurve = .linear, position: MarqueeLabel.ScrollStep.Position, edgeFades: MarqueeLabel.EdgeFade)
}
public struct FadeStep : MarqueeLabel.MarqueeStep {
  public let timeStep: CoreFoundation.CGFloat
  public let timingFunction: UIKit.UIView.AnimationCurve
  public let edgeFades: MarqueeLabel.EdgeFade
  public init(timeStep: CoreFoundation.CGFloat, timingFunction: UIKit.UIView.AnimationCurve = .linear, edgeFades: MarqueeLabel.EdgeFade)
}
public struct EdgeFade : Swift.OptionSet {
  public let rawValue: Swift.Int
  public static let leading: MarqueeLabel.EdgeFade
  public static let trailing: MarqueeLabel.EdgeFade
  public init(rawValue: Swift.Int)
  public typealias ArrayLiteralElement = MarqueeLabel.EdgeFade
  public typealias Element = MarqueeLabel.EdgeFade
  public typealias RawValue = Swift.Int
}
extension MarqueeLabel.MarqueeLabel.MarqueeType : Swift.Equatable {}
extension MarqueeLabel.MarqueeLabel.MarqueeType : Swift.Hashable {}
