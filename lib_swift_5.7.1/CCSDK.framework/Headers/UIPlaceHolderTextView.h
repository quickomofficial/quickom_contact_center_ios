//
//  UIPlaceHolderTextView.h
//  VoxyPAD
//
//  Created by Keeley Chen on 9/8/14.
//
//

#import <UIKit/UIKit.h>

@protocol UIPlaceHolderTextViewDelegate <NSObject>

- (void) imagePasted:(UIImage *) image;

@end

@interface UIPlaceHolderTextView : UITextView

@property (nonatomic, weak) id<UIPlaceHolderTextViewDelegate> phtDelegate;
@property (nonatomic, retain) NSString *placeholder;
@property (nonatomic, retain) UIColor *placeholderColor;

@property (nonatomic, assign) UIResponder *overrideNextResponder;

-(void)textChanged:(NSNotification*)notification;

- (void) showPlaceHolder;
- (void) hidePlaceHolder;

@end
