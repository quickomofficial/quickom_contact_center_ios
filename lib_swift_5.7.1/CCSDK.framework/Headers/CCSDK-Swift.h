#if 0
#elif defined(__arm64__) && __arm64__
// Generated by Apple Swift version 5.7.1 (swiftlang-5.7.1.135.3 clang-1400.0.29.51)
#ifndef CCSDK_SWIFT_H
#define CCSDK_SWIFT_H
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wgcc-compat"

#if !defined(__has_include)
# define __has_include(x) 0
#endif
#if !defined(__has_attribute)
# define __has_attribute(x) 0
#endif
#if !defined(__has_feature)
# define __has_feature(x) 0
#endif
#if !defined(__has_warning)
# define __has_warning(x) 0
#endif

#if __has_include(<swift/objc-prologue.h>)
# include <swift/objc-prologue.h>
#endif

#pragma clang diagnostic ignored "-Wduplicate-method-match"
#pragma clang diagnostic ignored "-Wauto-import"
#if defined(__OBJC__)
#include <Foundation/Foundation.h>
#endif
#if defined(__cplusplus)
#include <cstdint>
#include <cstddef>
#include <cstdbool>
#else
#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
#endif

#if !defined(SWIFT_TYPEDEFS)
# define SWIFT_TYPEDEFS 1
# if __has_include(<uchar.h>)
#  include <uchar.h>
# elif !defined(__cplusplus)
typedef uint_least16_t char16_t;
typedef uint_least32_t char32_t;
# endif
typedef float swift_float2  __attribute__((__ext_vector_type__(2)));
typedef float swift_float3  __attribute__((__ext_vector_type__(3)));
typedef float swift_float4  __attribute__((__ext_vector_type__(4)));
typedef double swift_double2  __attribute__((__ext_vector_type__(2)));
typedef double swift_double3  __attribute__((__ext_vector_type__(3)));
typedef double swift_double4  __attribute__((__ext_vector_type__(4)));
typedef int swift_int2  __attribute__((__ext_vector_type__(2)));
typedef int swift_int3  __attribute__((__ext_vector_type__(3)));
typedef int swift_int4  __attribute__((__ext_vector_type__(4)));
typedef unsigned int swift_uint2  __attribute__((__ext_vector_type__(2)));
typedef unsigned int swift_uint3  __attribute__((__ext_vector_type__(3)));
typedef unsigned int swift_uint4  __attribute__((__ext_vector_type__(4)));
#endif

#if !defined(SWIFT_PASTE)
# define SWIFT_PASTE_HELPER(x, y) x##y
# define SWIFT_PASTE(x, y) SWIFT_PASTE_HELPER(x, y)
#endif
#if !defined(SWIFT_METATYPE)
# define SWIFT_METATYPE(X) Class
#endif
#if !defined(SWIFT_CLASS_PROPERTY)
# if __has_feature(objc_class_property)
#  define SWIFT_CLASS_PROPERTY(...) __VA_ARGS__
# else
#  define SWIFT_CLASS_PROPERTY(...)
# endif
#endif

#if __has_attribute(objc_runtime_name)
# define SWIFT_RUNTIME_NAME(X) __attribute__((objc_runtime_name(X)))
#else
# define SWIFT_RUNTIME_NAME(X)
#endif
#if __has_attribute(swift_name)
# define SWIFT_COMPILE_NAME(X) __attribute__((swift_name(X)))
#else
# define SWIFT_COMPILE_NAME(X)
#endif
#if __has_attribute(objc_method_family)
# define SWIFT_METHOD_FAMILY(X) __attribute__((objc_method_family(X)))
#else
# define SWIFT_METHOD_FAMILY(X)
#endif
#if __has_attribute(noescape)
# define SWIFT_NOESCAPE __attribute__((noescape))
#else
# define SWIFT_NOESCAPE
#endif
#if __has_attribute(ns_consumed)
# define SWIFT_RELEASES_ARGUMENT __attribute__((ns_consumed))
#else
# define SWIFT_RELEASES_ARGUMENT
#endif
#if __has_attribute(warn_unused_result)
# define SWIFT_WARN_UNUSED_RESULT __attribute__((warn_unused_result))
#else
# define SWIFT_WARN_UNUSED_RESULT
#endif
#if __has_attribute(noreturn)
# define SWIFT_NORETURN __attribute__((noreturn))
#else
# define SWIFT_NORETURN
#endif
#if !defined(SWIFT_CLASS_EXTRA)
# define SWIFT_CLASS_EXTRA
#endif
#if !defined(SWIFT_PROTOCOL_EXTRA)
# define SWIFT_PROTOCOL_EXTRA
#endif
#if !defined(SWIFT_ENUM_EXTRA)
# define SWIFT_ENUM_EXTRA
#endif
#if !defined(SWIFT_CLASS)
# if __has_attribute(objc_subclassing_restricted)
#  define SWIFT_CLASS(SWIFT_NAME) SWIFT_RUNTIME_NAME(SWIFT_NAME) __attribute__((objc_subclassing_restricted)) SWIFT_CLASS_EXTRA
#  define SWIFT_CLASS_NAMED(SWIFT_NAME) __attribute__((objc_subclassing_restricted)) SWIFT_COMPILE_NAME(SWIFT_NAME) SWIFT_CLASS_EXTRA
# else
#  define SWIFT_CLASS(SWIFT_NAME) SWIFT_RUNTIME_NAME(SWIFT_NAME) SWIFT_CLASS_EXTRA
#  define SWIFT_CLASS_NAMED(SWIFT_NAME) SWIFT_COMPILE_NAME(SWIFT_NAME) SWIFT_CLASS_EXTRA
# endif
#endif
#if !defined(SWIFT_RESILIENT_CLASS)
# if __has_attribute(objc_class_stub)
#  define SWIFT_RESILIENT_CLASS(SWIFT_NAME) SWIFT_CLASS(SWIFT_NAME) __attribute__((objc_class_stub))
#  define SWIFT_RESILIENT_CLASS_NAMED(SWIFT_NAME) __attribute__((objc_class_stub)) SWIFT_CLASS_NAMED(SWIFT_NAME)
# else
#  define SWIFT_RESILIENT_CLASS(SWIFT_NAME) SWIFT_CLASS(SWIFT_NAME)
#  define SWIFT_RESILIENT_CLASS_NAMED(SWIFT_NAME) SWIFT_CLASS_NAMED(SWIFT_NAME)
# endif
#endif

#if !defined(SWIFT_PROTOCOL)
# define SWIFT_PROTOCOL(SWIFT_NAME) SWIFT_RUNTIME_NAME(SWIFT_NAME) SWIFT_PROTOCOL_EXTRA
# define SWIFT_PROTOCOL_NAMED(SWIFT_NAME) SWIFT_COMPILE_NAME(SWIFT_NAME) SWIFT_PROTOCOL_EXTRA
#endif

#if !defined(SWIFT_EXTENSION)
# define SWIFT_EXTENSION(M) SWIFT_PASTE(M##_Swift_, __LINE__)
#endif

#if !defined(OBJC_DESIGNATED_INITIALIZER)
# if __has_attribute(objc_designated_initializer)
#  define OBJC_DESIGNATED_INITIALIZER __attribute__((objc_designated_initializer))
# else
#  define OBJC_DESIGNATED_INITIALIZER
# endif
#endif
#if !defined(SWIFT_ENUM_ATTR)
# if defined(__has_attribute) && __has_attribute(enum_extensibility)
#  define SWIFT_ENUM_ATTR(_extensibility) __attribute__((enum_extensibility(_extensibility)))
# else
#  define SWIFT_ENUM_ATTR(_extensibility)
# endif
#endif
#if !defined(SWIFT_ENUM)
# define SWIFT_ENUM(_type, _name, _extensibility) enum _name : _type _name; enum SWIFT_ENUM_ATTR(_extensibility) SWIFT_ENUM_EXTRA _name : _type
# if __has_feature(generalized_swift_name)
#  define SWIFT_ENUM_NAMED(_type, _name, SWIFT_NAME, _extensibility) enum _name : _type _name SWIFT_COMPILE_NAME(SWIFT_NAME); enum SWIFT_COMPILE_NAME(SWIFT_NAME) SWIFT_ENUM_ATTR(_extensibility) SWIFT_ENUM_EXTRA _name : _type
# else
#  define SWIFT_ENUM_NAMED(_type, _name, SWIFT_NAME, _extensibility) SWIFT_ENUM(_type, _name, _extensibility)
# endif
#endif
#if !defined(SWIFT_UNAVAILABLE)
# define SWIFT_UNAVAILABLE __attribute__((unavailable))
#endif
#if !defined(SWIFT_UNAVAILABLE_MSG)
# define SWIFT_UNAVAILABLE_MSG(msg) __attribute__((unavailable(msg)))
#endif
#if !defined(SWIFT_AVAILABILITY)
# define SWIFT_AVAILABILITY(plat, ...) __attribute__((availability(plat, __VA_ARGS__)))
#endif
#if !defined(SWIFT_WEAK_IMPORT)
# define SWIFT_WEAK_IMPORT __attribute__((weak_import))
#endif
#if !defined(SWIFT_DEPRECATED)
# define SWIFT_DEPRECATED __attribute__((deprecated))
#endif
#if !defined(SWIFT_DEPRECATED_MSG)
# define SWIFT_DEPRECATED_MSG(...) __attribute__((deprecated(__VA_ARGS__)))
#endif
#if __has_feature(attribute_diagnose_if_objc)
# define SWIFT_DEPRECATED_OBJC(Msg) __attribute__((diagnose_if(1, Msg, "warning")))
#else
# define SWIFT_DEPRECATED_OBJC(Msg) SWIFT_DEPRECATED_MSG(Msg)
#endif
#if defined(__OBJC__)
#if !defined(IBSegueAction)
# define IBSegueAction
#endif
#endif
#if !defined(SWIFT_EXTERN)
# if defined(__cplusplus)
#  define SWIFT_EXTERN extern "C"
# else
#  define SWIFT_EXTERN extern
# endif
#endif
#if !defined(SWIFT_CALL)
# define SWIFT_CALL __attribute__((swiftcall))
#endif
#if defined(__cplusplus)
#if !defined(SWIFT_NOEXCEPT)
# define SWIFT_NOEXCEPT noexcept
#endif
#else
#if !defined(SWIFT_NOEXCEPT)
# define SWIFT_NOEXCEPT 
#endif
#endif
#if defined(__cplusplus)
#if !defined(SWIFT_CXX_INT_DEFINED)
#define SWIFT_CXX_INT_DEFINED
namespace swift {
using Int = ptrdiff_t;
using UInt = size_t;
}
#endif
#endif
#if defined(__OBJC__)
#if __has_feature(modules)
#if __has_warning("-Watimport-in-framework-header")
#pragma clang diagnostic ignored "-Watimport-in-framework-header"
#endif
@import AVFAudio;
@import BWFCallCenterFramework;
@import CallKit;
@import CoreFoundation;
@import Foundation;
@import ObjectiveC;
@import QuartzCore;
@import UIKit;
#endif

#endif
#pragma clang diagnostic ignored "-Wproperty-attribute-mismatch"
#pragma clang diagnostic ignored "-Wduplicate-method-arg"
#if __has_warning("-Wpragma-clang-attribute")
# pragma clang diagnostic ignored "-Wpragma-clang-attribute"
#endif
#pragma clang diagnostic ignored "-Wunknown-pragmas"
#pragma clang diagnostic ignored "-Wnullability"
#pragma clang diagnostic ignored "-Wdollar-in-identifier-extension"

#if __has_attribute(external_source_symbol)
# pragma push_macro("any")
# undef any
# pragma clang attribute push(__attribute__((external_source_symbol(language="Swift", defined_in="CCSDK",generated_declaration))), apply_to=any(function,enum,objc_interface,objc_category,objc_protocol))
# pragma pop_macro("any")
#endif

#if defined(__OBJC__)
@class NSCoder;

SWIFT_CLASS("_TtC5CCSDK10BaseEntity")
@interface BaseEntity : NSObject
- (nonnull instancetype)init SWIFT_UNAVAILABLE;
+ (nonnull instancetype)new SWIFT_UNAVAILABLE_MSG("-init is unavailable");
- (nullable instancetype)initWithCoder:(NSCoder * _Nonnull)aDecoder OBJC_DESIGNATED_INITIALIZER;
@end


@interface BaseEntity (SWIFT_EXTENSION(CCSDK)) <NSCoding>
- (void)encodeWithCoder:(NSCoder * _Nonnull)aCoder;
@end


SWIFT_CLASS("_TtC5CCSDK11BaseManager")
@interface BaseManager : NSObject
- (nonnull instancetype)init SWIFT_UNAVAILABLE;
+ (nonnull instancetype)new SWIFT_UNAVAILABLE_MSG("-init is unavailable");
- (void)prepare;
- (void)clearData;
@end

@class NSString;
@protocol UIViewControllerTransitionCoordinator;
@class NSBundle;

SWIFT_CLASS("_TtC5CCSDK18BaseViewController")
@interface BaseViewController : UIViewController
@property (nonatomic) BOOL hasSubTitleView;
@property (nonatomic) BOOL hasSubBackButton;
@property (nonatomic, copy) NSString * _Nonnull subTitleText;
@property (nonatomic) BOOL hasNavigationBar;
- (void)viewDidLoad;
@property (nonatomic, readonly) UIStatusBarStyle preferredStatusBarStyle;
- (void)viewWillAppear:(BOOL)animated;
- (void)viewWillDisappear:(BOOL)animated;
- (void)viewDidAppear:(BOOL)animated;
- (void)viewDidDisappear:(BOOL)animated;
@property (nonatomic, readonly) BOOL shouldAutorotate;
@property (nonatomic, readonly) UIInterfaceOrientationMask supportedInterfaceOrientations;
- (void)didReceiveMemoryWarning;
- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id <UIViewControllerTransitionCoordinator> _Nonnull)coordinator;
- (nonnull instancetype)initWithNibName:(NSString * _Nullable)nibNameOrNil bundle:(NSBundle * _Nullable)nibBundleOrNil OBJC_DESIGNATED_INITIALIZER;
- (nullable instancetype)initWithCoder:(NSCoder * _Nonnull)coder OBJC_DESIGNATED_INITIALIZER;
@end


@interface BaseViewController (SWIFT_EXTENSION(CCSDK)) <UIGestureRecognizerDelegate>
@end


@interface CALayer (SWIFT_EXTENSION(CCSDK))
- (void)pause;
- (void)resume;
@end

@class QRCodeEntity;

SWIFT_CLASS("_TtC5CCSDK25CCStartCallViewController")
@interface CCStartCallViewController : BaseViewController
- (void)viewDidLoad;
- (void)viewDidAppear:(BOOL)animated;
@property (nonatomic, readonly) UIInterfaceOrientationMask supportedInterfaceOrientations;
@property (nonatomic, readonly) BOOL shouldAutorotate;
@property (nonatomic, readonly) UIInterfaceOrientation preferredInterfaceOrientationForPresentation;
+ (void)showWithQr:(QRCodeEntity * _Nullable)qr name:(NSString * _Nullable)name userId:(NSString * _Nullable)userId email:(NSString * _Nullable)email phone:(NSString * _Nullable)phone claimCode:(NSString * _Nullable)claimCode;
- (nonnull instancetype)initWithNibName:(NSString * _Nullable)nibNameOrNil bundle:(NSBundle * _Nullable)nibBundleOrNil OBJC_DESIGNATED_INITIALIZER;
- (nullable instancetype)initWithCoder:(NSCoder * _Nonnull)coder OBJC_DESIGNATED_INITIALIZER;
@end



SWIFT_CLASS("_TtC5CCSDK17CallCenterManager")
@interface CallCenterManager : BaseManager
SWIFT_CLASS_PROPERTY(@property (nonatomic, class, readonly, strong) CallCenterManager * _Nonnull shared;)
+ (CallCenterManager * _Nonnull)shared SWIFT_WARN_UNUSED_RESULT;
@property (nonatomic, copy) NSString * _Nullable claimCode;
@property (nonatomic) BOOL isOnCallView;
- (void)clearData;
- (void)prepare;
- (void)startWithToken:(NSString * _Nonnull)token;
- (void)startFrameworkTimer;
@end

@class AVAudioPlayer;

@interface CallCenterManager (SWIFT_EXTENSION(CCSDK)) <AVAudioPlayerDelegate>
- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer * _Nonnull)player successfully:(BOOL)flag;
@end



@interface CallCenterManager (SWIFT_EXTENSION(CCSDK))
- (NSDictionary<NSString *, id> * _Nonnull)parseQRCodeWith:(NSString * _Nonnull)str SWIFT_WARN_UNUSED_RESULT;
@end


@interface CallCenterManager (SWIFT_EXTENSION(CCSDK))
SWIFT_CLASS_PROPERTY(@property (nonatomic, class, readonly, copy) NSString * _Nonnull kNotiWebRtcNewCCReceive_DidMakeCall;)
+ (NSString * _Nonnull)kNotiWebRtcNewCCReceive_DidMakeCall SWIFT_WARN_UNUSED_RESULT;
SWIFT_CLASS_PROPERTY(@property (nonatomic, class, readonly, copy) NSString * _Nonnull kNotiWebRtcNewCCReceive_DidMakeChat;)
+ (NSString * _Nonnull)kNotiWebRtcNewCCReceive_DidMakeChat SWIFT_WARN_UNUSED_RESULT;
@end


@class BWFRequestEntity;
@class NSNumber;

@interface CallCenterManager (SWIFT_EXTENSION(CCSDK)) <BWFCallCenterForSupporterProtocol>
- (void)bwfCallCenterDidReceiveRequest:(BWFRequestEntity * _Nonnull)request;
- (void)bwfCallCenterDidDropRequest:(BWFRequestEntity * _Nonnull)request;
- (void)bwfCallCenterDidForceDropCurrentRequest;
- (void)bwfCallCenterDidUpdateToOnlineStatus:(NSNumber * _Nonnull)isOnline;
@end


@interface CallCenterManager (SWIFT_EXTENSION(CCSDK))
- (void)handlePushkitPayloadWith:(NSString * _Nonnull)dataString;
@end


@interface CallCenterManager (SWIFT_EXTENSION(CCSDK)) <BWFCallCenterForRequesterProtocol>
- (void)requestCallWithQrcodeDict:(NSDictionary<NSString *, id> * _Nonnull)qrcodeDict needPollingAccount:(BOOL)needPollingAccount;
- (void)bwfCallCenterDidStartAsRequesterWithIdentifier:(NSString * _Nullable)identifier;
- (void)bwfCallCenterCallDidChangeToStateOutgoingTo:(NSString * _Nullable)identifier isVideoCall:(BOOL)isVideoCall;
@end

@class BWFCallCenterError;
@class NSDate;

@interface CallCenterManager (SWIFT_EXTENSION(CCSDK)) <BWFCallCenterProtocol>
- (void)bwfCallCenterDidStartAsEnterpriseWithIdentifier:(NSString * _Nullable)identifier;
- (void)bwfCallCenterDidStartAsSupporterWithIdentifier:(NSString * _Nullable)identifier;
- (void)bwfCallCenterDidStop;
- (void)bwfCallCenterDidConnectMessage;
- (void)bwfCallCenterFailedWithError:(BWFCallCenterError * _Nonnull)error;
- (void)bwfCallCenterCallDidChangeToStateConnectedWith:(NSString * _Nullable)identifier isVideoCall:(BOOL)isVideoCall;
- (void)bwfCallCenterCallDidChangeToStateEnd:(NSTimeInterval)totalDuration;
- (void)bwfCallCenterCallDidChangeToStateReceivedIncomingCallFrom:(NSString * _Nullable)identifier isVideoCall:(BOOL)isVideoCall extraInfo:(NSDictionary * _Nullable)extraInfo;
- (void)bwfCallCenterReceiveChatMessage:(NSDictionary * _Nonnull)content conversationId:(NSString * _Nullable)receiveConvId messageId:(NSString * _Nonnull)messageId from:(NSString * _Nonnull)identifier sender:(NSString * _Nullable)senderId date:(NSDate * _Nullable)date metaInfo:(NSDictionary * _Nullable)metaInfo;
- (void)bwfCallCenterReceiveTextMessage:(NSString * _Nonnull)text messageId:(NSString * _Nonnull)messageId from:(NSString * _Nonnull)identifier date:(NSDate * _Nullable)date;
- (void)bwfCallCenterReceiveTextMessage:(NSString * _Nonnull)text conversationId:(NSString * _Nullable)receiveConvId messageId:(NSString * _Nonnull)messageId from:(NSString * _Nonnull)identifier sender:(NSString * _Nullable)senderId date:(NSDate * _Nullable)date metaInfo:(NSDictionary * _Nullable)metaInfo;
- (void)bwfCallCenterReceiveImageMessage:(NSString * _Nonnull)url messageId:(NSString * _Nonnull)messageId from:(NSString * _Nonnull)identifier date:(NSDate * _Nullable)date;
- (void)bwfCallCenterReceiveImageMessage:(NSString * _Nonnull)url conversationId:(NSString * _Nullable)receiveConvId messageId:(NSString * _Nonnull)messageId from:(NSString * _Nonnull)identifier sender:(NSString * _Nullable)senderId date:(NSDate * _Nullable)date metaInfo:(NSDictionary * _Nullable)metaInfo;
- (void)bwfCallCenterReceiveCallNotification:(NSDictionary * _Nonnull)callInfo conversationId:(NSString * _Nullable)receiveConvId messageId:(NSString * _Nonnull)messageId from:(NSString * _Nonnull)identifier sender:(NSString * _Nullable)senderId date:(NSDate * _Nullable)date metaInfo:(NSDictionary * _Nullable)metaInfo;
- (void)bwfCallCenterReceiveStatus:(EnumBWFCallCenterMessageStatus)status forMessageId:(NSString * _Nonnull)msgId from:(NSString * _Nonnull)identifier;
- (void)bwfCallCenterReceiveCustomMessage:(NSDictionary * _Nullable)info;
@end


@interface CallCenterManager (SWIFT_EXTENSION(CCSDK)) <BWFNewCCProtocol>
- (void)bwfNewCC_onMemberEvent:(NSString * _Nonnull)eventName Data:(NSDictionary * _Nullable)info;
- (void)bwfNewCC_onCallStarted;
- (void)bwfNewCC_onVideoEvent:(NSString * _Nonnull)eventName VideoInfo:(NSDictionary * _Nullable)info;
- (void)bwfNewCC_onRoutingMessage:(NSString * _Nonnull)msgName RequestId:(NSString * _Nonnull)requestId RequestInfo:(NSDictionary * _Nullable)info;
- (void)bwfNewCC_onFailedToInvite:(NSString * _Nullable)reason;
- (void)bwfNewCC_onFailedToForward:(NSString * _Nullable)reason;
- (void)bwfNewCC_onAudioRMS:(NSDictionary * _Nonnull)info;
@end

@class CXProvider;
@class CXAnswerCallAction;
@class CXStartCallAction;
@class CXEndCallAction;
@class AVAudioSession;
@class CXSetHeldCallAction;
@class CXSetMutedCallAction;

@interface CallCenterManager (SWIFT_EXTENSION(CCSDK)) <CXProviderDelegate>
- (void)providerDidReset:(CXProvider * _Nonnull)provider;
- (void)provider:(CXProvider * _Nonnull)provider performAnswerCallAction:(CXAnswerCallAction * _Nonnull)action;
- (void)provider:(CXProvider * _Nonnull)provider performStartCallAction:(CXStartCallAction * _Nonnull)action;
- (void)provider:(CXProvider * _Nonnull)provider performEndCallAction:(CXEndCallAction * _Nonnull)action;
- (void)provider:(CXProvider * _Nonnull)provider didActivateAudioSession:(AVAudioSession * _Nonnull)audioSession;
- (void)provider:(CXProvider * _Nonnull)provider performSetHeldCallAction:(CXSetHeldCallAction * _Nonnull)action;
- (void)provider:(CXProvider * _Nonnull)provider performSetMutedCallAction:(CXSetMutedCallAction * _Nonnull)action;
@end

@class UIFont;

SWIFT_CLASS("_TtC5CCSDK8Constant")
@interface Constant : NSObject
SWIFT_CLASS_PROPERTY(@property (nonatomic, class, readonly, copy) NSString * _Nonnull guestApiKey;)
+ (NSString * _Nonnull)guestApiKey SWIFT_WARN_UNUSED_RESULT;
+ (UIFont * _Nonnull)FontSFUIDisplayWith:(CGFloat)size SWIFT_WARN_UNUSED_RESULT;
+ (UIFont * _Nonnull)FontSFUIDisplayBoldWith:(CGFloat)size SWIFT_WARN_UNUSED_RESULT;
+ (UIFont * _Nonnull)FontSFUIDisplaySemiBoldWith:(CGFloat)size SWIFT_WARN_UNUSED_RESULT;
+ (UIFont * _Nonnull)FontSFUIDisplayLightWith:(CGFloat)size SWIFT_WARN_UNUSED_RESULT;
+ (UIFont * _Nonnull)FontSFUIDisplayExtraboldWith:(CGFloat)size SWIFT_WARN_UNUSED_RESULT;
+ (UIFont * _Nonnull)FontSFUIDisplayMediumWith:(CGFloat)size SWIFT_WARN_UNUSED_RESULT;
+ (UIFont * _Nonnull)FontSFUIDisplayThinWith:(CGFloat)size SWIFT_WARN_UNUSED_RESULT;
+ (UIFont * _Nonnull)FontSFUIDisplayUltraLightWith:(CGFloat)size SWIFT_WARN_UNUSED_RESULT;
+ (UIFont * _Nonnull)FontSFUIDisplayBlackWith:(CGFloat)size SWIFT_WARN_UNUSED_RESULT;
SWIFT_CLASS_PROPERTY(@property (nonatomic, class, readonly, copy) NSString * _Nonnull kNotificationReceiveAudioRMS;)
+ (NSString * _Nonnull)kNotificationReceiveAudioRMS SWIFT_WARN_UNUSED_RESULT;
- (nonnull instancetype)init OBJC_DESIGNATED_INITIALIZER;
@end


SWIFT_CLASS("_TtC5CCSDK5Macro")
@interface Macro : NSObject
SWIFT_CLASS_PROPERTY(@property (nonatomic, class, readonly) BOOL hasNotch;)
+ (BOOL)hasNotch SWIFT_WARN_UNUSED_RESULT;
+ (void)dispatchAfterSecond:(NSTimeInterval)time execute:(void (^ _Nonnull)(void))execute;
+ (void)Log:(NSString * _Nonnull)msg;
+ (void)forceLog:(NSString * _Nonnull)msg;
- (nonnull instancetype)init OBJC_DESIGNATED_INITIALIZER;
@end


SWIFT_CLASS("_TtC5CCSDK17ManagementManager")
@interface ManagementManager : BaseManager
SWIFT_CLASS_PROPERTY(@property (nonatomic, class, readonly, strong) ManagementManager * _Nonnull shared;)
+ (ManagementManager * _Nonnull)shared SWIFT_WARN_UNUSED_RESULT;
- (void)clearData;
- (void)prepare;
- (void)preLoadAllMember;
@end




@interface NSDictionary<KeyType, ObjectType> (SWIFT_EXTENSION(CCSDK))
- (NSString * _Nullable)toJsonString SWIFT_WARN_UNUSED_RESULT;
@end



@interface NSObject (SWIFT_EXTENSION(CCSDK))
- (void)observeNotificationWithSelector:(SEL _Nonnull)selector name:(NSString * _Nonnull)name object:(id _Nullable)object;
- (void)removeObservationNotificationWithHasName:(NSString * _Nonnull)name object:(id _Nullable)object;
- (void)removeAllObservationNotification;
+ (void)postNotificationWithHasName:(NSString * _Nonnull)name;
+ (void)postNotificationWithHasName:(NSString * _Nonnull)name object:(id _Nullable)object;
+ (void)postNotificationWithHasName:(NSString * _Nonnull)name object:(id _Nullable)object userInfo:(NSDictionary<NSString *, id> * _Nullable)userInfo;
@end


SWIFT_CLASS("_TtC5CCSDK12QRCodeEntity")
@interface QRCodeEntity : BaseEntity
@property (nonatomic, copy) NSString * _Nullable name;
@property (nonatomic, copy) NSString * _Nullable qrcodeDescription;
@property (nonatomic, copy) NSString * _Nullable customURL;
@property (nonatomic) BOOL allowCall;
@property (nonatomic) BOOL allowChat;
@property (nonatomic, copy) NSString * _Nullable callType;
@property (nonatomic) BOOL onlyPersonal;
@property (nonatomic, copy) NSString * _Nullable step1_url;
@property (nonatomic, copy) NSString * _Nullable step2_url;
@property (nonatomic, copy) NSString * _Nullable step3_url;
@property (nonatomic, copy) NSString * _Nullable step4_url;
@property (nonatomic, copy) NSString * _Nullable step5_url;
- (nonnull instancetype)init OBJC_DESIGNATED_INITIALIZER;
- (nullable instancetype)initWithCoder:(NSCoder * _Nonnull)aDecoder OBJC_DESIGNATED_INITIALIZER;
- (void)updateInfoFromServer:(NSDictionary<NSString *, id> * _Nonnull)info;
@end


SWIFT_CLASS("_TtC5CCSDK17SDKNetworkManager")
@interface SDKNetworkManager : BaseManager
SWIFT_CLASS_PROPERTY(@property (nonatomic, class, strong) SDKNetworkManager * _Nullable internalInstance;)
+ (SDKNetworkManager * _Nullable)internalInstance SWIFT_WARN_UNUSED_RESULT;
+ (void)setInternalInstance:(SDKNetworkManager * _Nullable)value;
@property (nonatomic, copy) NSString * _Nullable token;
@property (nonatomic, copy) NSString * _Nullable storedUsername;
@property (nonatomic, copy) NSString * _Nullable storedPassword;
@property (nonatomic, copy) NSString * _Nullable enterpriseName;
SWIFT_CLASS_PROPERTY(@property (nonatomic, class, readonly, strong) SDKNetworkManager * _Nonnull shared;)
+ (SDKNetworkManager * _Nonnull)shared SWIFT_WARN_UNUSED_RESULT;
- (void)clearData;
- (NSDate * _Nonnull)getRealTime SWIFT_WARN_UNUSED_RESULT;
- (void)updateRealTime;
- (BOOL)matchLoginInfoWithEmail:(NSString * _Nonnull)email pass:(NSString * _Nonnull)pass SWIFT_WARN_UNUSED_RESULT;
- (void)saveLoginInfoWithEmail:(NSString * _Nonnull)email pass:(NSString * _Nonnull)pass newToken:(NSString * _Nonnull)newToken;
- (void)deleteLoginInfo;
- (void)doLogout;
- (void)reLogin:(BOOL)withoutLogin;
+ (void)enpLoginWithEmail:(NSString * _Nonnull)email pass:(NSString * _Nonnull)pass completion:(void (^ _Nonnull)(NSDictionary<NSString *, id> * _Nullable))completion;
+ (void)enpGetAccountDetailWithCompletion:(void (^ _Nonnull)(NSDictionary<NSString *, id> * _Nullable))completion;
+ (void)enpLogoutWithEnterprise:(NSString * _Nullable)enterprise deviceId:(NSString * _Nonnull)deviceId completion:(void (^ _Nonnull)(BOOL))completion;
+ (void)listConversationFromPage:(NSInteger)fromPage completion:(void (^ _Nonnull)(NSDictionary<NSString *, id> * _Nullable))completion;
+ (void)updatePushTokenWithDeviceId:(NSString * _Nonnull)deviceId voipToken:(NSString * _Nullable)voipToken normalToken:(NSString * _Nullable)normalToken enterprise:(NSString * _Nullable)enterprise completion:(void (^ _Nonnull)(NSDictionary<NSString *, id> * _Nullable))completion;
@end


SWIFT_CLASS("_TtC5CCSDK14SettingManager")
@interface SettingManager : BaseManager
SWIFT_CLASS_PROPERTY(@property (nonatomic, class, strong) SettingManager * _Nullable internalInstance;)
+ (SettingManager * _Nullable)internalInstance SWIFT_WARN_UNUSED_RESULT;
+ (void)setInternalInstance:(SettingManager * _Nullable)value;
SWIFT_CLASS_PROPERTY(@property (nonatomic, class, readonly, strong) SettingManager * _Nonnull shared;)
+ (SettingManager * _Nonnull)shared SWIFT_WARN_UNUSED_RESULT;
- (void)clearData;
- (void)prepare;
- (void)saveOwnerAccountFrom:(NSDictionary<NSString *, id> * _Nonnull)accInfo;
- (void)cleanupChat;
@end

@class UIView;

@interface UIBarItem (SWIFT_EXTENSION(CCSDK))
@property (nonatomic, readonly, strong) UIView * _Nullable view;
@end


@class UIImage;

@interface UIColor (SWIFT_EXTENSION(CCSDK))
- (UIImage * _Nullable)as1ptImage SWIFT_WARN_UNUSED_RESULT;
+ (UIColor * _Nonnull)defaultSystemTintColor SWIFT_WARN_UNUSED_RESULT;
+ (UIColor * _Nonnull)hexStringToUIColorWithHex:(NSString * _Nonnull)hex SWIFT_WARN_UNUSED_RESULT;
- (nullable instancetype)initWithRgbaString:(NSString * _Nonnull)rgbaString;
- (nullable instancetype)initWithHexString:(NSString * _Nonnull)hexString;
- (UIColor * _Nullable)lighterBy:(CGFloat)percentage SWIFT_WARN_UNUSED_RESULT;
- (UIColor * _Nullable)darkerBy:(CGFloat)percentage SWIFT_WARN_UNUSED_RESULT;
- (UIColor * _Nullable)adjustBy:(CGFloat)percentage SWIFT_WARN_UNUSED_RESULT;
@end


SWIFT_CLASS("_TtC5CCSDK21UIDotLoadingIndicator")
@interface UIDotLoadingIndicator : UIView
@property (nonatomic) IBInspectable NSInteger dotsCount;
@property (nonatomic) IBInspectable CGFloat dotsRadius;
@property (nonatomic) IBInspectable CGFloat dotsSpacing;
@property (nonatomic, strong) UIColor * _Null_unspecified tintColor;
- (nonnull instancetype)initWithFrame:(CGRect)frame OBJC_DESIGNATED_INITIALIZER;
- (nullable instancetype)initWithCoder:(NSCoder * _Nonnull)aDecoder OBJC_DESIGNATED_INITIALIZER;
- (void)layoutSubviews;
@end


@interface UIFont (SWIFT_EXTENSION(CCSDK))
+ (void)registerFontWithFilenameStringWithFilenameString:(NSString * _Nonnull)filenameString bundle:(NSBundle * _Nonnull)bundle;
@end








@interface UINavigationController (SWIFT_EXTENSION(CCSDK))
@property (nonatomic, readonly, strong) UIViewController * _Nullable rootViewController;
- (void)popViewControllerWithHandlerWithCompletion:(void (^ _Nonnull)(void))completion;
- (void)popViewControllerWithHandlerWithAnimated:(BOOL)animated completion:(void (^ _Nonnull)(void))completion;
- (void)pushViewControllerWithViewController:(UIViewController * _Nonnull)viewController completion:(void (^ _Nonnull)(void))completion;
@end



@interface UIView (SWIFT_EXTENSION(CCSDK))
- (void)setViewStackBackgroundWithColor:(UIColor * _Nonnull)color numOfLayer:(NSInteger)numOfLayer step:(CGFloat)step depth:(CGFloat)depth;
- (void)addDashedLineWithColor:(UIColor * _Nonnull)color;
@end



@interface UIView (SWIFT_EXTENSION(CCSDK))
- (void)rotateWithDuration:(double)duration;
- (void)stopRotating;
@end




typedef SWIFT_ENUM(NSInteger, qrcode_type, open) {
  qrcode_typeMessage = 0,
  qrcode_typeVoice = 1,
  qrcode_typeVideo = 2,
};

#endif
#if defined(__cplusplus)
#endif
#if __has_attribute(external_source_symbol)
# pragma clang attribute pop
#endif
#pragma clang diagnostic pop
#endif

#else
#error unsupported Swift architecture
#endif
