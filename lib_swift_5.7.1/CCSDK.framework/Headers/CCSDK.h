//
//  CCSDK.h
//  CCSDK
//
//  Created by Kinh Tran on 31/8/21.
//

#import <Foundation/Foundation.h>

//! Project version number for CCSDK.
FOUNDATION_EXPORT double CCSDKVersionNumber;

//! Project version string for CCSDK.
FOUNDATION_EXPORT const unsigned char CCSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CCSDK/PublicHeader.h>


#import "VoxyPADMacroDefine.h"
#import "VoxyPADFrameworkEnumDefine.h"
#import "JSONKit.h"
#import "MBTVKProgressHUD.h"
#import "VoxyPADCategoryExtend.h"
#import "UIView+Genie.h"
#import "UIPlaceHolderTextView.h"
#import "JSONKit.h"

#import "DLRadioButton.h"
#import "CUnderlineButton.h"

#import "BPXLUUIDHandler.h"
#import <SelfieSegmentation/SelfieSegment.h>


#import "VoxyPADImageManager.h"
#import "VoxyPADFileManager.h"
#import "VoxyPADDatabaseManager.h"
#import "VoxyPADUtils.h"
#import "VoxyPADLogger.h"

#import "KryptonoUtils.h"
#import "KryptonoFramework.h"

#import "CallCenterSDK.h"
