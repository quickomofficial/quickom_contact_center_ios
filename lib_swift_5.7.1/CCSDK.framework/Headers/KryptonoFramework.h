//
//  KryptonoFramework.h
//  KPayEnterpriseSDK
//
//  Created by Thai Tran on 9/24/18.
//  Copyright © 2018 Kryptono Exchange. All rights reserved.
//

#ifndef KryptonoFramework_h
#define KryptonoFramework_h

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <MobileCoreServices/MobileCoreServices.h>

#import "VoxyPADMacroDefine.h"
#import "VoxyPADFrameworkEnumDefine.h"
#import "JSONKit.h"
#import "MBTVKProgressHUD.h"
#import "VoxyPADCategoryExtend.h"
#import "UIView+Genie.h"

#if CCSDK
#import "UIAlertController+Window.h"
#import "AGEqualizerIndicatorView.h"
#endif

#if QUICKOM_MANAGER
#import "UIViewController+Window.h"
#else
#endif

#import "UIPlaceHolderTextView.h"
#import "JSONKit.h"

#ifdef BWF_SDK_CMM

#else
    #import "BPXLUUIDHandler.h"
    #import <SelfieSegmentation/SelfieSegment.h>
#endif



#import "VoxyPADImageManager.h"
#import "VoxyPADFileManager.h"
#import "VoxyPADDatabaseManager.h"
#import "VoxyPADUtils.h"
#import "VoxyPADLogger.h"

#import "KryptonoUtils.h"

#endif /* KryptonoFramework_h */
