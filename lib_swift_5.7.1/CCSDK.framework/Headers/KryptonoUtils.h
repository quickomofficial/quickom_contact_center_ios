//
//  KryptonoUtils.h
//  BWFCallingFramework
//
//  Created by Thai Tran on 3/20/19.
//  Copyright © 2019 Kryptono Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface KryptonoUtils : NSObject


+ (NSString *)bundleName;
+ (NSString *)bundleVersion;
+ (NSString *)bundleBuildNumber;

//Format: <bundle_name>_<bundle_version>
+ (NSString *)bundleNameAndVersion;

+ (void)resignKeyboardResponder;

+ (NSString *)currentLocale;

+ (void)showAlertWithTitle:(NSString *_Nullable)title
                   content:(NSString *_Nullable)content
         cancelButtonTitle:(NSString *_Nullable)button
            fromController:(UIViewController *)controller;

+ (void)showAlertWithTitle:(NSString *_Nullable)title
                   content:(NSString *_Nullable)content
         cancelButtonTitle:(NSString *_Nullable)button
             button1_title:(NSString *_Nullable)button1
            button1_action:(void(^_Nullable)(void))action1
            fromController:(UIViewController *)controller;

+ (void)showAlertWithTitle:(NSString *_Nullable)title
                   content:(NSString *_Nullable)content
         cancelButtonTitle:(NSString *_Nullable)button
             button1_title:(NSString *_Nullable)button1
            button1_action:(void(^_Nullable)(void))action1
             button2_title:(NSString *_Nullable)button2
            button2_action:(void(^_Nullable)(void))action2
            fromController:(UIViewController *)controller;

+ (void)showAlertWithTitle:(NSString *_Nullable)title
                   content:(NSString *_Nullable)content
         cancelButtonTitle:(NSString *_Nullable)button
             button1_title:(NSString *_Nullable)button1
            button1_action:(void(^_Nullable)(void))action1
             button2_title:(NSString *_Nullable)button2
            button2_action:(void(^_Nullable)(void))action2
             button3_title:(NSString *_Nullable)button3
            button3_action:(void(^_Nullable)(void))action3
            fromController:(UIViewController *)controller;

+ (void)showActionWithTitle:(NSString *_Nullable)title
                    content:(NSString *_Nullable)content
          cancelButtonTitle:(NSString *_Nullable)button
              button1_title:(NSString *_Nullable)button1
             button1_action:(void(^_Nullable)(void))action1
              button2_title:(NSString *_Nullable)button2
             button2_action:(void(^_Nullable)(void))action2
             fromController:(UIViewController *)controller;

+ (void)showActionWithTitle:(NSString *_Nullable)title
                    content:(NSString *_Nullable)content
          cancelButtonTitle:(NSString *_Nullable)button
              button1_title:(NSString *_Nullable)button1
             button1_action:(void(^_Nullable)(void))action1
              button2_title:(NSString *_Nullable)button2
             button2_action:(void(^_Nullable)(void))action2
              button3_title:(NSString *_Nullable)button3
             button3_action:(void(^_Nullable)(void))action3
             fromController:(UIViewController *)controller;

+ (void)showLoadingHUD:(NSString *)msg
                inView:(UIView * _Nullable )v
        hideAfterDelay:(NSTimeInterval)interval;
+ (void)showInfoHUD:(NSString *)msg
             inView:(UIView * _Nullable )v
               font:(UIFont *_Nullable)font
     hideAfterDelay:(NSTimeInterval)interval;
+ (void)showInfoHUD:(NSString *)msg
             inView:(UIView * _Nullable )v
               font:(UIFont *_Nullable)font;
+ (void)hideAllHUDsInView:(UIView * _Nullable )v;

@end

NS_ASSUME_NONNULL_END
