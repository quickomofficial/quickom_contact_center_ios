//
//  BWFErrorDefine.h
//  BWFMessageFramework
//
//  Created by Thai Tran on 4/16/19.
//  Copyright © 2019 Kryptono LLC. All rights reserved.
//

#ifndef BWFErrorDefine_h
#define BWFErrorDefine_h

////////////////////////////////////////////////////////////////////
// COMMON //////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////

#define BWF_ERROR_CODE_NO_ERROR 0

//INTERNAL
#define BWF_ERROR_CODE_FRAMEWORK_INTERNAL_ERROR              1

//CONFIGURATION
#define BWF_ERROR_CODE_FRAMEWORK_NOT_CONFIGURED             10
#define BWF_ERROR_CODE_FRAMEWORK_NOT_STARTED                11

//START / STOP
#define BWF_ERROR_CODE_UNABLE_TO_START_FRAMEWORK            20
#define BWF_ERROR_CODE_FRAMEWORK_ALREADY_STARTED            21
#define BWF_ERROR_CODE_FRAMEWORK_IS_CONFIGURING             22
#define BWF_ERROR_CODE_ENTERPRISE_ACCOUNT_NOT_AUTHORIZED    23
#define BWF_ERROR_CODE_ACCOUNT_NOT_EXIST                    24
#define BWF_ERROR_CODE_WRONG_ACCOUNT_TYPE                   25
#define BWF_ERROR_CODE_GROUP_NOT_FOUND                      26
#define BWF_ERROR_CODE_QRCODE_NOT_FOUND                     27
#define BWF_ERROR_CODE_QRCODE_INVALID_QRCODE_URL            28
#define BWF_ERROR_CODE_QRCODE_UNKNOWN_TELECOME_TYPE         29


////////////////////////////////////////////////////////////////////
// CALLING /////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
#define BWF_ERROR_CODE_CALLING_UNABLE_TO_CALL               100

#define BWF_ERROR_CODE_CALLING_CALLKIT_IS_NOT_SUPPORTED     107
#define BWF_ERROR_CODE_CALLING_CALLKIT_IS_NOT_ENABLED_YET   108

////////////////////////////////////////////////////////////////////
// MESSAGE ////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
#define BWF_ERROR_CODE_MESSAGE_UNABLE_TO_SEND_MESSAGE       202
#define BWF_ERROR_CODE_MESSAGE_RECEIVER_NOT_FOUND           203
#define BWF_ERROR_CODE_MESSAGE_UNABLE_TO_SEND_STATUS        204

////////////////////////////////////////////////////////////////////
// CALL CENTER /////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
#define BWF_ERROR_CODE_CALLCENTER_UNABLE_TO_CONNECT_TO_SERVER  300
#define BWF_ERROR_CODE_CALLCENTER_IS_DISCONNECTED_TO_SERVER    301

#define BWF_ERROR_CODE_CALLCENTER_UNABLE_TO_HANDLE_CHAT_REQUEST       310

#define BWF_ERROR_CODE_CALLCENTER_RECORDING_IS_NOT_SUPPORTED    311
#define BWF_ERROR_CODE_CALLCENTER_RECORDING_FAILED              312
#define BWF_ERROR_CODE_CALLCENTER_UNABLE_TO_HANDLE_CHAT_WHILE_IN_CALL_REQUEST       313

//For requester
#define BWF_ERROR_CODE_CALLCENTER_UNABLE_TO_SEND_REQUEST       325
#define BWF_ERROR_CODE_CALLCENTER_NO_AVAILABLE_SUPPORTER       326
#define BWF_ERROR_CODE_CALLCENTER_NOT_ALLOW_TO_CALL            327
#define BWF_ERROR_CODE_CALLCENTER_FAILED_TO_CONNECT_TO_FOUND_SUPPORTER            328
#define BWF_ERROR_CODE_CALLCENTER_USER_BUSY                    329
#define BWF_ERROR_CODE_CALLCENTER_NO_NETWORK                   330
#define BWF_ERROR_CODE_CALLCENTER_NETWORK_INTERRUPTED_WHILE_REQUESTING         331

#define BWF_ERROR_CODE_QRCODE_INVALID                          400
#define BWF_ERROR_CODE_QRCODE_LIMIT_EXCEED                     401


//For supporter
#define BWF_ERROR_CODE_CALLCENTER_ALL_REQUESTS_ARE_TAKEN_BY_OTHER_SUPPORTERS  350
#define BWF_ERROR_CODE_CALLCENTER_ALL_REQUESTS_ARE_CANCELED    351
#define BWF_ERROR_CODE_CALLCENTER_FAILED_TO_CONNECT_REQUESTER  352
#define BWF_ERROR_CODE_CALLCENTER_REQUEST_IS_HANDLED_BY_ANOTHER_SUPPORTER  353
#define BWF_ERROR_CODE_CALLCENTER_REQUEST_IS_TIMEOUT            354


#endif /* BWFErrorDefine_h */
