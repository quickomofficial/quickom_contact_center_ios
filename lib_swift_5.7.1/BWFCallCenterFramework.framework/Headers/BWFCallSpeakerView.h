//
//  BWFCallSpeakerView.h
//  BWFCallCenterFramework
//
//  Copyright © 2019 Beowulf. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BWFCallSpeakerView : UIView

/*!
 @abstract Set image for speaker states on/off
 */
- (void)setImageForStateOn:(UIImage * _Nullable)img;
- (void)setImageForStateOff:(UIImage * _Nullable)img;
- (void)setImageForStateMute:(UIImage * _Nullable)img;
- (void)setImageForStateBluetooth:(UIImage * _Nullable)img;

- (void)setCancelTitle:(NSString * _Nullable)cancelLabel;
- (void)setSpeakerTitle:(NSString * _Nullable)speakerLabel;
- (void)setImageForBluetoothIcon:(UIImage * _Nullable)img;
- (void)setImageForIphoneIcon:(UIImage * _Nullable)img;
- (void)setImageForSpeakerIcon:(UIImage * _Nullable)img;

- (void)setCustomHandler:(void(^ _Nullable)(void))customHandler;
- (NSArray *)getListAudioDeviceSelection;
- (void)handleSelection:(NSDictionary *)dict;

/*!
 @abstract Force speaker button to update state
 */
- (void)update;


@end

NS_ASSUME_NONNULL_END
