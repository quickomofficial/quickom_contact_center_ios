# Call Center SDK (CCSDK)
## Version 2.2

------------
## Installation

- Navigate to "https://gitlab.com/quickomofficial/quickom_contact_center_ios" and Download the entire repository
![download](images/download.png?raw=true)


*** IMPORTANT ***
- There are 'lib_swift_5.6', 'lib_swift_5.6.1' and 'lib_swift_5.5' folder, depend on your Xcode version, choose one and rename it to 'lib' (Xcode 13.3 came with swift 5.6, Xcode 13.3.1 came with swift 5.6.1, older Xcode use swift 5.5)
- Copy the lib folder to your desired place, in the example, I place the 'lib' folder just outside of project directory
![project_dir](images/project_dir.png?raw=true)

- Only add following frameworks in 'lib' folder to your project
![framework_list](images/framework_list.png?raw=true)

- In project Build Settings, find 'Framework Search Paths' and link to 'lib' folder
![framework_search_path](images/framework_search_path.png?raw=true)

- In project 'General', change 'Embed' for each framework to 'Embed & Sign'
![embed_and_sign](images/embed_and_sign.png?raw=true)

- Some frameworks not support bitcode yet, so change your project 'Enable Bitcode' to 'No' in 'Build Settings'
![bitcode](images/bitcode.png?raw=true)

*** IMPORTANT ***
- Remember to add Camera/Microphone permission description in Info.plist, otherwise your application will crash.
![permission](images/permission.png?raw=true)

- And to let call run in background, you need to set background mode to Voice over IP
![background_mode](images/background_mode.png?raw=true)

## Setup
### Step 1: Import CCSDK
```swift
import CCSDK
```

### Step 2: Start CCSDK once
- Only StartSDK once.
```swift
CallCenterSDK.start()
```

### Step 3: Login SDK
- Given username/email and password, please call following method every time app restart
```swift
CallCenterSDK.loginSDK(withUser: validUsername, password: validPassword)
```
### Step 4: Setup and Handle PushKit
- In order to receive call when app is in background, you need to implement PushKit.
- Firstly, choose an exterprise name which will be use to submit with push token. For example: "quickom_enterprise_app"
- Create VoIP Services Certificate folllowing this document https://docs.channelize.io/ios-call-sdk-voip-certificate-configuration/
- In the last step, instead of export to .pem file, choose .p12
- Contact us and send .p12, password (if any) and your enterprise name so that our server can push to your app.
- Register PushKit in AppDelegate launching
```swift
// register for VOIP Push
        pushRegistry = PKPushRegistry(queue: DispatchQueue.main)
        pushRegistry?.delegate = self
        pushRegistry?.desiredPushTypes = [.voIP]
```
- Implement PKPushRegistryDelegate, including obtain push token
```swift
func pushRegistry(_ registry: PKPushRegistry, didUpdate pushCredentials: PKPushCredentials, for type: PKPushType) {
        
        if pushCredentials.token.count == 0 {
            NSLog("Voip push token is nil")
            return
        }
        
        let token = pushCredentials.token.map { String(format: "%02.2hhx", $0) }.joined()
        UserDefaults.standard.set(token, forKey: kDeviceTokenVOIP)
        
        NSLog("DevicePushTokenVOIP = %@" + token)
        
        if CallCenterSDK.isSdkLoggedIn() == true {
            if let deviceId = BPXLUUIDHandler.uuid() {
                CallCenterSDK.updatePushToken(withDeviceId: deviceId, voipToken: token, normalToken: nil, enterprise: "quickom_enterprise_app") { result in
                    
                }
            }
        }
    }
```
- and handle push kit message
```swift
func pushRegistry(_ registry: PKPushRegistry, didReceiveIncomingPushWith payload: PKPushPayload, for type: PKPushType, completion: @escaping () -> Void) {
        
        if CallCenterSDK.isSdkLoggedIn() == false {
            return
        }
        
        let payloadDict = payload.dictionaryPayload
        let logStr = String.init(format: "Receive pushkit noti with content: %@", payloadDict)
        NSLog(logStr)
        var dataString = payloadDict["data"] as? String ?? ""
        if dataString.count == 0 {
            let aps = payloadDict["aps"] as? [String : Any] ?? [:]
            dataString = aps["alert"] as? String ?? ""
        }
        
        CallCenterSDK.handlePushkitPayload(dataString)
    }
```

### Step 5: Set CCSDK delegate to receive event when call or chat 
```swift
CallCenterSDK.start()
CallCenterSDK.setCCSDKDelegate(self)
```
### Step 6: Implement CallCenterSDKProtocol
```objective-c
@protocol CallCenterSDKProtocol <NSObject>

- (void)callCenterSDK_didMakeCallWithInfo:(NSDictionary<NSString*,id> *_Nonnull)info;
- (void)callCenterSDK_didMakeChatWithInfo:(NSDictionary<NSString*,id> *_Nonnull)info;

@end
```
- callCenterSDK_didMakeCallWithInfo is trigger when the call is about to start, the info contain the claimCode to retrieve chat history for later chat/call

- callCenterSDK_didMakeChatWithInfo is trigger when the call is about to start, the info contain the claimCode to retrieve chat history for later chat/call

- **IMPORTANT**: ***A practical implementation for these protocols is store claimCode to UserDefault with given aliasa and email/phoneNumber/userId
```swift
func callCenterSDK_didMakeCall(withInfo info: [String : Any]) {
NSLog("callCenterSDK_didMakeCall - with info = %@", info)
if let code = info["code"] as? String, code.count > 0 {
self.saveClaimCode(code, With: info)
}
}

func callCenterSDK_didMakeChat(withInfo info: [String : Any]) {
NSLog("callCenterSDK_didMakeChat - with info = %@", info)
if let code = info["code"] as? String, code.count > 0 {
self.saveClaimCode(code, With: info)
}
}

func saveClaimCode(_ code:String, With info:[String:Any]) {
var key : String? = nil
guard let alias = info["alias"] as? String, alias.count > 0
else { return }
// PhoneNumber may contain prefix +84 or 0 so trim that first
if let phoneNumber = info["phoneNumber"] as? String, phoneNumber.count > 0 {
var fixPhoneNumber = phoneNumber
fixPhoneNumber = fixPhoneNumber.headingTrim(CharacterSet(charactersIn: "0"))
fixPhoneNumber = fixPhoneNumber.replacingOccurrences(of: "+84", with: "")
key = alias + "_" + fixPhoneNumber
} else if let email = info["email"] as? String, email.count > 0 {
key = alias + "_" + email.lowercased()
} else if let userId = info["userId"] as? String, userId.count > 0 {
key = alias + "_" + userId.lowercased()
}
if let validKey = key {
UserDefaults.standard.setValue(code, forKey: validKey)
}
}
```
- Later, when make call or chat, you can retrieve claimCode from UserDefault to show old history chat.
```swift
var claimInfo = ["alias":alias]
if userId != nil { claimInfo["userId"] = userId }
if email != nil { claimInfo["email"] = email }
if phone != nil { claimInfo["phoneNumber"] = phone }
code = self.getClaimCode(With: claimInfo)
```
```swift
func getClaimCode(With info:[String : Any]) -> String? {
var key : String? = nil
guard let alias = info["alias"] as? String, alias.count > 0
else { return nil }
// PhoneNumber may contain prefix +84 or 0 so trim that first
if let phoneNumber = info["phoneNumber"] as? String, phoneNumber.count > 0 {
var fixPhoneNumber = phoneNumber
fixPhoneNumber = fixPhoneNumber.headingTrim(CharacterSet(charactersIn: "0"))
fixPhoneNumber = fixPhoneNumber.replacingOccurrences(of: "+84", with: "")
key = alias + "_" + fixPhoneNumber
} else if let email = info["email"] as? String, email.count > 0 {
key = alias + "_" + email.lowercased()
} else if let userId = info["userId"] as? String, userId.count > 0 {
key = alias + "_" + userId.lowercased()
}
if let validKey = key {
if let savedCode = UserDefaults.standard.value(forKey: validKey) as? String {
return savedCode
}
}
return nil
}
```

### Step 7: Logout SDK
- When you don't want to receive call anymore, call logout SDK
```swift
CallCenterSDK.logoutSDK()
```

## Usage
### Login/Logout SDK
- LoginSDK: Always call loginSDK everytime app restart (if not, CallKit will not work)
```swift
/// Login SDK with username/password
///
/// - Description: Call this method anytime app restart.
/// - Parameter username: The username for login
/// - Parameter password: Password for login
/// - Returns: Check for error in completion block if any
/// # Sample info  #
///
/// ```     Success
///     [
///
///     ]
/// ```
///
/// ```     Error
///     [
///         "error" : 30100,
///         "errorMessage" : "Call center account not found"
///     ]
/// ```
/// ```
///     [
///         "error" : 90101,
///         "errorMessage" : "Network error",
///     ]
/// ```
/// ```
///     [
///         "error" : 90102,
///         "errorMessage" : "Unknown error, please contact developer",
///     ]
/// ```
+ (void) loginSDKWithUser:(NSString *_Nonnull)username
                 Password:(NSString *_Nonnull)password
               completion:(void(^)(NSDictionary * _Nullable result))resultBlock;
```

- LogoutSDK: logout SDK, stop receive call and push kit

```swift
/// Logout SDK
///
/// - Description: Call this method when logout app
+ (void) logoutSDK;
```
    *** IMPORTANT NOTICE: You must call logoutSDK if you want to stop receive call (if not, you will receive unwanted PushKit call)


- To check if SDK is currently logged in or not, use this method
```swift
/// Check if SDK is logged in
///
/// - Description: Check if SDK is logged in
+ (BOOL) isSdkLoggedIn;
```
### Set desired language when using CCSDK
- Set language for displaying in CCSDK
```objective-c
/// Set desire language
///
/// - Description: Set a desire language for display
/// - Return: true if languageCode is supported, false if not supported
+ (BOOL) setDesiredLanguage:(NSString *)languageCode;
```
- To know which language is supported, call:
```objective-c
/// Get supported language
///
/// - Description: provide a list of supported language by the SDK
/// - For example: ["en", "vi"]
+ (NSArray *) supportedLanguages;
```

*** Sample get list of supported language:
```swift
let supportedLanguages = CallCenterSDK.supportedLanguages
```
And to set 'vi' (Vietnamese) as desired language
```swift
CallCenterSDK.setDesiredLanguage("vi")
```

### Request call center to get Call Center name and abilities
- Check if input call center url is mapped with a valid Call Center, 
```objective-c
/// Request call center info from given `url`.
///
/// - Warning: Invalid url may also give empty info.
/// - Parameter url: The call center url
/// - Returns: Implement your code in the completion block.
///
/// # Sample info  #
///
///     [
///         "name" : "call center label",
///         "allowChat" : true,
///         "allowCall" : true
///     ]
///
///     [
///         "error" : 10100,
///         "errorMessage" : "Input url is invalid"
///     ]
///
///     [
///         "error" : 20100,
///         "errorMessage" : "Call center not exist"
///     ]
///

+ (void) requestCallCenterInfoFrom:(NSString *_Nonnull)url
completion:(void(^)(NSDictionary * _Nullable result)
resultBlock;
```

- The result info contains an error message or following data
- name of the call center
- allow call
- allow chat
- You can check the error/data and adjust the display appropriately

```swift
CallCenterSDK.requestCallCenterInfo(from: validUrl) {[weak self] result in
guard let `self` = self,
let info = result as? [String : Any]
else { return }

if let errorMessage = info["error"] as? String {
self.btnMakeCall.isHidden = true
self.btnMakeChat.isHidden = true
DispatchQueue.main.async {
self.showAlert(errorMessage)
}
}

if let callCenterName = info["name"] as? String {
self.labelCallCenter.text = callCenterName
}
let allowCall = (info["allowCall"] as? Bool) ?? false
let allowChat = (info["allowChat"] as? Bool) ?? false

self.btnMakeCall.isHidden = !allowCall
self.btnMakeChat.isHidden = !allowChat
}
```

### Start Call with CallOption

```objective-c
/// Make request to call center with given `url`.
///
/// - Description: Check the call center info and offer user options to make decisions or call directly
///
/// - Warning: Invalid url will return error.
/// - Parameter url: The call center url copied from link or scanned from QR Code
/// - Parameter userId: Custom id for any purpose
/// - Parameter email: customer's email address
/// - Parameter phone: customer's phone number
/// - Parameter claimCode: used to claim old chat history
/// - Parameter callOption: 'NORMAL' to offer options to make call/chat, 'CALL' to call directly
/// - Returns: Check for error in completion block if any
/// # Sample info  #
///
///     Success
///     [
///
///     ]
///
///     Error
///     [
///         "error" : 20100,
///         "errorMessage" : "call center not exist"
///     ]
/// 
///     [
///         "error" : 20101,
///         "errorMessage" : "call center not allow to call",
///     ]
/// 
///     [
///         "error" : 20102,
///         "errorMessage" : "call center not allow to chat",
///     ]

+ (void) startCallCallCenterWith:(NSString *_Nonnull)url
UserID:(NSString *_Nullable)userId
Name:(NSString *_Nullable)name
Email:(NSString *_Nullable)email
Phone:(NSString *_Nullable)phone
ClaimCode:(NSString *_Nullable)claimCode
CallOption:(CallOption) callOption
completion:(void(^)(NSDictionary * _Nullable result))resultBlock;
```

### Make Call Directly
- Quickly make a call without request the call center info, 
```objective-c
/// Make call to call center with given `url`.
///
/// - Warning: Invalid url will return error.
/// - Parameter url: The call center url
/// - Parameter userId: Custom id for any purpose
/// - Parameter email: customer's email address
/// - Parameter phone: customer's phone number
/// - Parameter claimCode: used to claim old chat history
/// - Returns: Check for error in completion block if any
/// # Sample info  #
///
///     Success
///     [
///
///     ]
/// 
///     Error
///     [
///         "error" : 20100,
///         "errorMessage" : "call center not exist"
///     ]
/// 
///     [
///         "error" : 20101,
///         "errorMessage" : "call center not allow to call",
///     ]
/// 
///     [
///         "error" : 20102,
///         "errorMessage" : "call center not allow to chat",
///     ]

+ (void) makeCallCallCenterWith:(NSString *_Nonnull)url
UserID:(NSString *_Nullable)userId
Name:(NSString *_Nullable)name
Email:(NSString *_Nullable)email
Phone:(NSString *_Nullable)phone
ClaimCode:(NSString *_Nullable)claimCode
completion:(void(^)(NSDictionary * _Nullable result))resultBlock;
```

- The result info contains an error message or empty if success

```swift
CallCenterSDK.makeCallCallCenter(with: validUrl,
userID: userId,
name: callerName,
email: "useremail@gmail.com",
phone: nil,
claimCode: code) {[weak self] result in
guard let `self` = self,
let info = result as? [String : Any]
else { return }
if let errorMessage = info["errorMessage"] as? String, errorMessage.count > 0 {
DispatchQueue.main.async {
self.showAlert(errorMessage)
}
}
}
```

### Make Chat Directly
- Quickly show chat dialog without request the call center info, 
```objective-c
/// Make chat to call center with given `url`.
///
/// - Warning: Invalid url will return error.
/// - Parameter url: The call center url 
/// - Parameter userId: Custom id for any purpose
/// - Parameter name: Name of caller
/// - Parameter email: customer's email address
/// - Parameter phone: customer's phone number
/// - Parameter claimCode: used to claim old chat history
/// - Returns: Check for error in completion block if any
/// # Sample info  #
///
///     Success
///     [
///
///     ]
///
///     Error
///     [
///         "error" : 20100,
///         "errorMessage" : "call center not exist"
///     ]
/// 
///     [
///         "error" : 20101,
///         "errorMessage" : "call center not allow to call",
///     ]
/// 
///     [
///         "error" : 20102,
///         "errerrorMessageor" : "call center not allow to chat",
///     ]

+ (void) makeChatCallCenterWith:(NSString *_Nonnull)url
UserID:(NSString *_Nullable)userId
Name:(NSString *_Nullable)name
Email:(NSString *_Nullable)email
Phone:(NSString *_Nullable)phone
ClaimCode:(NSString *_Nullable)claimCode
completion:(void(^)(NSDictionary * _Nullable result))resultBlock;
```

- The result info contains an error message or empty if success

```swift
CallCenterSDK.makeChatCallCenter(with: validUrl,
userID: userId,
name: callerName,
email: "useremail@gmail.com",
phone: nil,
claimCode: code) {[weak self] result in
guard let `self` = self,
let info = result as? [String : Any]
else { return }
if let errorMessage = info["error"] as? String, errorMessage.count > 0 {
DispatchQueue.main.async {
self.showAlert(errorMessage)
}
}
}
```

### IsInCall
- Used to check whether there is a call is in progress
```objective-c
/// Call this method to check if there is a call is running
///
/// - Returns: true if call is running (even just make call), false if no call is running

+ (BOOL) isInCall;
```
### IsValidEmail
- Used to check if a string is a valid email address
```objective-c
/// Check if give 'str' is a valid email.
///
/// - Parameter str: The string to check
/// - Returns: YES if 'str' is valid email, NO if not.
+ (BOOL)isValidEmail:(NSString *)str;
```

### IsValidPhoneNumber
- Used to check if a string is a valid phone number
```objective-c
/// Check if give 'str' is a valid phone number.
///
/// - Description: A valid phone number is begin with '0' and followed by digits in range [0-9]. Or begin with '+' and followed by digits in range [0-9].
///
/// - Parameter str: The string to check
/// - Returns: YES if 'str' is valid phone number, NO if not.
+ (BOOL)isValidPhoneNumber:(NSString *)str;
```

### IsValidCallCenterUrl
- Used to check and get Call Center alias from url (if url is valid)
```objective-c
/// Check if give 'str' is a valid call center url.
///
/// - Parameter str: The string to check
/// - Returns: alias of call center if 'str' is valid call center url, NULL if not.
+ (NSString *)isValidCallCenterUrl:(NSString *)str;
```

### IsValidCallCenterUrl
- Used to check and get Call Center alias from url (if url is valid)
```objective-c
/// Check if give 'str' is a valid call center url.
///
/// - Parameter str: The string to check
/// - Returns: alias of call center if 'str' is valid call center url, NULL if not.
+ (NSString *)isValidCallCenterUrl:(NSString *)str;
```
