// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.5 (swiftlang-1300.0.31.1 clang-1300.0.29.1)
// swift-module-flags: -target arm64-apple-ios10.0 -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -Onone -module-name SwiftPhoenixClient
import Foundation
import Swift
@_exported import SwiftPhoenixClient
import _Concurrency
@_hasMissingDesignatedInitializers public class Defaults {
  public static var timeoutInterval: Swift.Double
  public static var heartbeatInterval: Swift.Double
  public static var reconnectSteppedBackOff: (_ tries: Swift.Int) -> Foundation.TimeInterval
  public static var rejoinSteppedBackOff: (_ tries: Swift.Int) -> Foundation.TimeInterval
  public static var encode: (_ json: [Swift.String : Any]) -> Foundation.Data
  public static var decode: (_ data: Foundation.Data) -> [Swift.String : Any]?
  public static var heartbeatQueue: Dispatch.DispatchQueue
  @objc deinit
}
public enum ChannelState : Swift.String {
  case closed
  case errored
  case joined
  case joining
  case leaving
  public init?(rawValue: Swift.String)
  public typealias RawValue = Swift.String
  public var rawValue: Swift.String {
    get
  }
}
public struct ChannelEvent {
  public static var heartbeat: Swift.String
  public static var join: Swift.String
  public static var leave: Swift.String
  public static var reply: Swift.String
  public static var error: Swift.String
  public static var close: Swift.String
}
@_hasMissingDesignatedInitializers public class Channel {
  final public let topic: Swift.String
  public var params: SwiftPhoenixClient.Payload {
    get
    set(value)
  }
  @objc deinit
  public var onMessage: (_ message: SwiftPhoenixClient.Message) -> SwiftPhoenixClient.Message
  @discardableResult
  public func join(timeout: Foundation.TimeInterval? = nil) -> SwiftPhoenixClient.Push
  @discardableResult
  public func onClose(_ callback: @escaping ((SwiftPhoenixClient.Message) -> Swift.Void)) -> Swift.Int
  @discardableResult
  public func delegateOnClose<Target>(to owner: Target, callback: @escaping ((Target, SwiftPhoenixClient.Message) -> Swift.Void)) -> Swift.Int where Target : AnyObject
  @discardableResult
  public func onError(_ callback: @escaping ((_ message: SwiftPhoenixClient.Message) -> Swift.Void)) -> Swift.Int
  @discardableResult
  public func delegateOnError<Target>(to owner: Target, callback: @escaping ((Target, SwiftPhoenixClient.Message) -> Swift.Void)) -> Swift.Int where Target : AnyObject
  @discardableResult
  public func on(_ event: Swift.String, callback: @escaping ((SwiftPhoenixClient.Message) -> Swift.Void)) -> Swift.Int
  @discardableResult
  public func delegateOn<Target>(_ event: Swift.String, to owner: Target, callback: @escaping ((Target, SwiftPhoenixClient.Message) -> Swift.Void)) -> Swift.Int where Target : AnyObject
  public func off(_ event: Swift.String, ref: Swift.Int? = nil)
  @discardableResult
  public func push(_ event: Swift.String, payload: SwiftPhoenixClient.Payload, timeout: Foundation.TimeInterval = Defaults.timeoutInterval) -> SwiftPhoenixClient.Push
  @discardableResult
  public func leave(timeout: Foundation.TimeInterval = Defaults.timeoutInterval) -> SwiftPhoenixClient.Push
  public func onMessage(callback: @escaping (SwiftPhoenixClient.Message) -> SwiftPhoenixClient.Message)
}
extension SwiftPhoenixClient.Channel {
  public var isClosed: Swift.Bool {
    get
  }
  public var isErrored: Swift.Bool {
    get
  }
  public var isJoined: Swift.Bool {
    get
  }
  public var isJoining: Swift.Bool {
    get
  }
  public var isLeaving: Swift.Bool {
    get
  }
}
final public class Presence {
  public struct Options {
    public static var defaults: SwiftPhoenixClient.Presence.Options
    public init(events: [SwiftPhoenixClient.Presence.Events : Swift.String])
  }
  public enum Events : Swift.String {
    case state
    case diff
    public init?(rawValue: Swift.String)
    public typealias RawValue = Swift.String
    public var rawValue: Swift.String {
      get
    }
  }
  public typealias Meta = [Swift.String : Any]
  public typealias Map = [Swift.String : [SwiftPhoenixClient.Presence.Meta]]
  public typealias State = [Swift.String : SwiftPhoenixClient.Presence.Map]
  public typealias Diff = [Swift.String : SwiftPhoenixClient.Presence.State]
  public typealias OnJoin = (_ key: Swift.String, _ current: SwiftPhoenixClient.Presence.Map?, _ new: SwiftPhoenixClient.Presence.Map) -> Swift.Void
  public typealias OnLeave = (_ key: Swift.String, _ current: SwiftPhoenixClient.Presence.Map, _ left: SwiftPhoenixClient.Presence.Map) -> Swift.Void
  public typealias OnSync = () -> Swift.Void
  final public var state: SwiftPhoenixClient.Presence.State {
    get
  }
  final public var pendingDiffs: [SwiftPhoenixClient.Presence.Diff] {
    get
  }
  final public var joinRef: Swift.String? {
    get
  }
  final public var isPendingSyncState: Swift.Bool {
    get
  }
  final public var onJoin: SwiftPhoenixClient.Presence.OnJoin {
    get
    set(newValue)
  }
  final public func onJoin(_ callback: @escaping SwiftPhoenixClient.Presence.OnJoin)
  final public var onLeave: SwiftPhoenixClient.Presence.OnLeave {
    get
    set(newValue)
  }
  final public func onLeave(_ callback: @escaping SwiftPhoenixClient.Presence.OnLeave)
  final public var onSync: SwiftPhoenixClient.Presence.OnSync {
    get
    set(newValue)
  }
  final public func onSync(_ callback: @escaping SwiftPhoenixClient.Presence.OnSync)
  public init(channel: SwiftPhoenixClient.Channel, opts: SwiftPhoenixClient.Presence.Options = Options.defaults)
  final public func list() -> [SwiftPhoenixClient.Presence.Map]
  final public func list<T>(by transformer: (Swift.String, SwiftPhoenixClient.Presence.Map) -> T) -> [T]
  final public func filter(by filter: ((Swift.String, SwiftPhoenixClient.Presence.Map) -> Swift.Bool)?) -> SwiftPhoenixClient.Presence.State
  @discardableResult
  public static func syncState(_ currentState: SwiftPhoenixClient.Presence.State, newState: SwiftPhoenixClient.Presence.State, onJoin: (_ key: Swift.String, _ current: SwiftPhoenixClient.Presence.Map?, _ new: SwiftPhoenixClient.Presence.Map) -> Swift.Void = {_,_,_ in }, onLeave: (_ key: Swift.String, _ current: SwiftPhoenixClient.Presence.Map, _ left: SwiftPhoenixClient.Presence.Map) -> Swift.Void = {_,_,_ in }) -> SwiftPhoenixClient.Presence.State
  @discardableResult
  public static func syncDiff(_ currentState: SwiftPhoenixClient.Presence.State, diff: SwiftPhoenixClient.Presence.Diff, onJoin: (_ key: Swift.String, _ current: SwiftPhoenixClient.Presence.Map?, _ new: SwiftPhoenixClient.Presence.Map) -> Swift.Void = {_,_,_ in }, onLeave: (_ key: Swift.String, _ current: SwiftPhoenixClient.Presence.Map, _ left: SwiftPhoenixClient.Presence.Map) -> Swift.Void = {_,_,_ in }) -> SwiftPhoenixClient.Presence.State
  public static func filter(_ presences: SwiftPhoenixClient.Presence.State, by filter: ((Swift.String, SwiftPhoenixClient.Presence.Map) -> Swift.Bool)?) -> SwiftPhoenixClient.Presence.State
  public static func listBy<T>(_ presences: SwiftPhoenixClient.Presence.State, transformer: (Swift.String, SwiftPhoenixClient.Presence.Map) -> T) -> [T]
  @objc deinit
}
@_hasMissingDesignatedInitializers public class Message {
  final public let ref: Swift.String
  final public let topic: Swift.String
  final public let event: Swift.String
  public var payload: SwiftPhoenixClient.Payload
  public var status: Swift.String? {
    get
  }
  @objc deinit
}
public struct Delegated<Input, Output> {
  public init()
  public mutating func delegate<Target>(to target: Target, with callback: @escaping (Target, Input) -> Output) where Target : AnyObject
  public func call(_ input: Input) -> Output?
  public var isDelegateSet: Swift.Bool {
    get
  }
}
extension SwiftPhoenixClient.Delegated {
  public mutating func stronglyDelegate<Target>(to target: Target, with callback: @escaping (Target, Input) -> Output) where Target : AnyObject
  public mutating func manuallyDelegate(with callback: @escaping (Input) -> Output)
  public mutating func removeDelegate()
}
extension SwiftPhoenixClient.Delegated where Input == Swift.Void {
  public mutating func delegate<Target>(to target: Target, with callback: @escaping (Target) -> Output) where Target : AnyObject
  public mutating func stronglyDelegate<Target>(to target: Target, with callback: @escaping (Target) -> Output) where Target : AnyObject
}
extension SwiftPhoenixClient.Delegated where Input == Swift.Void {
  public func call() -> Output?
}
extension SwiftPhoenixClient.Delegated where Output == Swift.Void {
  public func call(_ input: Input)
}
extension SwiftPhoenixClient.Delegated where Input == Swift.Void, Output == Swift.Void {
  public func call()
}
@_hasMissingDesignatedInitializers public class Push {
  weak public var channel: SwiftPhoenixClient.Channel?
  final public let event: Swift.String
  public var payload: SwiftPhoenixClient.Payload
  public var timeout: Foundation.TimeInterval
  public func resend(_ timeout: Foundation.TimeInterval = Defaults.timeoutInterval)
  public func send()
  @discardableResult
  public func receive(_ status: Swift.String, callback: @escaping ((SwiftPhoenixClient.Message) -> ())) -> SwiftPhoenixClient.Push
  @discardableResult
  public func delegateReceive<Target>(_ status: Swift.String, to owner: Target, callback: @escaping ((Target, SwiftPhoenixClient.Message) -> ())) -> SwiftPhoenixClient.Push where Target : AnyObject
  @objc deinit
}
public enum SocketError : Swift.Error {
  case abnormalClosureError
  public static func == (a: SwiftPhoenixClient.SocketError, b: SwiftPhoenixClient.SocketError) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public typealias Payload = [Swift.String : Any]
public typealias PayloadClosure = () -> SwiftPhoenixClient.Payload?
public class Socket : SwiftPhoenixClient.PhoenixTransportDelegate {
  final public let endPoint: Swift.String
  public var endPointUrl: Foundation.URL {
    get
  }
  public var params: SwiftPhoenixClient.Payload? {
    get
  }
  final public let paramsClosure: SwiftPhoenixClient.PayloadClosure?
  public var encode: ([Swift.String : Any]) -> Foundation.Data
  public var decode: (Foundation.Data) -> [Swift.String : Any]?
  public var timeout: Foundation.TimeInterval
  public var heartbeatInterval: Foundation.TimeInterval
  public var reconnectAfter: (Swift.Int) -> Foundation.TimeInterval
  public var rejoinAfter: (Swift.Int) -> Foundation.TimeInterval
  public var logger: ((Swift.String) -> Swift.Void)?
  public var skipHeartbeat: Swift.Bool
  public var disableSSLCertValidation: Swift.Bool
  public var enabledSSLCipherSuites: [Security.SSLCipherSuite]?
  @available(macOS 10.15, iOS 13, watchOS 6, tvOS 13, *)
  public convenience init(_ endPoint: Swift.String, params: SwiftPhoenixClient.Payload? = nil)
  @available(macOS 10.15, iOS 13, watchOS 6, tvOS 13, *)
  public convenience init(_ endPoint: Swift.String, paramsClosure: SwiftPhoenixClient.PayloadClosure?)
  public init(endPoint: Swift.String, transport: @escaping ((Foundation.URL) -> SwiftPhoenixClient.PhoenixTransport), paramsClosure: SwiftPhoenixClient.PayloadClosure? = nil)
  @objc deinit
  public var websocketProtocol: Swift.String {
    get
  }
  public var isConnected: Swift.Bool {
    get
  }
  public func connect()
  public func disconnect(code: SwiftPhoenixClient.Socket.CloseCode = CloseCode.normal, callback: (() -> Swift.Void)? = nil)
  @discardableResult
  public func onOpen(callback: @escaping () -> Swift.Void) -> Swift.String
  @discardableResult
  public func delegateOnOpen<T>(to owner: T, callback: @escaping ((T) -> Swift.Void)) -> Swift.String where T : AnyObject
  @discardableResult
  public func onClose(callback: @escaping () -> Swift.Void) -> Swift.String
  @discardableResult
  public func delegateOnClose<T>(to owner: T, callback: @escaping ((T) -> Swift.Void)) -> Swift.String where T : AnyObject
  @discardableResult
  public func onError(callback: @escaping (Swift.Error) -> Swift.Void) -> Swift.String
  @discardableResult
  public func delegateOnError<T>(to owner: T, callback: @escaping ((T, Swift.Error) -> Swift.Void)) -> Swift.String where T : AnyObject
  @discardableResult
  public func onMessage(callback: @escaping (SwiftPhoenixClient.Message) -> Swift.Void) -> Swift.String
  @discardableResult
  public func delegateOnMessage<T>(to owner: T, callback: @escaping ((T, SwiftPhoenixClient.Message) -> Swift.Void)) -> Swift.String where T : AnyObject
  public func releaseCallbacks()
  public func channel(_ topic: Swift.String, params: [Swift.String : Any] = [:]) -> SwiftPhoenixClient.Channel
  public func remove(_ channel: SwiftPhoenixClient.Channel)
  public func off(_ refs: [Swift.String])
  public func makeRef() -> Swift.String
  public func onOpen()
  public func onError(error: Swift.Error)
  public func onMessage(message: Swift.String)
  public func onClose(code: Swift.Int)
}
extension SwiftPhoenixClient.Socket {
  public enum CloseCode : Swift.Int {
    case abnormal
    case normal
    case goingAway
    public init?(rawValue: Swift.Int)
    public typealias RawValue = Swift.Int
    public var rawValue: Swift.Int {
      get
    }
  }
}
public protocol PhoenixTransport {
  var readyState: SwiftPhoenixClient.PhoenixTransportReadyState { get }
  var delegate: SwiftPhoenixClient.PhoenixTransportDelegate? { get set }
  func connect()
  func disconnect(code: Swift.Int, reason: Swift.String?)
  func send(data: Foundation.Data)
}
public protocol PhoenixTransportDelegate {
  func onOpen()
  func onError(error: Swift.Error)
  func onMessage(message: Swift.String)
  func onClose(code: Swift.Int)
}
public enum PhoenixTransportReadyState {
  case connecting
  case open
  case closing
  case closed
  public static func == (a: SwiftPhoenixClient.PhoenixTransportReadyState, b: SwiftPhoenixClient.PhoenixTransportReadyState) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
@objc @available(macOS 10.15, iOS 13, watchOS 6, tvOS 13, *)
public class URLSessionTransport : ObjectiveC.NSObject, SwiftPhoenixClient.PhoenixTransport, Foundation.URLSessionWebSocketDelegate {
  public init(url: Foundation.URL, configuration: Foundation.URLSessionConfiguration = .default)
  public var readyState: SwiftPhoenixClient.PhoenixTransportReadyState
  public var delegate: SwiftPhoenixClient.PhoenixTransportDelegate?
  public func connect()
  public func disconnect(code: Swift.Int, reason: Swift.String?)
  public func send(data: Foundation.Data)
  @objc public func urlSession(_ session: Foundation.URLSession, webSocketTask: Foundation.URLSessionWebSocketTask, didOpenWithProtocol protocol: Swift.String?)
  @objc public func urlSession(_ session: Foundation.URLSession, webSocketTask: Foundation.URLSessionWebSocketTask, didCloseWith closeCode: Foundation.URLSessionWebSocketTask.CloseCode, reason: Foundation.Data?)
  @objc public func urlSession(_ session: Foundation.URLSession, task: Foundation.URLSessionTask, didCompleteWithError error: Swift.Error?)
  @objc deinit
}
