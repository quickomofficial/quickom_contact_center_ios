// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.5 (swiftlang-1300.0.31.1 clang-1300.0.29.1)
// swift-module-flags: -target armv7-apple-ios10.0 -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name NotificationBanner
import Foundation
import MarqueeLabel
@_exported import NotificationBanner
import SnapKit
import Swift
import UIKit
import _Concurrency
extension UIKit.UIWindow {
  @_Concurrency.MainActor(unsafe) public var winWidth: CoreGraphics.CGFloat {
    get
  }
  @_Concurrency.MainActor(unsafe) public var winHeight: CoreGraphics.CGFloat {
    get
  }
}
@objc @_inheritsConvenienceInitializers @_hasMissingDesignatedInitializers @objcMembers @_Concurrency.MainActor(unsafe) open class StatusBarNotificationBanner : NotificationBanner.BaseNotificationBanner {
  @_Concurrency.MainActor(unsafe) @objc override public var bannerHeight: CoreGraphics.CGFloat {
    @_Concurrency.MainActor(unsafe) @objc get
    @_Concurrency.MainActor(unsafe) @objc set
  }
  @objc @_Concurrency.MainActor(unsafe) convenience public init(title: Swift.String, style: NotificationBanner.BannerStyle = .info, colors: NotificationBanner.BannerColorsProtocol? = nil)
  @objc @_Concurrency.MainActor(unsafe) convenience public init(attributedTitle: Foundation.NSAttributedString, style: NotificationBanner.BannerStyle = .info, colors: NotificationBanner.BannerColorsProtocol? = nil)
  @objc @_Concurrency.MainActor(unsafe) public init(customView: UIKit.UIView)
  @_Concurrency.MainActor(unsafe) @objc required dynamic public init?(coder aDecoder: Foundation.NSCoder)
  @objc deinit
}
extension NotificationBanner.StatusBarNotificationBanner {
  @_Concurrency.MainActor(unsafe) public func applyStyling(titleColor: UIKit.UIColor? = nil, titleTextAlign: UIKit.NSTextAlignment? = nil)
}
@objc @_hasMissingDesignatedInitializers @objcMembers @_Concurrency.MainActor(unsafe) open class NotificationBanner : NotificationBanner.BaseNotificationBanner {
  @objc @_Concurrency.MainActor(unsafe) public var subtitleLabel: MarqueeLabel.MarqueeLabel? {
    get
  }
  @objc @_Concurrency.MainActor(unsafe) public init(title: Swift.String? = nil, subtitle: Swift.String? = nil, leftView: UIKit.UIView? = nil, rightView: UIKit.UIView? = nil, style: NotificationBanner.BannerStyle = .info, colors: NotificationBanner.BannerColorsProtocol? = nil)
  @objc @_Concurrency.MainActor(unsafe) convenience public init(attributedTitle: Foundation.NSAttributedString, attributedSubtitle: Foundation.NSAttributedString? = nil, leftView: UIKit.UIView? = nil, rightView: UIKit.UIView? = nil, style: NotificationBanner.BannerStyle = .info, colors: NotificationBanner.BannerColorsProtocol? = nil)
  @objc @_Concurrency.MainActor(unsafe) public init(customView: UIKit.UIView)
  @_Concurrency.MainActor(unsafe) @objc required dynamic public init?(coder aDecoder: Foundation.NSCoder)
  @objc deinit
}
extension NotificationBanner.NotificationBanner {
  @_Concurrency.MainActor(unsafe) public func applyStyling(cornerRadius: CoreGraphics.CGFloat? = nil, titleFont: UIKit.UIFont? = nil, titleColor: UIKit.UIColor? = nil, titleTextAlign: UIKit.NSTextAlignment? = nil, subtitleFont: UIKit.UIFont? = nil, subtitleColor: UIKit.UIColor? = nil, subtitleTextAlign: UIKit.NSTextAlignment? = nil)
}
extension Swift.String {
  public func height(forConstrainedWidth width: CoreGraphics.CGFloat, font: UIKit.UIFont) -> CoreGraphics.CGFloat
}
@objc public enum BannerPosition : Swift.Int {
  case bottom
  case top
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
@objc @_Concurrency.MainActor(unsafe) open class FloatingNotificationBanner : NotificationBanner.GrowingNotificationBanner {
  @_Concurrency.MainActor(unsafe) public init(title: Swift.String? = nil, subtitle: Swift.String? = nil, titleFont: UIKit.UIFont? = nil, titleColor: UIKit.UIColor? = nil, titleTextAlign: UIKit.NSTextAlignment? = nil, subtitleFont: UIKit.UIFont? = nil, subtitleColor: UIKit.UIColor? = nil, subtitleTextAlign: UIKit.NSTextAlignment? = nil, leftView: UIKit.UIView? = nil, rightView: UIKit.UIView? = nil, style: NotificationBanner.BannerStyle = .info, colors: NotificationBanner.BannerColorsProtocol? = nil, iconPosition: NotificationBanner.GrowingNotificationBanner.IconPosition = .center)
  @objc @_Concurrency.MainActor(unsafe) public init(customView: UIKit.UIView)
  @_Concurrency.MainActor(unsafe) public func show(queuePosition: NotificationBanner.QueuePosition = .back, bannerPosition: NotificationBanner.BannerPosition = .top, queue: NotificationBanner.NotificationBannerQueue = NotificationBannerQueue.default, on viewController: UIKit.UIViewController? = nil, edgeInsets: UIKit.UIEdgeInsets = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8), cornerRadius: CoreGraphics.CGFloat? = nil, shadowColor: UIKit.UIColor = .black, shadowOpacity: CoreGraphics.CGFloat = 1, shadowBlurRadius: CoreGraphics.CGFloat = 0, shadowCornerRadius: CoreGraphics.CGFloat = 0, shadowOffset: UIKit.UIOffset = .zero, shadowEdgeInsets: UIKit.UIEdgeInsets? = nil)
  @_Concurrency.MainActor(unsafe) @objc required dynamic public init?(coder aDecoder: Foundation.NSCoder)
  @objc deinit
}
@objc public protocol BannerColorsProtocol {
  @objc func color(for style: NotificationBanner.BannerStyle) -> UIKit.UIColor
}
@_hasMissingDesignatedInitializers public class BannerColors : NotificationBanner.BannerColorsProtocol {
  @objc public func color(for style: NotificationBanner.BannerStyle) -> UIKit.UIColor
  @objc deinit
}
@objc @_hasMissingDesignatedInitializers @_Concurrency.MainActor(unsafe) open class GrowingNotificationBanner : NotificationBanner.BaseNotificationBanner {
  public enum IconPosition {
    case top
    case center
    public static func == (a: NotificationBanner.GrowingNotificationBanner.IconPosition, b: NotificationBanner.GrowingNotificationBanner.IconPosition) -> Swift.Bool
    public func hash(into hasher: inout Swift.Hasher)
    public var hashValue: Swift.Int {
      get
    }
  }
  @_Concurrency.MainActor(unsafe) @objc override public var bannerHeight: CoreGraphics.CGFloat {
    @_Concurrency.MainActor(unsafe) @objc get
    @_Concurrency.MainActor(unsafe) @objc set
  }
  @objc @_Concurrency.MainActor(unsafe) public var subtitleLabel: UIKit.UILabel? {
    get
  }
  @_Concurrency.MainActor(unsafe) public init(title: Swift.String? = nil, subtitle: Swift.String? = nil, leftView: UIKit.UIView? = nil, rightView: UIKit.UIView? = nil, style: NotificationBanner.BannerStyle = .info, colors: NotificationBanner.BannerColorsProtocol? = nil, iconPosition: NotificationBanner.GrowingNotificationBanner.IconPosition = .center, sideViewSize: CoreGraphics.CGFloat = 24.0)
  @_Concurrency.MainActor(unsafe) @objc required dynamic public init?(coder aDecoder: Foundation.NSCoder)
  @objc deinit
}
extension NotificationBanner.GrowingNotificationBanner {
  @_Concurrency.MainActor(unsafe) public func applyStyling(cornerRadius: CoreGraphics.CGFloat? = nil, titleFont: UIKit.UIFont? = nil, titleColor: UIKit.UIColor? = nil, titleTextAlign: UIKit.NSTextAlignment? = nil, subtitleFont: UIKit.UIFont? = nil, subtitleColor: UIKit.UIColor? = nil, subtitleTextAlign: UIKit.NSTextAlignment? = nil)
}
@objc public enum QueuePosition : Swift.Int {
  case back
  case front
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
@objc @objcMembers open class NotificationBannerQueue : ObjectiveC.NSObject {
  @objc public static let `default`: NotificationBanner.NotificationBannerQueue
  @objc public var numberOfBanners: Swift.Int {
    @objc get
  }
  @objc public init(maxBannersOnScreenSimultaneously: Swift.Int = 1)
  @objc public func removeAll()
  @objc public func dismissAllForced()
  @objc deinit
}
public enum BannerHaptic {
  case light
  case medium
  case heavy
  case none
  public static func == (a: NotificationBanner.BannerHaptic, b: NotificationBanner.BannerHaptic) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
@objc @_inheritsConvenienceInitializers open class BannerHapticGenerator : ObjectiveC.NSObject {
  open class func generate(_ haptic: NotificationBanner.BannerHaptic)
  @objc override dynamic public init()
  @objc deinit
}
@objc public enum BannerStyle : Swift.Int {
  case danger
  case info
  case customView
  case success
  case warning
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
public protocol NotificationBannerDelegate : AnyObject {
  func notificationBannerWillAppear(_ banner: NotificationBanner.BaseNotificationBanner)
  func notificationBannerDidAppear(_ banner: NotificationBanner.BaseNotificationBanner)
  func notificationBannerWillDisappear(_ banner: NotificationBanner.BaseNotificationBanner)
  func notificationBannerDidDisappear(_ banner: NotificationBanner.BaseNotificationBanner)
}
@objc @_hasMissingDesignatedInitializers @objcMembers @_Concurrency.MainActor(unsafe) open class BaseNotificationBanner : UIKit.UIView {
  @objc @_Concurrency.MainActor(unsafe) public static let BannerWillAppear: Foundation.Notification.Name
  @objc @_Concurrency.MainActor(unsafe) public static let BannerDidAppear: Foundation.Notification.Name
  @objc @_Concurrency.MainActor(unsafe) public static let BannerWillDisappear: Foundation.Notification.Name
  @objc @_Concurrency.MainActor(unsafe) public static let BannerDidDisappear: Foundation.Notification.Name
  @objc @_Concurrency.MainActor(unsafe) public static let BannerObjectKey: Swift.String
  @_Concurrency.MainActor(unsafe) weak public var delegate: NotificationBanner.NotificationBannerDelegate?
  @objc @_Concurrency.MainActor(unsafe) final public let style: NotificationBanner.BannerStyle
  @objc @_Concurrency.MainActor(unsafe) public var bannerHeight: CoreGraphics.CGFloat {
    @objc get
    @objc set
  }
  @objc @_Concurrency.MainActor(unsafe) public var titleLabel: UIKit.UILabel? {
    get
  }
  @objc @_Concurrency.MainActor(unsafe) public var duration: Swift.Double {
    @objc get
    @objc set
  }
  @objc @_Concurrency.MainActor(unsafe) public var autoDismiss: Swift.Bool {
    @objc get
    @objc set
  }
  @objc @_Concurrency.MainActor(unsafe) public var transparency: CoreGraphics.CGFloat {
    @objc get
    @objc set
  }
  @_Concurrency.MainActor(unsafe) public var haptic: NotificationBanner.BannerHaptic
  @objc @_Concurrency.MainActor(unsafe) public var dismissOnTap: Swift.Bool
  @objc @_Concurrency.MainActor(unsafe) public var dismissOnSwipeUp: Swift.Bool
  @objc @_Concurrency.MainActor(unsafe) public var onTap: (() -> Swift.Void)?
  @objc @_Concurrency.MainActor(unsafe) public var onSwipeUp: (() -> Swift.Void)?
  @objc @_Concurrency.MainActor(unsafe) public var bannerQueue: NotificationBanner.NotificationBannerQueue
  @objc @_Concurrency.MainActor(unsafe) public var animationDuration: Swift.Double
  @objc @_Concurrency.MainActor(unsafe) public var isDisplaying: Swift.Bool
  @_Concurrency.MainActor(unsafe) @objc override dynamic open var backgroundColor: UIKit.UIColor? {
    @_Concurrency.MainActor(unsafe) @objc get
    @_Concurrency.MainActor(unsafe) @objc set
  }
  @_Concurrency.MainActor(unsafe) @objc required dynamic public init?(coder aDecoder: Foundation.NSCoder)
  @objc deinit
  @objc @_Concurrency.MainActor(unsafe) public func show(queuePosition: NotificationBanner.QueuePosition = .back, bannerPosition: NotificationBanner.BannerPosition = .top, queue: NotificationBanner.NotificationBannerQueue = NotificationBannerQueue.default, on viewController: UIKit.UIViewController? = nil)
  @objc @_Concurrency.MainActor(unsafe) public func resetDuration()
  @objc @_Concurrency.MainActor(unsafe) public func dismiss(forced: Swift.Bool = false)
  @objc @_Concurrency.MainActor(unsafe) public func remove()
}
extension NotificationBanner.BannerPosition : Swift.Equatable {}
extension NotificationBanner.BannerPosition : Swift.Hashable {}
extension NotificationBanner.BannerPosition : Swift.RawRepresentable {}
extension NotificationBanner.GrowingNotificationBanner.IconPosition : Swift.Equatable {}
extension NotificationBanner.GrowingNotificationBanner.IconPosition : Swift.Hashable {}
extension NotificationBanner.QueuePosition : Swift.Equatable {}
extension NotificationBanner.QueuePosition : Swift.Hashable {}
extension NotificationBanner.QueuePosition : Swift.RawRepresentable {}
extension NotificationBanner.BannerHaptic : Swift.Equatable {}
extension NotificationBanner.BannerHaptic : Swift.Hashable {}
extension NotificationBanner.BannerStyle : Swift.Equatable {}
extension NotificationBanner.BannerStyle : Swift.Hashable {}
extension NotificationBanner.BannerStyle : Swift.RawRepresentable {}
