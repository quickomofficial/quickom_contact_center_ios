//
//  VoxyPADDatabaseManager.h
//  VoxyPADFramework
//
//  Created by Hong Thai on 11/11/15.
//  Copyright © 2015 VoxyPAD Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VoxyPADDatabaseManager : NSObject

+ (VoxyPADDatabaseManager *)sharedDatabaseManager;

- (void)clearData;

/*!
 @abstract Initialize database
 @param
 path: the path in disk
 name: name of the database (for example: test.sqlite)
 @result    YES: database exists at path or copy database from main bundle to 'path' successfully
 NO: can not find database at 'path' and can not find database on main bundle.
 @discussion MUST CALL AT THE BEGINNING
 If database not exist, it will copy the database (which has the name as parameter 'name') from main bundle
 and paste to 'path'. If it can not find the database (which has the name as parameter 'name') on main bundle
 it will return NO
 Otherwise, use the existing database
 */
- (BOOL)setupDatabaseWithPath:(NSString *)path databaseName:(NSString *)name;



/*!
 @abstract Create table on an existing database
 @param
 @discussion sqlString must be formed as
 "CREATE TABLE <table_name> ('<column_name>' TEXT PRIMARY KEY  NOT NULL  UNIQUE, '<column_name>' INTEGER, etc)"
 
 */
- (BOOL)createTable:(NSString *)sqlString onDatabase:(NSString *)databaseName;



/*!
 @abstract Update table on an existing database
 @param
 @discussion Add column, create column, etc
 sqlString should be formed as
 "ALTER TABLE <table_name" ADD COLUMN <new_column_name> TEXT"
 
 */
- (BOOL)updateTable:(NSString *)sqlString onDatabase:(NSString *)databaseName;




/*!
 @abstract Execute Insert/Update/Delete query on an existing database
 @param
 @discussion sqlString should be formed as
 
 + For insert a row: "INSERT INTO <table_name> (<column_1>,<column_2>,etc) VALUES (?,?,etc);"
 + For update a row: "UPDATE <table_name> SET <column_1> = ?, <column_2> = ?, etc WHERE <condition_column> = ?"
 + For delete a row: "DELETE FROM <table_name> WHERE <condition_column> = ?"

    If using Swift, please call the second method.
        -> self.excute(onDatabase: <dbName>,
                       sqlUpdateQuery: <sqlString>,
                       arguments: getVaList([<param1>, <param2>, ... , <param_n>]))
 */
- (BOOL)excuteOnDatabase:(NSString *)databaseName SQLUpdateQuery:(NSString *)sqlString,...;
- (BOOL)excuteOnDatabase:(NSString *)databaseName SQLUpdateQuery:(NSString *)sqlString arguments:(va_list)args;



/*!
 @abstract Get data on a table of an existing database
 @param columnNameArray: array which each member is a string represented for column name
 @return An array - Each member is a dictionary has key-value pair
 - key is column name
 - value is the value of the column name
 @discussion sqlString should be formed as
 "SELECT * FROM <table_name>"
 or "SELECT * FROM <table_name> where <condition_column> == %@"
 
 If using Swift, please call the second method.
        -> self.execute(onDatabase: <dbName>,
                        columnNameArray: <columnArray>,
                        sqlGetQuery: <sqlString>,
                        arguments: getVaList([<param1>, <param2>, ... , <param_n>]))
 */
- (NSMutableArray *)executeOnDatabase:(NSString *)databaseName
                      columnNameArray:(NSMutableArray *)columnNameArray
                          SQLGetQuery:(NSString *)sqlString, ...;
- (NSMutableArray *)executeOnDatabase:(NSString *)databaseName
                      columnNameArray:(NSMutableArray *)columnNameArray
                          SQLGetQuery:(NSString *)sqlString
                            arguments:(va_list)args;




@end
