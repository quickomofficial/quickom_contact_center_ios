// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.5 (swiftlang-1300.0.31.1 clang-1300.0.29.1)
// swift-module-flags: -target arm64-apple-ios8.0 -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name CountryPickerView
@_exported import CountryPickerView
import Swift
import UIKit
import _Concurrency
public protocol CountryPickerViewDelegate : AnyObject {
  func countryPickerView(_ countryPickerView: CountryPickerView.CountryPickerView, didSelectCountry country: CountryPickerView.Country)
  func countryPickerView(_ countryPickerView: CountryPickerView.CountryPickerView, willShow viewController: CountryPickerView.CountryPickerViewController)
  func countryPickerView(_ countryPickerView: CountryPickerView.CountryPickerView, didShow viewController: CountryPickerView.CountryPickerViewController)
}
public protocol CountryPickerViewDataSource : AnyObject {
  func preferredCountries(in countryPickerView: CountryPickerView.CountryPickerView) -> [CountryPickerView.Country]
  func sectionTitleForPreferredCountries(in countryPickerView: CountryPickerView.CountryPickerView) -> Swift.String?
  func showOnlyPreferredSection(in countryPickerView: CountryPickerView.CountryPickerView) -> Swift.Bool
  func sectionTitleLabelFont(in countryPickerView: CountryPickerView.CountryPickerView) -> UIKit.UIFont
  func sectionTitleLabelColor(in countryPickerView: CountryPickerView.CountryPickerView) -> UIKit.UIColor?
  func cellLabelFont(in countryPickerView: CountryPickerView.CountryPickerView) -> UIKit.UIFont
  func cellLabelColor(in countryPickerView: CountryPickerView.CountryPickerView) -> UIKit.UIColor?
  func cellImageViewSize(in countryPickerView: CountryPickerView.CountryPickerView) -> CoreGraphics.CGSize
  func cellImageViewCornerRadius(in countryPickerView: CountryPickerView.CountryPickerView) -> CoreGraphics.CGFloat
  func navigationTitle(in countryPickerView: CountryPickerView.CountryPickerView) -> Swift.String?
  func closeButtonNavigationItem(in countryPickerView: CountryPickerView.CountryPickerView) -> UIKit.UIBarButtonItem?
  func searchBarPosition(in countryPickerView: CountryPickerView.CountryPickerView) -> CountryPickerView.SearchBarPosition
  func showPhoneCodeInList(in countryPickerView: CountryPickerView.CountryPickerView) -> Swift.Bool
  func showCountryCodeInList(in countryPickerView: CountryPickerView.CountryPickerView) -> Swift.Bool
  func showCheckmarkInList(in countryPickerView: CountryPickerView.CountryPickerView) -> Swift.Bool
  func localeForCountryNameInList(in countryPickerView: CountryPickerView.CountryPickerView) -> Foundation.Locale
  func excludedCountries(in countryPickerView: CountryPickerView.CountryPickerView) -> [CountryPickerView.Country]
}
extension CountryPickerView.CountryPickerViewDataSource {
  public func preferredCountries(in countryPickerView: CountryPickerView.CountryPickerView) -> [CountryPickerView.Country]
  public func sectionTitleForPreferredCountries(in countryPickerView: CountryPickerView.CountryPickerView) -> Swift.String?
  public func showOnlyPreferredSection(in countryPickerView: CountryPickerView.CountryPickerView) -> Swift.Bool
  public func sectionTitleLabelFont(in countryPickerView: CountryPickerView.CountryPickerView) -> UIKit.UIFont
  public func sectionTitleLabelColor(in countryPickerView: CountryPickerView.CountryPickerView) -> UIKit.UIColor?
  public func cellLabelFont(in countryPickerView: CountryPickerView.CountryPickerView) -> UIKit.UIFont
  public func cellLabelColor(in countryPickerView: CountryPickerView.CountryPickerView) -> UIKit.UIColor?
  public func cellImageViewCornerRadius(in countryPickerView: CountryPickerView.CountryPickerView) -> CoreGraphics.CGFloat
  public func cellImageViewSize(in countryPickerView: CountryPickerView.CountryPickerView) -> CoreGraphics.CGSize
  public func navigationTitle(in countryPickerView: CountryPickerView.CountryPickerView) -> Swift.String?
  public func closeButtonNavigationItem(in countryPickerView: CountryPickerView.CountryPickerView) -> UIKit.UIBarButtonItem?
  public func searchBarPosition(in countryPickerView: CountryPickerView.CountryPickerView) -> CountryPickerView.SearchBarPosition
  public func showPhoneCodeInList(in countryPickerView: CountryPickerView.CountryPickerView) -> Swift.Bool
  public func showCountryCodeInList(in countryPickerView: CountryPickerView.CountryPickerView) -> Swift.Bool
  public func showCheckmarkInList(in countryPickerView: CountryPickerView.CountryPickerView) -> Swift.Bool
  public func localeForCountryNameInList(in countryPickerView: CountryPickerView.CountryPickerView) -> Foundation.Locale
  public func excludedCountries(in countryPickerView: CountryPickerView.CountryPickerView) -> [CountryPickerView.Country]
}
extension CountryPickerView.CountryPickerViewDelegate {
  public func countryPickerView(_ countryPickerView: CountryPickerView.CountryPickerView, willShow viewController: CountryPickerView.CountryPickerViewController)
  public func countryPickerView(_ countryPickerView: CountryPickerView.CountryPickerView, didShow viewController: CountryPickerView.CountryPickerViewController)
}
@objc @_inheritsConvenienceInitializers @_Concurrency.MainActor(unsafe) public class NibView : UIKit.UIView {
  @_Concurrency.MainActor(unsafe) @objc override dynamic public init(frame: CoreGraphics.CGRect)
  @_Concurrency.MainActor(unsafe) @objc required dynamic public init?(coder aDecoder: Foundation.NSCoder)
  @objc deinit
}
@objc @_inheritsConvenienceInitializers @_Concurrency.MainActor(unsafe) public class CountryPickerViewController : UIKit.UITableViewController {
  @_Concurrency.MainActor(unsafe) public var searchController: UIKit.UISearchController?
  @_Concurrency.MainActor(unsafe) @objc override dynamic public func viewDidLoad()
  @_Concurrency.MainActor(unsafe) @objc override dynamic public init(style: UIKit.UITableView.Style)
  @_Concurrency.MainActor(unsafe) @objc override dynamic public init(nibName nibNameOrNil: Swift.String?, bundle nibBundleOrNil: Foundation.Bundle?)
  @_Concurrency.MainActor(unsafe) @objc required dynamic public init?(coder: Foundation.NSCoder)
  @objc deinit
}
extension CountryPickerView.CountryPickerViewController {
  @_Concurrency.MainActor(unsafe) @objc override dynamic public func numberOfSections(in tableView: UIKit.UITableView) -> Swift.Int
  @_Concurrency.MainActor(unsafe) @objc override dynamic public func tableView(_ tableView: UIKit.UITableView, numberOfRowsInSection section: Swift.Int) -> Swift.Int
  @_Concurrency.MainActor(unsafe) @objc override dynamic public func tableView(_ tableView: UIKit.UITableView, cellForRowAt indexPath: Foundation.IndexPath) -> UIKit.UITableViewCell
  @_Concurrency.MainActor(unsafe) @objc override dynamic public func tableView(_ tableView: UIKit.UITableView, titleForHeaderInSection section: Swift.Int) -> Swift.String?
  @_Concurrency.MainActor(unsafe) @objc override dynamic public func sectionIndexTitles(for tableView: UIKit.UITableView) -> [Swift.String]?
  @_Concurrency.MainActor(unsafe) @objc override dynamic public func tableView(_ tableView: UIKit.UITableView, sectionForSectionIndexTitle title: Swift.String, at index: Swift.Int) -> Swift.Int
}
extension CountryPickerView.CountryPickerViewController {
  @_Concurrency.MainActor(unsafe) @objc override dynamic public func tableView(_ tableView: UIKit.UITableView, willDisplayHeaderView view: UIKit.UIView, forSection section: Swift.Int)
  @_Concurrency.MainActor(unsafe) @objc override dynamic public func tableView(_ tableView: UIKit.UITableView, didSelectRowAt indexPath: Foundation.IndexPath)
}
extension CountryPickerView.CountryPickerViewController : UIKit.UISearchResultsUpdating {
  @_Concurrency.MainActor(unsafe) @objc dynamic public func updateSearchResults(for searchController: UIKit.UISearchController)
}
extension CountryPickerView.CountryPickerViewController : UIKit.UISearchBarDelegate {
  @_Concurrency.MainActor(unsafe) @objc dynamic public func searchBarTextDidBeginEditing(_ searchBar: UIKit.UISearchBar)
  @_Concurrency.MainActor(unsafe) @objc dynamic public func searchBarCancelButtonClicked(_ searchBar: UIKit.UISearchBar)
}
extension CountryPickerView.CountryPickerViewController : UIKit.UISearchControllerDelegate {
  @_Concurrency.MainActor(unsafe) @objc dynamic public func willPresentSearchController(_ searchController: UIKit.UISearchController)
  @_Concurrency.MainActor(unsafe) @objc dynamic public func willDismissSearchController(_ searchController: UIKit.UISearchController)
}
public typealias CPVCountry = CountryPickerView.Country
public enum SearchBarPosition {
  case tableViewHeader, navigationBar, hidden
  public static func == (a: CountryPickerView.SearchBarPosition, b: CountryPickerView.SearchBarPosition) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public struct Country : Swift.Equatable {
  public let name: Swift.String
  public let code: Swift.String
  public let phoneCode: Swift.String
  public func localizedName(_ locale: Foundation.Locale = Locale.current) -> Swift.String?
  public var flag: UIKit.UIImage {
    get
  }
}
public func == (lhs: CountryPickerView.Country, rhs: CountryPickerView.Country) -> Swift.Bool
public func != (lhs: CountryPickerView.Country, rhs: CountryPickerView.Country) -> Swift.Bool
@objc @_inheritsConvenienceInitializers @_Concurrency.MainActor(unsafe) public class CountryPickerView : CountryPickerView.NibView {
  @objc @IBOutlet @_Concurrency.MainActor(unsafe) weak public var flagImageView: UIKit.UIImageView! {
    @objc get
    @objc set
  }
  @objc @IBOutlet @_Concurrency.MainActor(unsafe) weak public var countryDetailsLabel: UIKit.UILabel!
  @_Concurrency.MainActor(unsafe) public var showCountryCodeInView: Swift.Bool {
    get
    set
  }
  @_Concurrency.MainActor(unsafe) public var showPhoneCodeInView: Swift.Bool {
    get
    set
  }
  @_Concurrency.MainActor(unsafe) public var showCountryNameInView: Swift.Bool {
    get
    set
  }
  @_Concurrency.MainActor(unsafe) public var font: UIKit.UIFont {
    get
    set
  }
  @_Concurrency.MainActor(unsafe) public var textColor: UIKit.UIColor {
    get
    set
  }
  @_Concurrency.MainActor(unsafe) public var flagSpacingInView: CoreGraphics.CGFloat {
    get
    set
  }
  @_Concurrency.MainActor(unsafe) weak public var dataSource: CountryPickerView.CountryPickerViewDataSource?
  @_Concurrency.MainActor(unsafe) weak public var delegate: CountryPickerView.CountryPickerViewDelegate?
  @_Concurrency.MainActor(unsafe) weak public var hostViewController: UIKit.UIViewController?
  @_Concurrency.MainActor(unsafe) public var selectedCountry: CountryPickerView.Country {
    get
  }
  @_Concurrency.MainActor(unsafe) @objc override dynamic public init(frame: CoreGraphics.CGRect)
  @_Concurrency.MainActor(unsafe) @objc required dynamic public init?(coder aDecoder: Foundation.NSCoder)
  @_Concurrency.MainActor(unsafe) public func showCountriesList(from viewController: UIKit.UIViewController)
  @_Concurrency.MainActor(unsafe) final public let countries: [CountryPickerView.Country]
  @objc deinit
}
extension CountryPickerView.CountryPickerView {
  @_Concurrency.MainActor(unsafe) public func setCountryByName(_ name: Swift.String)
  @_Concurrency.MainActor(unsafe) public func setCountryByPhoneCode(_ phoneCode: Swift.String)
  @_Concurrency.MainActor(unsafe) public func setCountryByCode(_ code: Swift.String)
  @_Concurrency.MainActor(unsafe) public func getCountryByName(_ name: Swift.String) -> CountryPickerView.Country?
  @_Concurrency.MainActor(unsafe) public func getCountryByPhoneCode(_ phoneCode: Swift.String) -> CountryPickerView.Country?
  @_Concurrency.MainActor(unsafe) public func getCountryByCode(_ code: Swift.String) -> CountryPickerView.Country?
}
extension CountryPickerView.SearchBarPosition : Swift.Equatable {}
extension CountryPickerView.SearchBarPosition : Swift.Hashable {}
