//
//  BWFChatEntity.h
//  BWFCallCenterFramework
//
//  Created by Thai Tran on 11/16/20.
//  Copyright © 2020 Kryptono LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface BWFChatEntity : NSObject <NSCoding>

@property(nonatomic, strong) NSString* _Nullable groupId;

@property(nonatomic, strong) NSString* _Nullable messageId;
@property(nonatomic, strong) NSDate* _Nullable createdAt;
@property(nonatomic, strong) NSString* _Nullable senderId;
@property(nonatomic, strong) NSString* _Nullable senderDisplayName;

@property(nonatomic, strong) NSString* _Nullable content;


@property(nonatomic, readwrite) BOOL isOwnMessage;

@end

NS_ASSUME_NONNULL_END
