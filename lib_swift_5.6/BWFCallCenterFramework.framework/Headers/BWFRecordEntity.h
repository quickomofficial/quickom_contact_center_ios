//
//  BWFRecordEntity.h
//  BWFCallCenterFramework
//
//  Created by Thai Tran on 10/20/20.
//  Copyright © 2020 Kryptono LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BWFCallCenterEnumDefine.h"

NS_ASSUME_NONNULL_BEGIN

@class BWFCallCenterError;
@interface BWFRecordEntity : NSObject

@property (nonatomic, strong) NSString* path;
@property (nonatomic, readwrite) EnumBWFCallCenterRecordingType type;
@property (nonatomic, strong) NSDate* createdAt;
@property (nonatomic, readwrite) NSTimeInterval durationInSecond;

@property (nonatomic, strong) NSString* belongToRequestId;

@property (nonatomic, strong) BWFCallCenterError* error;


@end

NS_ASSUME_NONNULL_END
