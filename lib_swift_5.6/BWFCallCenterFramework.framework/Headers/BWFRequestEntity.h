//
//  BWFRequestEntity.h
//  BWFCallCenterFramework
//
//  Copyright © 2019 Beowulf. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "BWFCallCenterEnumDefine.h"

NS_ASSUME_NONNULL_BEGIN

@interface BWFRequestEntity : NSObject

@property (nonatomic, strong) NSString* requestId;
@property (nonatomic, strong) NSString* callId;
@property (nonatomic, strong) NSString* toIdentifier;
@property (nonatomic, strong) NSString* fromIdentifier;
@property (nonatomic, strong) NSString* chatIdentifier;
@property (nonatomic, strong) NSString* sipIdentifier;
@property (nonatomic, readwrite) BOOL supportWebRTC;
@property (nonatomic, readwrite) BOOL hideRequest;
 
@property (nonatomic, readwrite) EnumBWFCallCenterRequestType type;

@property (nonatomic, strong) NSDate* requestDate;
@property (nonatomic, strong) NSDictionary* _Nullable extraInfo;


@end

NS_ASSUME_NONNULL_END
