//
//  BWFCallCenterError.h
//  BWFCallCenterFramework
//
//  Copyright © 2019 Beowulf. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface BWFCallCenterError : NSObject


/* These define the error. Domains are described by names that are arbitrary strings used to differentiate groups of codes; for custom domain using reverse-DNS naming will help avoid conflicts. Codes are domain-specific.
 */
@property (readonly) NSInteger code;

/* Additional info which may be used to describe the error further. Examples of keys that might be included in here are "Line Number", "Failed URL", etc. Embedding other errors in here can also be used as a way to communicate underlying reasons for failures; for instance "File System Error" embedded in the userInfo of an NSError returned from a higher level document object.
 */
@property (readonly, copy) id extraObject;

/* The primary user-presentable message for the error, for instance for NSFileReadNoPermissionError: "The file "File Name" couldn't be opened because you don't have permission to view it.". This message should ideally indicate what failed and why it failed.
 */
@property (readonly, copy) NSString *errorDescription;



+ (instancetype)errorWithCode:(NSInteger)code
                  description:(NSString * _Nullable)desc
                  extraObject:(id _Nullable)obj;

@end

NS_ASSUME_NONNULL_END
