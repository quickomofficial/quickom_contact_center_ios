// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.5 effective-4.1.50 (swiftlang-1300.0.31.1 clang-1300.0.29.1)
// swift-module-flags: -target arm64-apple-ios8.0 -enable-objc-interop -enable-library-evolution -swift-version 4 -enforce-exclusivity=checked -Onone -module-name MessagePack
import Foundation
@_exported import MessagePack
import Swift
import _Concurrency
public func unpack(_ data: MessagePack.Subdata, compatibility: Swift.Bool = false) throws -> (value: MessagePack.MessagePackValue, remainder: MessagePack.Subdata)
public func unpack(_ data: Foundation.Data, compatibility: Swift.Bool = false) throws -> (value: MessagePack.MessagePackValue, remainder: Foundation.Data)
public func unpackFirst(_ data: Foundation.Data, compatibility: Swift.Bool = false) throws -> MessagePack.MessagePackValue
public func unpackAll(_ data: Foundation.Data, compatibility: Swift.Bool = false) throws -> [MessagePack.MessagePackValue]
public struct Subdata : Swift.RandomAccessCollection {
  public init(data: Foundation.Data, startIndex: Swift.Int = 0)
  public init(data: Foundation.Data, startIndex: Swift.Int, endIndex: Swift.Int)
  public var startIndex: Swift.Int {
    get
  }
  public var endIndex: Swift.Int {
    get
  }
  public var count: Swift.Int {
    get
  }
  public var isEmpty: Swift.Bool {
    get
  }
  public subscript(index: Swift.Int) -> Swift.UInt8 {
    get
  }
  public func index(before i: Swift.Int) -> Swift.Int
  public func index(after i: Swift.Int) -> Swift.Int
  public subscript(bounds: Swift.Range<Swift.Int>) -> MessagePack.Subdata {
    get
  }
  public var data: Foundation.Data {
    get
  }
  public typealias Element = Swift.UInt8
  public typealias Index = Swift.Int
  public typealias Indices = Swift.Range<Swift.Int>
  public typealias Iterator = Swift.IndexingIterator<MessagePack.Subdata>
  public typealias SubSequence = MessagePack.Subdata
}
public enum MessagePackValue {
  case `nil`
  case bool(Swift.Bool)
  case int(Swift.Int64)
  case uint(Swift.UInt64)
  case float(Swift.Float)
  case double(Swift.Double)
  case string(Swift.String)
  case binary(Foundation.Data)
  case array([MessagePack.MessagePackValue])
  case map([MessagePack.MessagePackValue : MessagePack.MessagePackValue])
  case extended(Swift.Int8, Foundation.Data)
}
extension MessagePack.MessagePackValue : Swift.CustomStringConvertible {
  public var description: Swift.String {
    get
  }
}
extension MessagePack.MessagePackValue : Swift.Equatable {
  public static func == (lhs: MessagePack.MessagePackValue, rhs: MessagePack.MessagePackValue) -> Swift.Bool
}
extension MessagePack.MessagePackValue : Swift.Hashable {
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public enum MessagePackError : Swift.Error {
  case invalidArgument
  case insufficientData
  case invalidData
  public static func == (a: MessagePack.MessagePackError, b: MessagePack.MessagePackError) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
extension MessagePack.MessagePackValue : Swift.ExpressibleByArrayLiteral {
  public init(arrayLiteral elements: MessagePack.MessagePackValue...)
  public typealias ArrayLiteralElement = MessagePack.MessagePackValue
}
extension MessagePack.MessagePackValue : Swift.ExpressibleByBooleanLiteral {
  public init(booleanLiteral value: Swift.Bool)
  public typealias BooleanLiteralType = Swift.Bool
}
extension MessagePack.MessagePackValue : Swift.ExpressibleByDictionaryLiteral {
  public init(dictionaryLiteral elements: (MessagePack.MessagePackValue, MessagePack.MessagePackValue)...)
  public typealias Key = MessagePack.MessagePackValue
  public typealias Value = MessagePack.MessagePackValue
}
extension MessagePack.MessagePackValue : Swift.ExpressibleByExtendedGraphemeClusterLiteral {
  public init(extendedGraphemeClusterLiteral value: Swift.String)
  public typealias ExtendedGraphemeClusterLiteralType = Swift.String
}
extension MessagePack.MessagePackValue : Swift.ExpressibleByFloatLiteral {
  public init(floatLiteral value: Swift.Double)
  public typealias FloatLiteralType = Swift.Double
}
extension MessagePack.MessagePackValue : Swift.ExpressibleByIntegerLiteral {
  public init(integerLiteral value: Swift.Int64)
  public typealias IntegerLiteralType = Swift.Int64
}
extension MessagePack.MessagePackValue : Swift.ExpressibleByNilLiteral {
  public init(nilLiteral: ())
}
extension MessagePack.MessagePackValue : Swift.ExpressibleByStringLiteral {
  public init(stringLiteral value: Swift.String)
  public typealias StringLiteralType = Swift.String
}
extension MessagePack.MessagePackValue : Swift.ExpressibleByUnicodeScalarLiteral {
  public init(unicodeScalarLiteral value: Swift.String)
  public typealias UnicodeScalarLiteralType = Swift.String
}
extension MessagePack.MessagePackValue {
  public var count: Swift.Int? {
    get
  }
  public subscript(i: Swift.Int) -> MessagePack.MessagePackValue? {
    get
  }
  public subscript(key: MessagePack.MessagePackValue) -> MessagePack.MessagePackValue? {
    get
  }
  public var isNil: Swift.Bool {
    get
  }
  @available(*, deprecated, message: "use int64Value: instead")
  public var integerValue: Swift.Int64? {
    get
  }
  public var intValue: Swift.Int? {
    get
  }
  public var int8Value: Swift.Int8? {
    get
  }
  public var int16Value: Swift.Int16? {
    get
  }
  public var int32Value: Swift.Int32? {
    get
  }
  public var int64Value: Swift.Int64? {
    get
  }
  @available(*, deprecated, message: "use uint64Value: instead")
  public var unsignedIntegerValue: Swift.UInt64? {
    get
  }
  public var uintValue: Swift.UInt? {
    get
  }
  public var uint8Value: Swift.UInt8? {
    get
  }
  public var uint16Value: Swift.UInt16? {
    get
  }
  public var uint32Value: Swift.UInt32? {
    get
  }
  public var uint64Value: Swift.UInt64? {
    get
  }
  public var arrayValue: [MessagePack.MessagePackValue]? {
    get
  }
  public var boolValue: Swift.Bool? {
    get
  }
  public var floatValue: Swift.Float? {
    get
  }
  public var doubleValue: Swift.Double? {
    get
  }
  public var stringValue: Swift.String? {
    get
  }
  public var dataValue: Foundation.Data? {
    get
  }
  public var extendedValue: (Swift.Int8, Foundation.Data)? {
    get
  }
  public var extendedType: Swift.Int8? {
    get
  }
  public var dictionaryValue: [MessagePack.MessagePackValue : MessagePack.MessagePackValue]? {
    get
  }
}
extension MessagePack.MessagePackValue {
  public init()
  public init(_ value: Swift.Bool)
  public init<S>(_ value: S) where S : Swift.SignedInteger
  public init<U>(_ value: U) where U : Swift.UnsignedInteger
  public init(_ value: Swift.Float)
  public init(_ value: Swift.Double)
  public init(_ value: Swift.String)
  public init(_ value: [MessagePack.MessagePackValue])
  public init(_ value: [MessagePack.MessagePackValue : MessagePack.MessagePackValue])
  public init(_ value: Foundation.Data)
  public init(type: Swift.Int8, data: Foundation.Data)
}
public func pack(_ value: MessagePack.MessagePackValue) -> Foundation.Data
