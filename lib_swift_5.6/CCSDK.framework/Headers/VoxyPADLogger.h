//
//  VoxyPADLogger.h
//  VoxyPADFramework
//
//  Created by Hong Thai on 8/8/15.
//  Copyright (c) 2015 VoxyPAD Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VoxyPADLogger : NSObject

//If enableLogConsoleToFile = YES, all log console will write to file on Document directory. Main usage for debugging on device that can plug into computer to debug.
//Filename of the file is "VoxyPADLogConsole.txt"
//Default is NO
+ (void)enableLogConsoleToFile:(BOOL)enable;
+ (NSString *)getLogConsoleFile;
+ (void)deleteLogConsoleFile;

+ (void)logConsole:(NSString *)format, ...;
+ (void)logFile:(NSString *)filePath text:(NSString *)format, ...;

@end
