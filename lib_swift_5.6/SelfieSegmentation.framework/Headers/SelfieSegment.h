#import <Foundation/Foundation.h>
#import <CoreVideo/CoreVideo.h>

@class SelfieSegment;

@protocol SegmentDelegate <NSObject>
// - (void)selfieSegment: (SelfieSegment*)selfieSegment didOutputLandmarks: (NSArray<Landmark *> *)landmarks;
- (void)selfieSegment: (SelfieSegment*)selfieSegment didOutputPixelBuffer: (CVPixelBufferRef)pixelBuffer;
@end

@interface SelfieSegment : NSObject
- (instancetype)init;
- (void)startGraph;
- (void)processVideoFrame:(CVPixelBufferRef)imageBuffer;
@property (weak, nonatomic) id <SegmentDelegate> delegate;
@end
