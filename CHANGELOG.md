# quickom_contact_center_sdk_ios changelog

The latest version of this file can be found at the master branch of the
quickom_contact_center_sdk_ios repository.

## 2.2 (27/11/2022)
- update new GUI for video call

## 2.1.1 (26/08/2022)
- fix bug welcome ringtone sometimes not playing

## 2.1.0 (04/08/2022)
- fix bug can't chat in some cases.

## 2.0.1 (14/07/2022)
- fix bug can't call for first time

## 2.0.0 (01/06/2022)
- allow login with email/username and password
- receive call both background (CallKit) and foreground
- remove support for swift 5.4, please upgrade to Xcode 13 or later to use this SDK.

## 1.0.4 (3/11/2021)
- allow to set desired language, if not set, device language will be used
- apply ringing duration
- cached background, logo, logo_center to avoid re-download call center theme
- support display file message (pdf, doc, xls, xlsx, ppt, mp4)
- update README.md
## 1.0.3 (2/10/2021)
- update README.md
- support Swift 5.5
- fix bugs
## 1.0.2 (23/09/2021)
- remove 'contact' field and replace with 'email' and 'phone' 
- remove 'callCallCenter', 'chatCallCenter'
- change 'forceCallCallCenter' to 'makeCallCallCenter'
- change 'forceChatCallCenter' to 'makeChatCallCenter'
- add 'startCallCallCenter' with 'CallOption' to show CallOption screen or call directly
- clarify errorCode
- support 'isValidEmail' and 'isValidPhoneNumber' to check if input email or phone is valid to SDK
- update README.md
- update CC_SDK_Sample project

## 1.0.1 (13/09/2021)
- Init CHANGELOG.md
- add CallCenterSDKProtocol, setCCSDKDelegate
- add isIncall function to check whether the CallCenterSDK is in call or not
- add ClaimCode for Call and Chat feature

## 1.0.0 (10/09/2021)
- Init this repo, add lib, add images for README.md, add CC_SDK_Sample project
