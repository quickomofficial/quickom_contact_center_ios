// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.4 (swiftlang-1205.0.26.9 clang-1205.0.19.55)
// swift-module-flags: -target armv7-apple-ios10.0 -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -Onone -module-name SnapKit
import Foundation
import Swift
import UIKit
public typealias ConstraintInsets = UIKit.UIEdgeInsets
@_hasMissingDesignatedInitializers public class ConstraintMakerFinalizable {
  @discardableResult
  public func labeled(_ label: Swift.String) -> SnapKit.ConstraintMakerFinalizable
  public var constraint: SnapKit.Constraint {
    get
  }
  @objc deinit
}
public protocol ConstraintConstantTarget {
}
extension CGPoint : SnapKit.ConstraintConstantTarget {
}
extension CGSize : SnapKit.ConstraintConstantTarget {
}
extension UIEdgeInsets : SnapKit.ConstraintConstantTarget {
}
@available(iOS 11.0, tvOS 11.0, *)
extension NSDirectionalEdgeInsets : SnapKit.ConstraintConstantTarget {
}
extension UIView {
  @available(*, deprecated, renamed: "snp.left")
  public var snp_left: SnapKit.ConstraintItem {
    get
  }
  @available(*, deprecated, renamed: "snp.top")
  public var snp_top: SnapKit.ConstraintItem {
    get
  }
  @available(*, deprecated, renamed: "snp.right")
  public var snp_right: SnapKit.ConstraintItem {
    get
  }
  @available(*, deprecated, renamed: "snp.bottom")
  public var snp_bottom: SnapKit.ConstraintItem {
    get
  }
  @available(*, deprecated, renamed: "snp.leading")
  public var snp_leading: SnapKit.ConstraintItem {
    get
  }
  @available(*, deprecated, renamed: "snp.trailing")
  public var snp_trailing: SnapKit.ConstraintItem {
    get
  }
  @available(*, deprecated, renamed: "snp.width")
  public var snp_width: SnapKit.ConstraintItem {
    get
  }
  @available(*, deprecated, renamed: "snp.height")
  public var snp_height: SnapKit.ConstraintItem {
    get
  }
  @available(*, deprecated, renamed: "snp.centerX")
  public var snp_centerX: SnapKit.ConstraintItem {
    get
  }
  @available(*, deprecated, renamed: "snp.centerY")
  public var snp_centerY: SnapKit.ConstraintItem {
    get
  }
  @available(*, deprecated, renamed: "snp.baseline")
  public var snp_baseline: SnapKit.ConstraintItem {
    get
  }
  @available(iOS 8.0, macOS 10.11, *)
  @available(*, deprecated, renamed: "snp.lastBaseline")
  public var snp_lastBaseline: SnapKit.ConstraintItem {
    get
  }
  @available(iOS 8.0, macOS 10.11, *)
  @available(iOS, deprecated, renamed: "snp.firstBaseline")
  public var snp_firstBaseline: SnapKit.ConstraintItem {
    get
  }
  @available(iOS 8.0, *)
  @available(iOS, deprecated, renamed: "snp.leftMargin")
  public var snp_leftMargin: SnapKit.ConstraintItem {
    get
  }
  @available(iOS 8.0, *)
  @available(iOS, deprecated, renamed: "snp.topMargin")
  public var snp_topMargin: SnapKit.ConstraintItem {
    get
  }
  @available(iOS 8.0, *)
  @available(iOS, deprecated, renamed: "snp.rightMargin")
  public var snp_rightMargin: SnapKit.ConstraintItem {
    get
  }
  @available(iOS 8.0, *)
  @available(iOS, deprecated, renamed: "snp.bottomMargin")
  public var snp_bottomMargin: SnapKit.ConstraintItem {
    get
  }
  @available(iOS 8.0, *)
  @available(iOS, deprecated, renamed: "snp.leadingMargin")
  public var snp_leadingMargin: SnapKit.ConstraintItem {
    get
  }
  @available(iOS 8.0, *)
  @available(iOS, deprecated, renamed: "snp.trailingMargin")
  public var snp_trailingMargin: SnapKit.ConstraintItem {
    get
  }
  @available(iOS 8.0, *)
  @available(iOS, deprecated, renamed: "snp.centerXWithinMargins")
  public var snp_centerXWithinMargins: SnapKit.ConstraintItem {
    get
  }
  @available(iOS 8.0, *)
  @available(iOS, deprecated, renamed: "snp.centerYWithinMargins")
  public var snp_centerYWithinMargins: SnapKit.ConstraintItem {
    get
  }
  @available(*, deprecated, renamed: "snp.edges")
  public var snp_edges: SnapKit.ConstraintItem {
    get
  }
  @available(*, deprecated, renamed: "snp.size")
  public var snp_size: SnapKit.ConstraintItem {
    get
  }
  @available(*, deprecated, renamed: "snp.center")
  public var snp_center: SnapKit.ConstraintItem {
    get
  }
  @available(iOS 8.0, *)
  @available(iOS, deprecated, renamed: "snp.margins")
  public var snp_margins: SnapKit.ConstraintItem {
    get
  }
  @available(iOS 8.0, *)
  @available(iOS, deprecated, renamed: "snp.centerWithinMargins")
  public var snp_centerWithinMargins: SnapKit.ConstraintItem {
    get
  }
  @available(*, deprecated, renamed: "snp.prepareConstraints(_:)")
  public func snp_prepareConstraints(_ closure: (SnapKit.ConstraintMaker) -> Swift.Void) -> [SnapKit.Constraint]
  @available(*, deprecated, renamed: "snp.makeConstraints(_:)")
  public func snp_makeConstraints(_ closure: (SnapKit.ConstraintMaker) -> Swift.Void)
  @available(*, deprecated, renamed: "snp.remakeConstraints(_:)")
  public func snp_remakeConstraints(_ closure: (SnapKit.ConstraintMaker) -> Swift.Void)
  @available(*, deprecated, renamed: "snp.updateConstraints(_:)")
  public func snp_updateConstraints(_ closure: (SnapKit.ConstraintMaker) -> Swift.Void)
  @available(*, deprecated, renamed: "snp.removeConstraints()")
  public func snp_removeConstraints()
  public var snp: SnapKit.ConstraintViewDSL {
    get
  }
}
@_hasMissingDesignatedInitializers public class ConstraintMakerRelatable {
  @discardableResult
  public func equalTo(_ other: SnapKit.ConstraintRelatableTarget, _ file: Swift.String = #file, _ line: Swift.UInt = #line) -> SnapKit.ConstraintMakerEditable
  @discardableResult
  public func equalToSuperview(_ file: Swift.String = #file, _ line: Swift.UInt = #line) -> SnapKit.ConstraintMakerEditable
  @discardableResult
  public func lessThanOrEqualTo(_ other: SnapKit.ConstraintRelatableTarget, _ file: Swift.String = #file, _ line: Swift.UInt = #line) -> SnapKit.ConstraintMakerEditable
  @discardableResult
  public func lessThanOrEqualToSuperview(_ file: Swift.String = #file, _ line: Swift.UInt = #line) -> SnapKit.ConstraintMakerEditable
  @discardableResult
  public func greaterThanOrEqualTo(_ other: SnapKit.ConstraintRelatableTarget, _ file: Swift.String = #file, line: Swift.UInt = #line) -> SnapKit.ConstraintMakerEditable
  @discardableResult
  public func greaterThanOrEqualToSuperview(_ file: Swift.String = #file, line: Swift.UInt = #line) -> SnapKit.ConstraintMakerEditable
  @objc deinit
}
public protocol ConstraintOffsetTarget : SnapKit.ConstraintConstantTarget {
}
extension Int : SnapKit.ConstraintOffsetTarget {
}
extension UInt : SnapKit.ConstraintOffsetTarget {
}
extension Float : SnapKit.ConstraintOffsetTarget {
}
extension Double : SnapKit.ConstraintOffsetTarget {
}
extension CGFloat : SnapKit.ConstraintOffsetTarget {
}
@available(iOS 9.0, macOS 10.11, *)
public struct ConstraintLayoutGuideDSL : SnapKit.ConstraintAttributesDSL {
  @discardableResult
  public func prepareConstraints(_ closure: (SnapKit.ConstraintMaker) -> Swift.Void) -> [SnapKit.Constraint]
  public func makeConstraints(_ closure: (SnapKit.ConstraintMaker) -> Swift.Void)
  public func remakeConstraints(_ closure: (SnapKit.ConstraintMaker) -> Swift.Void)
  public func updateConstraints(_ closure: (SnapKit.ConstraintMaker) -> Swift.Void)
  public func removeConstraints()
  public var target: Swift.AnyObject? {
    get
  }
}
@_inheritsConvenienceInitializers @_hasMissingDesignatedInitializers public class ConstraintMakerEditable : SnapKit.ConstraintMakerPrioritizable {
  @discardableResult
  public func multipliedBy(_ amount: SnapKit.ConstraintMultiplierTarget) -> SnapKit.ConstraintMakerEditable
  @discardableResult
  public func dividedBy(_ amount: SnapKit.ConstraintMultiplierTarget) -> SnapKit.ConstraintMakerEditable
  @discardableResult
  public func offset(_ amount: SnapKit.ConstraintOffsetTarget) -> SnapKit.ConstraintMakerEditable
  @discardableResult
  public func inset(_ amount: SnapKit.ConstraintInsetTarget) -> SnapKit.ConstraintMakerEditable
  @available(iOS 11.0, tvOS 11.0, *)
  @discardableResult
  public func inset(_ amount: SnapKit.ConstraintDirectionalInsetTarget) -> SnapKit.ConstraintMakerEditable
  @objc deinit
}
@available(iOS 9.0, *)
public typealias ConstraintLayoutGuide = UIKit.UILayoutGuide
@_hasMissingDesignatedInitializers public class ConstraintMaker {
  public var left: SnapKit.ConstraintMakerExtendable {
    get
  }
  public var top: SnapKit.ConstraintMakerExtendable {
    get
  }
  public var bottom: SnapKit.ConstraintMakerExtendable {
    get
  }
  public var right: SnapKit.ConstraintMakerExtendable {
    get
  }
  public var leading: SnapKit.ConstraintMakerExtendable {
    get
  }
  public var trailing: SnapKit.ConstraintMakerExtendable {
    get
  }
  public var width: SnapKit.ConstraintMakerExtendable {
    get
  }
  public var height: SnapKit.ConstraintMakerExtendable {
    get
  }
  public var centerX: SnapKit.ConstraintMakerExtendable {
    get
  }
  public var centerY: SnapKit.ConstraintMakerExtendable {
    get
  }
  @available(*, deprecated, renamed: "lastBaseline")
  public var baseline: SnapKit.ConstraintMakerExtendable {
    get
  }
  public var lastBaseline: SnapKit.ConstraintMakerExtendable {
    get
  }
  @available(iOS 8.0, macOS 10.11, *)
  public var firstBaseline: SnapKit.ConstraintMakerExtendable {
    get
  }
  @available(iOS 8.0, *)
  public var leftMargin: SnapKit.ConstraintMakerExtendable {
    get
  }
  @available(iOS 8.0, *)
  public var rightMargin: SnapKit.ConstraintMakerExtendable {
    get
  }
  @available(iOS 8.0, *)
  public var topMargin: SnapKit.ConstraintMakerExtendable {
    get
  }
  @available(iOS 8.0, *)
  public var bottomMargin: SnapKit.ConstraintMakerExtendable {
    get
  }
  @available(iOS 8.0, *)
  public var leadingMargin: SnapKit.ConstraintMakerExtendable {
    get
  }
  @available(iOS 8.0, *)
  public var trailingMargin: SnapKit.ConstraintMakerExtendable {
    get
  }
  @available(iOS 8.0, *)
  public var centerXWithinMargins: SnapKit.ConstraintMakerExtendable {
    get
  }
  @available(iOS 8.0, *)
  public var centerYWithinMargins: SnapKit.ConstraintMakerExtendable {
    get
  }
  public var edges: SnapKit.ConstraintMakerExtendable {
    get
  }
  public var horizontalEdges: SnapKit.ConstraintMakerExtendable {
    get
  }
  public var verticalEdges: SnapKit.ConstraintMakerExtendable {
    get
  }
  public var directionalEdges: SnapKit.ConstraintMakerExtendable {
    get
  }
  public var directionalHorizontalEdges: SnapKit.ConstraintMakerExtendable {
    get
  }
  public var directionalVerticalEdges: SnapKit.ConstraintMakerExtendable {
    get
  }
  public var size: SnapKit.ConstraintMakerExtendable {
    get
  }
  public var center: SnapKit.ConstraintMakerExtendable {
    get
  }
  @available(iOS 8.0, *)
  public var margins: SnapKit.ConstraintMakerExtendable {
    get
  }
  @available(iOS 8.0, *)
  public var directionalMargins: SnapKit.ConstraintMakerExtendable {
    get
  }
  @available(iOS 8.0, *)
  public var centerWithinMargins: SnapKit.ConstraintMakerExtendable {
    get
  }
  @objc deinit
}
public protocol ConstraintDirectionalInsetTarget : SnapKit.ConstraintConstantTarget {
}
@available(iOS 11.0, tvOS 11.0, *)
extension NSDirectionalEdgeInsets : SnapKit.ConstraintDirectionalInsetTarget {
}
public struct ConstraintPriority : Swift.ExpressibleByFloatLiteral, Swift.Equatable, Swift.Strideable {
  public typealias FloatLiteralType = Swift.Float
  public let value: Swift.Float
  public init(floatLiteral value: Swift.Float)
  public init(_ value: Swift.Float)
  public static var required: SnapKit.ConstraintPriority {
    get
  }
  public static var high: SnapKit.ConstraintPriority {
    get
  }
  public static var medium: SnapKit.ConstraintPriority {
    get
  }
  public static var low: SnapKit.ConstraintPriority {
    get
  }
  public static func == (lhs: SnapKit.ConstraintPriority, rhs: SnapKit.ConstraintPriority) -> Swift.Bool
  public func advanced(by n: SnapKit.ConstraintPriority.FloatLiteralType) -> SnapKit.ConstraintPriority
  public func distance(to other: SnapKit.ConstraintPriority) -> SnapKit.ConstraintPriority.FloatLiteralType
  public typealias Stride = SnapKit.ConstraintPriority.FloatLiteralType
}
public protocol ConstraintRelatableTarget {
}
extension Int : SnapKit.ConstraintRelatableTarget {
}
extension UInt : SnapKit.ConstraintRelatableTarget {
}
extension Float : SnapKit.ConstraintRelatableTarget {
}
extension Double : SnapKit.ConstraintRelatableTarget {
}
extension CGFloat : SnapKit.ConstraintRelatableTarget {
}
extension CGSize : SnapKit.ConstraintRelatableTarget {
}
extension CGPoint : SnapKit.ConstraintRelatableTarget {
}
extension UIEdgeInsets : SnapKit.ConstraintRelatableTarget {
}
@available(iOS 11.0, tvOS 11.0, *)
extension NSDirectionalEdgeInsets : SnapKit.ConstraintRelatableTarget {
}
extension ConstraintItem : SnapKit.ConstraintRelatableTarget {
}
extension UIView : SnapKit.ConstraintRelatableTarget {
}
@available(iOS 9.0, macOS 10.11, *)
extension UILayoutGuide : SnapKit.ConstraintRelatableTarget {
}
public protocol ConstraintDSL {
  var target: Swift.AnyObject? { get }
  func setLabel(_ value: Swift.String?)
  func label() -> Swift.String?
}
extension ConstraintDSL {
  public func setLabel(_ value: Swift.String?)
  public func label() -> Swift.String?
}
public protocol ConstraintBasicAttributesDSL : SnapKit.ConstraintDSL {
}
extension ConstraintBasicAttributesDSL {
  public var left: SnapKit.ConstraintItem {
    get
  }
  public var top: SnapKit.ConstraintItem {
    get
  }
  public var right: SnapKit.ConstraintItem {
    get
  }
  public var bottom: SnapKit.ConstraintItem {
    get
  }
  public var leading: SnapKit.ConstraintItem {
    get
  }
  public var trailing: SnapKit.ConstraintItem {
    get
  }
  public var width: SnapKit.ConstraintItem {
    get
  }
  public var height: SnapKit.ConstraintItem {
    get
  }
  public var centerX: SnapKit.ConstraintItem {
    get
  }
  public var centerY: SnapKit.ConstraintItem {
    get
  }
  public var edges: SnapKit.ConstraintItem {
    get
  }
  public var directionalEdges: SnapKit.ConstraintItem {
    get
  }
  public var horizontalEdges: SnapKit.ConstraintItem {
    get
  }
  public var verticalEdges: SnapKit.ConstraintItem {
    get
  }
  public var directionalHorizontalEdges: SnapKit.ConstraintItem {
    get
  }
  public var directionalVerticalEdges: SnapKit.ConstraintItem {
    get
  }
  public var size: SnapKit.ConstraintItem {
    get
  }
  public var center: SnapKit.ConstraintItem {
    get
  }
}
public protocol ConstraintAttributesDSL : SnapKit.ConstraintBasicAttributesDSL {
}
extension ConstraintAttributesDSL {
  @available(*, deprecated, renamed: "lastBaseline")
  public var baseline: SnapKit.ConstraintItem {
    get
  }
  @available(iOS 8.0, macOS 10.11, *)
  public var lastBaseline: SnapKit.ConstraintItem {
    get
  }
  @available(iOS 8.0, macOS 10.11, *)
  public var firstBaseline: SnapKit.ConstraintItem {
    get
  }
  @available(iOS 8.0, *)
  public var leftMargin: SnapKit.ConstraintItem {
    get
  }
  @available(iOS 8.0, *)
  public var topMargin: SnapKit.ConstraintItem {
    get
  }
  @available(iOS 8.0, *)
  public var rightMargin: SnapKit.ConstraintItem {
    get
  }
  @available(iOS 8.0, *)
  public var bottomMargin: SnapKit.ConstraintItem {
    get
  }
  @available(iOS 8.0, *)
  public var leadingMargin: SnapKit.ConstraintItem {
    get
  }
  @available(iOS 8.0, *)
  public var trailingMargin: SnapKit.ConstraintItem {
    get
  }
  @available(iOS 8.0, *)
  public var centerXWithinMargins: SnapKit.ConstraintItem {
    get
  }
  @available(iOS 8.0, *)
  public var centerYWithinMargins: SnapKit.ConstraintItem {
    get
  }
  @available(iOS 8.0, *)
  public var margins: SnapKit.ConstraintItem {
    get
  }
  @available(iOS 8.0, *)
  public var directionalMargins: SnapKit.ConstraintItem {
    get
  }
  @available(iOS 8.0, *)
  public var centerWithinMargins: SnapKit.ConstraintItem {
    get
  }
}
@_inheritsConvenienceInitializers @_hasMissingDesignatedInitializers public class ConstraintMakerExtendable : SnapKit.ConstraintMakerRelatable {
  public var left: SnapKit.ConstraintMakerExtendable {
    get
  }
  public var top: SnapKit.ConstraintMakerExtendable {
    get
  }
  public var bottom: SnapKit.ConstraintMakerExtendable {
    get
  }
  public var right: SnapKit.ConstraintMakerExtendable {
    get
  }
  public var leading: SnapKit.ConstraintMakerExtendable {
    get
  }
  public var trailing: SnapKit.ConstraintMakerExtendable {
    get
  }
  public var width: SnapKit.ConstraintMakerExtendable {
    get
  }
  public var height: SnapKit.ConstraintMakerExtendable {
    get
  }
  public var centerX: SnapKit.ConstraintMakerExtendable {
    get
  }
  public var centerY: SnapKit.ConstraintMakerExtendable {
    get
  }
  @available(*, deprecated, renamed: "lastBaseline")
  public var baseline: SnapKit.ConstraintMakerExtendable {
    get
  }
  public var lastBaseline: SnapKit.ConstraintMakerExtendable {
    get
  }
  @available(iOS 8.0, macOS 10.11, *)
  public var firstBaseline: SnapKit.ConstraintMakerExtendable {
    get
  }
  @available(iOS 8.0, *)
  public var leftMargin: SnapKit.ConstraintMakerExtendable {
    get
  }
  @available(iOS 8.0, *)
  public var rightMargin: SnapKit.ConstraintMakerExtendable {
    get
  }
  @available(iOS 8.0, *)
  public var topMargin: SnapKit.ConstraintMakerExtendable {
    get
  }
  @available(iOS 8.0, *)
  public var bottomMargin: SnapKit.ConstraintMakerExtendable {
    get
  }
  @available(iOS 8.0, *)
  public var leadingMargin: SnapKit.ConstraintMakerExtendable {
    get
  }
  @available(iOS 8.0, *)
  public var trailingMargin: SnapKit.ConstraintMakerExtendable {
    get
  }
  @available(iOS 8.0, *)
  public var centerXWithinMargins: SnapKit.ConstraintMakerExtendable {
    get
  }
  @available(iOS 8.0, *)
  public var centerYWithinMargins: SnapKit.ConstraintMakerExtendable {
    get
  }
  public var edges: SnapKit.ConstraintMakerExtendable {
    get
  }
  public var horizontalEdges: SnapKit.ConstraintMakerExtendable {
    get
  }
  public var verticalEdges: SnapKit.ConstraintMakerExtendable {
    get
  }
  public var directionalEdges: SnapKit.ConstraintMakerExtendable {
    get
  }
  public var directionalHorizontalEdges: SnapKit.ConstraintMakerExtendable {
    get
  }
  public var directionalVerticalEdges: SnapKit.ConstraintMakerExtendable {
    get
  }
  public var size: SnapKit.ConstraintMakerExtendable {
    get
  }
  @available(iOS 8.0, *)
  public var margins: SnapKit.ConstraintMakerExtendable {
    get
  }
  @available(iOS 8.0, *)
  public var directionalMargins: SnapKit.ConstraintMakerExtendable {
    get
  }
  @available(iOS 8.0, *)
  public var centerWithinMargins: SnapKit.ConstraintMakerExtendable {
    get
  }
  @objc deinit
}
@_hasMissingDesignatedInitializers final public class ConstraintItem {
  @objc deinit
}
public func == (lhs: SnapKit.ConstraintItem, rhs: SnapKit.ConstraintItem) -> Swift.Bool
public protocol ConstraintPriorityTarget {
  var constraintPriorityTargetValue: Swift.Float { get }
}
extension Int : SnapKit.ConstraintPriorityTarget {
  public var constraintPriorityTargetValue: Swift.Float {
    get
  }
}
extension UInt : SnapKit.ConstraintPriorityTarget {
  public var constraintPriorityTargetValue: Swift.Float {
    get
  }
}
extension Float : SnapKit.ConstraintPriorityTarget {
  public var constraintPriorityTargetValue: Swift.Float {
    get
  }
}
extension Double : SnapKit.ConstraintPriorityTarget {
  public var constraintPriorityTargetValue: Swift.Float {
    get
  }
}
extension CGFloat : SnapKit.ConstraintPriorityTarget {
  public var constraintPriorityTargetValue: Swift.Float {
    get
  }
}
extension UILayoutPriority : SnapKit.ConstraintPriorityTarget {
  public var constraintPriorityTargetValue: Swift.Float {
    get
  }
}
@available(iOS 8.0, *)
public typealias ConstraintLayoutSupport = UIKit.UILayoutSupport
@available(iOS 9.0, macOS 10.11, *)
extension UILayoutGuide {
  public var snp: SnapKit.ConstraintLayoutGuideDSL {
    get
  }
}
public typealias ConstraintInterfaceLayoutDirection = UIKit.UIUserInterfaceLayoutDirection
public struct ConstraintConfig {
  public static var interfaceLayoutDirection: SnapKit.ConstraintInterfaceLayoutDirection
}
@_hasMissingDesignatedInitializers final public class Constraint {
  final public var layoutConstraints: [SnapKit.LayoutConstraint]
  final public var isActive: Swift.Bool {
    get
    set(newValue)
  }
  @available(*, deprecated, renamed: "activate()")
  final public func install()
  @available(*, deprecated, renamed: "deactivate()")
  final public func uninstall()
  final public func activate()
  final public func deactivate()
  @discardableResult
  final public func update(offset: SnapKit.ConstraintOffsetTarget) -> SnapKit.Constraint
  @discardableResult
  final public func update(inset: SnapKit.ConstraintInsetTarget) -> SnapKit.Constraint
  @available(iOS 11.0, tvOS 11.0, *)
  @discardableResult
  final public func update(inset: SnapKit.ConstraintDirectionalInsetTarget) -> SnapKit.Constraint
  @discardableResult
  final public func update(priority: SnapKit.ConstraintPriorityTarget) -> SnapKit.Constraint
  @discardableResult
  final public func update(priority: SnapKit.ConstraintPriority) -> SnapKit.Constraint
  @available(*, deprecated, renamed: "update(offset:)")
  final public func updateOffset(amount: SnapKit.ConstraintOffsetTarget)
  @available(*, deprecated, renamed: "update(inset:)")
  final public func updateInsets(amount: SnapKit.ConstraintInsetTarget)
  @available(*, deprecated, renamed: "update(priority:)")
  final public func updatePriority(amount: SnapKit.ConstraintPriorityTarget)
  @available(*, deprecated, message: "Use update(priority: ConstraintPriorityTarget) instead.")
  final public func updatePriorityRequired()
  @available(*, deprecated, message: "Use update(priority: ConstraintPriorityTarget) instead.")
  final public func updatePriorityHigh()
  @available(*, deprecated, message: "Use update(priority: ConstraintPriorityTarget) instead.")
  final public func updatePriorityMedium()
  @available(*, deprecated, message: "Use update(priority: ConstraintPriorityTarget) instead.")
  final public func updatePriorityLow()
  @objc deinit
}
public typealias ConstraintView = UIKit.UIView
public protocol LayoutConstraintItem : AnyObject {
}
@available(iOS 9.0, macOS 10.11, *)
extension UILayoutGuide : SnapKit.LayoutConstraintItem {
}
extension UIView : SnapKit.LayoutConstraintItem {
}
@available(*, deprecated, message: "Use ConstraintMakerPrioritizable instead.")
public typealias ConstraintMakerPriortizable = SnapKit.ConstraintMakerPrioritizable
@_inheritsConvenienceInitializers @_hasMissingDesignatedInitializers public class ConstraintMakerPrioritizable : SnapKit.ConstraintMakerFinalizable {
  @discardableResult
  public func priority(_ amount: SnapKit.ConstraintPriority) -> SnapKit.ConstraintMakerFinalizable
  @discardableResult
  public func priority(_ amount: SnapKit.ConstraintPriorityTarget) -> SnapKit.ConstraintMakerFinalizable
  @available(*, deprecated, message: "Use priority(.required) instead.")
  @discardableResult
  public func priorityRequired() -> SnapKit.ConstraintMakerFinalizable
  @available(*, deprecated, message: "Use priority(.high) instead.")
  @discardableResult
  public func priorityHigh() -> SnapKit.ConstraintMakerFinalizable
  @available(*, deprecated, message: "Use priority(.medium) instead.")
  @discardableResult
  public func priorityMedium() -> SnapKit.ConstraintMakerFinalizable
  @available(*, deprecated, message: "Use priority(.low) instead.")
  @discardableResult
  public func priorityLow() -> SnapKit.ConstraintMakerFinalizable
  @objc deinit
}
@objc @_inheritsConvenienceInitializers public class LayoutConstraint : UIKit.NSLayoutConstraint {
  public var label: Swift.String? {
    get
    set(newValue)
  }
  @objc override dynamic public init()
  @objc deinit
}
@available(iOS 11.0, tvOS 11.0, *)
public typealias ConstraintDirectionalInsets = UIKit.NSDirectionalEdgeInsets
public protocol ConstraintInsetTarget : SnapKit.ConstraintConstantTarget {
}
extension Int : SnapKit.ConstraintInsetTarget {
}
extension UInt : SnapKit.ConstraintInsetTarget {
}
extension Float : SnapKit.ConstraintInsetTarget {
}
extension Double : SnapKit.ConstraintInsetTarget {
}
extension CGFloat : SnapKit.ConstraintInsetTarget {
}
extension UIEdgeInsets : SnapKit.ConstraintInsetTarget {
}
public protocol ConstraintMultiplierTarget {
  var constraintMultiplierTargetValue: CoreGraphics.CGFloat { get }
}
extension Int : SnapKit.ConstraintMultiplierTarget {
  public var constraintMultiplierTargetValue: CoreGraphics.CGFloat {
    get
  }
}
extension UInt : SnapKit.ConstraintMultiplierTarget {
  public var constraintMultiplierTargetValue: CoreGraphics.CGFloat {
    get
  }
}
extension Float : SnapKit.ConstraintMultiplierTarget {
  public var constraintMultiplierTargetValue: CoreGraphics.CGFloat {
    get
  }
}
extension Double : SnapKit.ConstraintMultiplierTarget {
  public var constraintMultiplierTargetValue: CoreGraphics.CGFloat {
    get
  }
}
extension CGFloat : SnapKit.ConstraintMultiplierTarget {
  public var constraintMultiplierTargetValue: CoreGraphics.CGFloat {
    get
  }
}
public struct ConstraintViewDSL : SnapKit.ConstraintAttributesDSL {
  @discardableResult
  public func prepareConstraints(_ closure: (SnapKit.ConstraintMaker) -> Swift.Void) -> [SnapKit.Constraint]
  public func makeConstraints(_ closure: (SnapKit.ConstraintMaker) -> Swift.Void)
  public func remakeConstraints(_ closure: (SnapKit.ConstraintMaker) -> Swift.Void)
  public func updateConstraints(_ closure: (SnapKit.ConstraintMaker) -> Swift.Void)
  public func removeConstraints()
  public var contentHuggingHorizontalPriority: Swift.Float {
    get
    nonmutating set(newValue)
  }
  public var contentHuggingVerticalPriority: Swift.Float {
    get
    nonmutating set(newValue)
  }
  public var contentCompressionResistanceHorizontalPriority: Swift.Float {
    get
    nonmutating set(newValue)
  }
  public var contentCompressionResistanceVerticalPriority: Swift.Float {
    get
    nonmutating set(newValue)
  }
  public var target: Swift.AnyObject? {
    get
  }
}
extension LayoutConstraint {
  @objc override dynamic public var description: Swift.String {
    @objc get
  }
}
@available(iOS 8.0, *)
public struct ConstraintLayoutSupportDSL : SnapKit.ConstraintDSL {
  public var target: Swift.AnyObject? {
    get
  }
  public var top: SnapKit.ConstraintItem {
    get
  }
  public var bottom: SnapKit.ConstraintItem {
    get
  }
  public var height: SnapKit.ConstraintItem {
    get
  }
}
extension ConstraintMakerRelatable {
  @discardableResult
  public func equalToSuperview<T>(_ closure: (SnapKit.ConstraintView) -> T, _ file: Swift.String = #file, line: Swift.UInt = #line) -> SnapKit.ConstraintMakerEditable where T : SnapKit.ConstraintRelatableTarget
  @discardableResult
  public func lessThanOrEqualToSuperview<T>(_ closure: (SnapKit.ConstraintView) -> T, _ file: Swift.String = #file, line: Swift.UInt = #line) -> SnapKit.ConstraintMakerEditable where T : SnapKit.ConstraintRelatableTarget
  @discardableResult
  public func greaterThanOrEqualTo<T>(_ closure: (SnapKit.ConstraintView) -> T, _ file: Swift.String = #file, line: Swift.UInt = #line) -> SnapKit.ConstraintMakerEditable where T : SnapKit.ConstraintRelatableTarget
}
@available(iOS 8.0, *)
extension UILayoutSupport {
  public var snp: SnapKit.ConstraintLayoutSupportDSL {
    get
  }
}
@_hasMissingDesignatedInitializers public class ConstraintDescription {
  @objc deinit
}
