// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.4 (swiftlang-1205.0.26.9 clang-1205.0.19.55)
// swift-module-flags: -target arm64-apple-ios9.0 -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -Onone -module-name NVActivityIndicatorView
import QuartzCore
import Swift
import UIKit
public enum NVActivityIndicatorType : Swift.CaseIterable {
  case blank
  case ballPulse
  case ballGridPulse
  case ballClipRotate
  case squareSpin
  case ballClipRotatePulse
  case ballClipRotateMultiple
  case ballPulseRise
  case ballRotate
  case cubeTransition
  case ballZigZag
  case ballZigZagDeflect
  case ballTrianglePath
  case ballScale
  case lineScale
  case lineScaleParty
  case ballScaleMultiple
  case ballPulseSync
  case ballBeat
  case ballDoubleBounce
  case lineScalePulseOut
  case lineScalePulseOutRapid
  case ballScaleRipple
  case ballScaleRippleMultiple
  case ballSpinFadeLoader
  case lineSpinFadeLoader
  case triangleSkewSpin
  case pacman
  case ballGridBeat
  case semiCircleSpin
  case ballRotateChase
  case orbit
  case audioEqualizer
  case circleStrokeSpin
  public static func == (a: NVActivityIndicatorView.NVActivityIndicatorType, b: NVActivityIndicatorView.NVActivityIndicatorType) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public typealias AllCases = [NVActivityIndicatorView.NVActivityIndicatorType]
  public static var allCases: [NVActivityIndicatorView.NVActivityIndicatorType] {
    get
  }
  public var hashValue: Swift.Int {
    get
  }
}
public typealias FadeInAnimation = (UIKit.UIView) -> Swift.Void
public typealias FadeOutAnimation = (UIKit.UIView, @escaping () -> Swift.Void) -> Swift.Void
@objc final public class NVActivityIndicatorView : UIKit.UIView {
  public static var DEFAULT_TYPE: NVActivityIndicatorView.NVActivityIndicatorType
  public static var DEFAULT_COLOR: UIKit.UIColor
  public static var DEFAULT_TEXT_COLOR: UIKit.UIColor
  public static var DEFAULT_PADDING: CoreGraphics.CGFloat
  public static var DEFAULT_BLOCKER_SIZE: CoreGraphics.CGSize
  public static var DEFAULT_BLOCKER_DISPLAY_TIME_THRESHOLD: Swift.Int
  public static var DEFAULT_BLOCKER_MINIMUM_DISPLAY_TIME: Swift.Int
  public static var DEFAULT_BLOCKER_MESSAGE: Swift.String?
  public static var DEFAULT_BLOCKER_MESSAGE_SPACING: CoreGraphics.CGFloat
  public static var DEFAULT_BLOCKER_MESSAGE_FONT: UIKit.UIFont
  public static var DEFAULT_BLOCKER_BACKGROUND_COLOR: UIKit.UIColor
  public static var DEFAULT_FADE_IN_ANIMATION: (UIKit.UIView) -> Swift.Void
  public static var DEFAULT_FADE_OUT_ANIMATION: (UIKit.UIView, @escaping () -> Swift.Void) -> Swift.Void
  final public var type: NVActivityIndicatorView.NVActivityIndicatorType
  @objc @IBInspectable final public var color: UIKit.UIColor
  @objc @IBInspectable final public var padding: CoreGraphics.CGFloat
  @available(*, deprecated)
  final public var animating: Swift.Bool {
    get
  }
  final public var isAnimating: Swift.Bool {
    get
  }
  @objc required dynamic public init?(coder aDecoder: Foundation.NSCoder)
  public init(frame: CoreGraphics.CGRect, type: NVActivityIndicatorView.NVActivityIndicatorType? = nil, color: UIKit.UIColor? = nil, padding: CoreGraphics.CGFloat? = nil)
  @objc override final public var intrinsicContentSize: CoreGraphics.CGSize {
    @objc get
  }
  @objc override final public var bounds: CoreGraphics.CGRect {
    @objc get
    @objc set(value)
  }
  final public func startAnimating()
  final public func stopAnimating()
  @objc override dynamic public init(frame: CoreGraphics.CGRect)
  @objc deinit
}
