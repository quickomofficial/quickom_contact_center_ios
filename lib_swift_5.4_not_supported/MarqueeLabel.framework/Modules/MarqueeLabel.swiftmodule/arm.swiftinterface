// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.4 (swiftlang-1205.0.26.9 clang-1205.0.19.55)
// swift-module-flags: -target armv7-apple-ios8.0 -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -Onone -module-name MarqueeLabel
import QuartzCore
import Swift
import UIKit
@objc @_inheritsConvenienceInitializers @IBDesignable open class MarqueeLabel : UIKit.UILabel, QuartzCore.CAAnimationDelegate {
  public enum MarqueeType : Swift.CaseIterable {
    case left
    case leftRight
    case right
    case rightLeft
    case continuous
    case continuousReverse
    public static func == (a: MarqueeLabel.MarqueeLabel.MarqueeType, b: MarqueeLabel.MarqueeLabel.MarqueeType) -> Swift.Bool
    public func hash(into hasher: inout Swift.Hasher)
    public typealias AllCases = [MarqueeLabel.MarqueeLabel.MarqueeType]
    public static var allCases: [MarqueeLabel.MarqueeLabel.MarqueeType] {
      get
    }
    public var hashValue: Swift.Int {
      get
    }
  }
  open var type: MarqueeLabel.MarqueeLabel.MarqueeType {
    get
    set(value)
  }
  open var scrollSequence: [MarqueeLabel.MarqueeStep]?
  open var animationCurve: UIKit.UIView.AnimationCurve
  @objc @IBInspectable open var labelize: Swift.Bool {
    @objc get
    @objc set(value)
  }
  @objc @IBInspectable open var holdScrolling: Swift.Bool {
    @objc get
    @objc set(value)
  }
  @objc @IBInspectable public var forceScrolling: Swift.Bool {
    @objc get
    @objc set(value)
  }
  @objc @IBInspectable open var tapToScroll: Swift.Bool {
    @objc get
    @objc set(value)
  }
  open var isPaused: Swift.Bool {
    get
  }
  open var awayFromHome: Swift.Bool {
    get
  }
  open var animationPosition: CoreGraphics.CGFloat? {
    get
  }
  public enum SpeedLimit {
    case rate(CoreGraphics.CGFloat)
    case duration(CoreGraphics.CGFloat)
  }
  open var speed: MarqueeLabel.MarqueeLabel.SpeedLimit {
    get
    set(value)
  }
  @objc @available(*, deprecated, message: "Use speed property instead")
  @IBInspectable open var scrollDuration: CoreGraphics.CGFloat {
    @objc get
    @objc set(newValue)
  }
  @objc @available(*, deprecated, message: "Use speed property instead")
  @IBInspectable open var scrollRate: CoreGraphics.CGFloat {
    @objc get
    @objc set(newValue)
  }
  @objc @IBInspectable open var leadingBuffer: CoreGraphics.CGFloat {
    @objc get
    @objc set(value)
  }
  @objc @IBInspectable open var trailingBuffer: CoreGraphics.CGFloat {
    @objc get
    @objc set(value)
  }
  @objc @IBInspectable open var fadeLength: CoreGraphics.CGFloat {
    @objc get
    @objc set(value)
  }
  @objc @IBInspectable open var animationDelay: CoreGraphics.CGFloat
  public var animationDuration: CoreGraphics.CGFloat {
    get
  }
  open class func restartLabelsOfController(_ controller: UIKit.UIViewController)
  open class func controllerViewWillAppear(_ controller: UIKit.UIViewController)
  open class func controllerViewDidAppear(_ controller: UIKit.UIViewController)
  open class func controllerLabelsLabelize(_ controller: UIKit.UIViewController)
  open class func controllerLabelsAnimate(_ controller: UIKit.UIViewController)
  public init(frame: CoreGraphics.CGRect, rate: CoreGraphics.CGFloat, fadeLength fade: CoreGraphics.CGFloat)
  public init(frame: CoreGraphics.CGRect, duration: CoreGraphics.CGFloat, fadeLength fade: CoreGraphics.CGFloat)
  @objc required dynamic public init?(coder aDecoder: Foundation.NSCoder)
  @objc override dynamic public convenience init(frame: CoreGraphics.CGRect)
  @objc override dynamic open func awakeFromNib()
  @available(iOS 8.0, *)
  @objc override dynamic open func prepareForInterfaceBuilder()
  @objc override dynamic open func layoutSubviews()
  @objc override dynamic open func willMove(toWindow newWindow: UIKit.UIWindow?)
  @objc override dynamic open func didMoveToWindow()
  @objc override dynamic open func sizeThatFits(_ size: CoreGraphics.CGSize) -> CoreGraphics.CGSize
  open func sizeThatFits(_ size: CoreGraphics.CGSize, withBuffers: Swift.Bool) -> CoreGraphics.CGSize
  open func textLayoutSize() -> CoreGraphics.CGSize
  open func labelShouldScroll() -> Swift.Bool
  @objc public func animationDidStop(_ anim: QuartzCore.CAAnimation, finished flag: Swift.Bool)
  @objc override dynamic open class var layerClass: Swift.AnyClass {
    @objc get
  }
  @objc override dynamic open func draw(_ layer: QuartzCore.CALayer, in ctx: CoreGraphics.CGContext)
  @objc public func restartForViewController(_ notification: Foundation.Notification)
  @objc public func labelizeForController(_ notification: Foundation.Notification)
  @objc public func animateForController(_ notification: Foundation.Notification)
  public func triggerScrollStart()
  @objc public func restartLabel()
  @available(*, deprecated, message: "Use the shutdownLabel function instead")
  public func resetLabel()
  @objc public func shutdownLabel()
  public func pauseLabel()
  public func unpauseLabel()
  @objc public func labelWasTapped(_ recognizer: UIKit.UIGestureRecognizer)
  open func textCoordinateForFramePoint(_ point: CoreGraphics.CGPoint) -> CoreGraphics.CGPoint?
  open func labelWillBeginScroll()
  open func labelReturnedToHome(_ finished: Swift.Bool)
  @objc override dynamic open func forBaselineLayout() -> UIKit.UIView
  @objc override dynamic open var forLastBaselineLayout: UIKit.UIView {
    @objc get
  }
  @objc override dynamic open var text: Swift.String? {
    @objc get
    @objc set(newValue)
  }
  @objc override dynamic open var attributedText: Foundation.NSAttributedString? {
    @objc get
    @objc set(newValue)
  }
  @objc override dynamic open var font: UIKit.UIFont! {
    @objc get
    @objc set(newValue)
  }
  @objc override dynamic open var textColor: UIKit.UIColor! {
    @objc get
    @objc set(newValue)
  }
  @objc override dynamic open var backgroundColor: UIKit.UIColor? {
    @objc get
    @objc set(newValue)
  }
  @objc override dynamic open var shadowColor: UIKit.UIColor? {
    @objc get
    @objc set(newValue)
  }
  @objc override dynamic open var shadowOffset: CoreGraphics.CGSize {
    @objc get
    @objc set(newValue)
  }
  @objc override dynamic open var highlightedTextColor: UIKit.UIColor? {
    @objc get
    @objc set(newValue)
  }
  @objc override dynamic open var isHighlighted: Swift.Bool {
    @objc get
    @objc set(newValue)
  }
  @objc override dynamic open var isEnabled: Swift.Bool {
    @objc get
    @objc set(newValue)
  }
  @objc override dynamic open var numberOfLines: Swift.Int {
    @objc get
    @objc set(newValue)
  }
  @objc override dynamic open var baselineAdjustment: UIKit.UIBaselineAdjustment {
    @objc get
    @objc set(newValue)
  }
  @objc override dynamic open var intrinsicContentSize: CoreGraphics.CGSize {
    @objc get
  }
  @objc override dynamic open var tintColor: UIKit.UIColor! {
    @objc get
    @objc set(newValue)
  }
  @objc override dynamic open func tintColorDidChange()
  @objc override dynamic open var contentMode: UIKit.UIView.ContentMode {
    @objc get
    @objc set(newValue)
  }
  @objc override dynamic open var isAccessibilityElement: Swift.Bool {
    @objc get
    @objc set(value)
  }
  @objc deinit
}
public protocol MarqueeStep {
  var timeStep: CoreGraphics.CGFloat { get }
  var timingFunction: UIKit.UIView.AnimationCurve { get }
  var edgeFades: MarqueeLabel.EdgeFade { get }
}
public struct ScrollStep : MarqueeLabel.MarqueeStep {
  public enum Position {
    case home
    case away
    case partial(CoreGraphics.CGFloat)
  }
  public let timeStep: CoreGraphics.CGFloat
  public let timingFunction: UIKit.UIView.AnimationCurve
  public let position: MarqueeLabel.ScrollStep.Position
  public let edgeFades: MarqueeLabel.EdgeFade
  public init(timeStep: CoreGraphics.CGFloat, timingFunction: UIKit.UIView.AnimationCurve = .linear, position: MarqueeLabel.ScrollStep.Position, edgeFades: MarqueeLabel.EdgeFade)
}
public struct FadeStep : MarqueeLabel.MarqueeStep {
  public let timeStep: CoreGraphics.CGFloat
  public let timingFunction: UIKit.UIView.AnimationCurve
  public let edgeFades: MarqueeLabel.EdgeFade
  public init(timeStep: CoreGraphics.CGFloat, timingFunction: UIKit.UIView.AnimationCurve = .linear, edgeFades: MarqueeLabel.EdgeFade)
}
public struct EdgeFade : Swift.OptionSet {
  public let rawValue: Swift.Int
  public static var leading: MarqueeLabel.EdgeFade
  public static var trailing: MarqueeLabel.EdgeFade
  public init(rawValue: Swift.Int)
  public typealias ArrayLiteralElement = MarqueeLabel.EdgeFade
  public typealias Element = MarqueeLabel.EdgeFade
  public typealias RawValue = Swift.Int
}
