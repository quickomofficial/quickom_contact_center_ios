//
//  BWFCallCenterProtocol.h
//  BWFCallCenterFramework
//
//  Copyright © 2019 Kryptono LLC. All rights reserved.
//

#ifndef BWFCallCenterProtocol_h
#define BWFCallCenterProtocol_h

#import "BWFCallCenter.h"
#import "BWFCallCenterEnumDefine.h"

@class BWFRequestEntity;
@class BWFCallCenterError;
@protocol BWFCallCenterProtocol <NSObject>

@optional

//v2
- (void)bwfCallCenterDidStartWithIdentifier:(NSString * _Nullable)identifier;

/*!
 @abstract Start framework successfully
 */
- (void)bwfCallCenterDidStartAsRequesterWithIdentifier:(NSString * _Nullable)identifier;
- (void)bwfCallCenterDidStartAsSupporterWithIdentifier:(NSString * _Nullable)identifier;
- (void)bwfCallCenterDidStartAsEnterpriseWithIdentifier:(NSString * _Nullable)identifier;

/*!
 @abstract Stop framework successfully
 */
- (void)bwfCallCenterDidStop;

/*!
 @abstract Returned errors of framework
 @param error : returned error
 @discussion This method will be invoked if any error occurs within the framework
 @see For more detail, check
         + File 'BWFErrorDefine.h'
         + Class 'BWFError'
 */
- (void)bwfCallCenterFailedWithError:(BWFCallCenterError * _Nonnull)error;

/*!
 @abstract XMPP Message connect successfully
 @discussion This method will be invoked whenever xmpp message connect and authenticate successfully
 */
- (void)bwfCallCenterDidConnectMessage;


/*!
 @abstract Current active call changes to state Connected
 @discussion This method is for CALLER and CALLEE roles
 State 'Connected' is a state that CALLER and CALLEE are connected and they can talk together.
 CALLER and CALLEE are allowed to request video by calling following method
 + requestVideoPreviewAndVideoRemoteView:
 */
- (void)bwfCallCenterCallDidChangeToStateConnectedWith:(NSString * _Nullable)identifier
                                           isVideoCall:(BOOL)isVideoCall;


/*!
 @abstract Current active call changes to state End
 @discussion This method is for CALLER and CALLEE roles
 State 'End' is a state that call is disconnected between CALLER and CALLEE
 */
- (void)bwfCallCenterCallDidChangeToStateEnd:(NSTimeInterval)totalDuration;
- (void)bwfCallCenterCallDidChangeToStateEnd:(NSNumber * _Nonnull)totalDurationInSecond
                                   extraInfo:(NSDictionary * _Nullable)info;

//MARK: Receive Message and Status

/*!
 @abstract Receive message
 @param text : the text message
 messageId: an unique identifier of the message
 identifier: an unique id to identify an user on your platform. In this case, it's an identifier of SEND whom sends message to
 date: a date when receives the message
 @discussion Notify that there's an incoming text message.
 @see For more detail, check
 + File 'BWFErrorDefine.h'
 + Class 'BWFError'
 */
- (void)bwfCallCenterReceiveChatMessage:(NSDictionary * _Nonnull)content
                         conversationId:(NSString * _Nullable)convId
                              messageId:(NSString * _Nonnull)messageId
                                   from:(NSString * _Nonnull)identifier
                                 sender:(NSString * _Nullable)sender
                                   date:(NSDate * _Nullable)date
                               metaInfo:(NSDictionary * _Nullable)metaInfo;

- (void)bwfCallCenterReceiveTextMessage:(NSString * _Nonnull)text
                              messageId:(NSString * _Nonnull)messageId
                                   from:(NSString * _Nonnull)identifier
                                   date:(NSDate * _Nullable)date;

- (void)bwfCallCenterReceiveImageMessage:(NSString * _Nonnull)url
                               messageId:(NSString * _Nonnull)messageId
                                    from:(NSString * _Nonnull)identifier
                                    date:(NSDate * _Nullable)date;

- (void)bwfCallCenterReceiveTextMessage:(NSString * _Nonnull)text
                         conversationId:(NSString * _Nullable)convId
                              messageId:(NSString * _Nonnull)messageId
                                   from:(NSString * _Nonnull)identifier
                                   date:(NSDate * _Nullable)date;

- (void)bwfCallCenterReceiveImageMessage:(NSString * _Nonnull)url
                          conversationId:(NSString * _Nullable)convId
                               messageId:(NSString * _Nonnull)messageId
                                    from:(NSString * _Nonnull)identifier
                                    date:(NSDate * _Nullable)date;

- (void)bwfCallCenterReceiveTextMessage:(NSString * _Nonnull)text
                         conversationId:(NSString * _Nullable)convId
                              messageId:(NSString * _Nonnull)messageId
                                   from:(NSString * _Nonnull)identifier
                                 sender:(NSString * _Nullable)sender
                                   date:(NSDate * _Nullable)date
                               metaInfo:(NSDictionary * _Nullable)metaInfo;

- (void)bwfCallCenterReceiveImageMessage:(NSString * _Nonnull)url
                          conversationId:(NSString * _Nullable)convId
                               messageId:(NSString * _Nonnull)messageId
                                    from:(NSString * _Nonnull)identifier
                                  sender:(NSString * _Nullable)sender
                                    date:(NSDate * _Nullable)date
                                metaInfo:(NSDictionary * _Nullable)metaInfo;

- (void)bwfCallCenterReceiveCallNotification:(NSDictionary* _Nonnull)callInfo
                          conversationId:(NSString * _Nullable)convId
                               messageId:(NSString * _Nonnull)messageId
                                    from:(NSString * _Nonnull)identifier
                                  sender:(NSString * _Nullable)sender
                                    date:(NSDate * _Nullable)date
                                metaInfo:(NSDictionary * _Nullable)metaInfo;

/*!
 @abstract Receive status of message
 @param status : status of a message
 messageId: an unique identifier of the message
 @discussion Notify that there's an incoming message's status.
 @see For more detail, check
 + File 'BWFErrorDefine.h'
 + Class 'BWFError'
 */
- (void)bwfCallCenterReceiveStatus:(EnumBWFCallCenterMessageStatus)status
                      forMessageId:(NSString * _Nonnull)msgId
                              from:(NSString * _Nonnull)identifier;


//MARK: Receive Room Information

/*!
@abstract Receive member list of a room
@param memberChatIdList : an array (may be empty) of chat id, which are chatId of each member in the room
roomId: an unique identifier of the room
@discussion Return the member list with chat id for each member.
@see For more detail, check
+ File 'BWFErrorDefine.h'
+ Class 'BWFError'
*/

- (void)bwfCallCenterReceiveRoomMember:(NSArray *_Nullable) memberChatIdList
                             forRoomId:(NSString *_Nullable)roomId;

/*!
 @abstract Result for preparation a call from Caller
 @param identifier : the identify of user who will be connected with
 isVideoCall : call is voice only or video
 @discussion This method is for CALLER role
 This method will be invoked if the call from caller is ready to start after Caller called one of these following methods:
 + voiceCallTo:
 + videoCallTo:
 */
- (void)bwfCallCenterFinishCallPreparationTo:(NSString * _Nonnull)identifier
                                 isVideoCall:(BOOL)isVideoCall;


/*!
 @abstract Current active call changes to state Outgoing
 @discussion This method is for CALLER role
 State 'Outgoing' is a state that call is established on CALLER side and delivers signals to CALLEE side and CALLEE doesn't accept or deny the call.
 */
- (void)bwfCallCenterCallDidChangeToStateOutgoingTo:(NSString * _Nullable)identifier
                                        isVideoCall:(BOOL)isVideoCall;



/*!
 @abstract CALLEE receives incoming call signal
 @param identifier : an identify of user who sends call signal to CALLEE
 isVideoCall : call is voice only or video
 @discussion This method is for CALLEE role
 */
- (void)bwfCallCenterCallDidChangeToStateReceivedIncomingCallFrom:(NSString * _Nullable)identifier
                                                      isVideoCall:(BOOL)isVideoCall
                                                        extraInfo:(NSDictionary * _Nullable)extraInfo;




- (void)bwfCallCenterReceiveCustomMessage:(NSDictionary * _Nullable)info;

@end


@protocol BWFCallCenterForRequesterProtocol <NSObject>

@optional

- (void)bwfCallCenterDidFindSupporterViaRequest:(BWFRequestEntity * _Nonnull)request;
//- (void)bwfCallCenterTryToCallFoundSupporterForRequest:(BWFRequestEntity *)request;
//- (void)bwfCallCenterDidSuccessfullyCallToFoundSupporterForRequest:(BWFRequestEntity *)request;

- (void)bwfCallCenterDidTimeoutFindingSupporters;


/*!
 @abstract Notify when to show rating GUI
 @param completionHandler : a block that do rating
 @discussion + Show rating GUI once this method is invoked
 + Call the 'RatingCompletionHandler' to do the rating logic.
 + Showcase: Show a rating view GUI inside this method. Add a button named 'Rate' on this view. When user click the button, collect data that user entered (level of satisfaction, comment from user), call the 'RatingCompletionHandler' to rate
 */
- (void)bwfCallCenterShouldRateForLastSupport:(RatingCompletionHandler _Nonnull)completionHandler;

@end


@protocol BWFCallCenterForSupporterProtocol <NSObject>

@optional
- (void)bwfCallCenterDidReceiveRequest:(BWFRequestEntity * _Nonnull)request;

- (void)bwfCallCenterDidUpdateToOnlineStatus:(NSNumber * _Nonnull)isOnline;

- (void)bwfCallCenterDidDropRequest:(BWFRequestEntity * _Nonnull)request;

- (void)bwfCallCenterDidForceDropCurrentRequest;

- (void)bwfCallCenterDidTimeoutWaiting;

@end

@protocol BWFConferenceProtocol <NSObject>

- (void)bwfConferenceDidChangeToState:(EnumBWFConferenceState) state;

- (void)bwfConferenceDidUpdateMeta:(NSDictionary *_Nonnull)meta;
- (void)bwfConferenceDidUpdateRoomInfo:(NSDictionary *_Nonnull)meta;
- (void)bwfConferenceDidUpdateMemberInfo:(NSDictionary *_Nonnull)memberMap;

- (void)bwfConferenceDidConnect:(UIView *_Nonnull) streamView
                       StreamId:(NSString *_Nonnull) streamId
                           Meta:(NSDictionary *_Nullable) info;

- (void)bwfConferenceDidAddVideo:(UIView *_Nonnull) streamView
                        StreamId:(NSString *_Nonnull) streamId
                            Meta:(NSDictionary *_Nullable) info;

- (void)bwfConferenceDidMixLocalVideo:(UIView *_Nullable) streamView
                        StreamId:(NSString *_Nullable) streamId
                            Meta:(NSDictionary *_Nullable) info;

- (void)bwfConferenceDidLocalVideo:(UIView *_Nullable) streamView
                        StreamId:(NSString *_Nullable) streamId
                            Meta:(NSDictionary *_Nullable) info;

- (void)bwfConferenceDidUpdateVideo:(UIView *_Nullable) streamView
                        StreamId:(NSString *_Nonnull) streamId
                            Meta:(NSDictionary *_Nullable) info;

- (void)bwfConferenceDidSilent:(BOOL) isSilent;

- (void)bwfConferenceDidRemoveVideoForStream:(NSString *_Nonnull)streamId;

- (void)bwfConferenceDidReceiveCustomMessage:(NSDictionary *_Nullable) info;

- (void)bwfConferenceDidReceiveChatMessage:(NSDictionary *_Nullable) info;

- (void)bwfConferenceDidDeleteChatMessage:(NSDictionary *_Nullable) info;

@end

@protocol BWFNewCCProtocol <NSObject>

- (void)bwfNewCC_onRoutingMessage:(NSString *_Nonnull)msgName RequestId:(NSString *_Nonnull) requestId RequestInfo:(NSDictionary *_Nullable) info;

- (void)bwfNewCC_onFailedToInvite:(NSString *_Nullable)reason;
- (void)bwfNewCC_onFailedToForward:(NSString *_Nullable)reason;

- (void)bwfNewCC_onVideoEvent:(NSString *_Nonnull)eventName VideoInfo:(NSDictionary *_Nullable) info;

- (void)bwfNewCC_onMemberEvent:(NSString *_Nonnull)eventName
                          Data:(NSDictionary *_Nullable) info;

- (void)bwfNewCC_onCallStarted;

@end

#endif /* BWFCallCenterProtocol_h */
