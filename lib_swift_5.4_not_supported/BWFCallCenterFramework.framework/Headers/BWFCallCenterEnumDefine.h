//
//  BWFCallCenterEnumDefine.h
//  BWFCallCenterFramework
//
//  Copyright © 2019 Beowulf. All rights reserved.
//

#ifndef BWFCallCenterEnumDefine_h
#define BWFCallCenterEnumDefine_h


typedef NS_ENUM(NSUInteger, EnumBWFCallCenterRequestType) {
    BWFCallCenterRequestTypeChat    = 1, //Chat
    BWFCallCenterRequestTypeVoice   = 2, //Voice
    BWFCallCenterRequestTypeVideo   = 3, //Video
};

enum bwfcallcenter_message_status {
    BWFCallCenterMessageStatusSent        = 0, //Sender
    BWFCallCenterMessageStatusDelivered   = 1, //Sender
    BWFCallCenterMessageStatusSeen        = 2, //Sender receives Seen status from Receiver
    BWFCallCenterMessageStatusFailed      = 3, //Sender
};
typedef enum bwfcallcenter_message_status EnumBWFCallCenterMessageStatus;


enum bwfcallcenter_incoming_request_handle_type {
    EnumBWFCallCenterIncomingRequestHandleType_None = 0, //Receive incoming request, notify app immediately
    EnumBWFCallCenterIncomingRequestHandleType_Queue = 1, //Receive incoming request
};
typedef enum bwfcallcenter_incoming_request_handle_type EnumBWFCallCenterIncomingRequestHandleType;



enum bwfcallcenter_recording_type {
    EnumBWFCallCenterRecordingType_NoRecord = 0,
    EnumBWFCallCenterRecordingType_Video = 1,
    EnumBWFCallCenterRecordingType_Voice = 2,
    EnumBWFCallCenterRecordingType_Chat = 3
};
typedef enum bwfcallcenter_recording_type EnumBWFCallCenterRecordingType;




enum bwf_callcenter_push_content_type {
    BWFCallCenterPushContentTypeOutgoingCall   = 1, //Outgoing call
    BWFCallCenterPushContentTypeCancelOutgoingCallWithoutConnected = 2, //Cancel the outgoing call which isn't connected
    BWFCallCenterPushContentTypeTimeoutOutgoingCallWithoutConnected = 3, //The outgoing call is timeout before connected
    
    BWFCallCenterPushContentTypeIncomingCallIsAccepted = 4, //The incoming call is accepted by callee
    BWFCallCenterPushContentTypeIncomingCallIsDenied = 5, //The incoming call is denied by callee
    
    BWFCallCenterPushContentTypeIncomingChat   = 10, //incoming chat

};
typedef enum bwf_callcenter_push_content_type EnumBWFCallCenterPushContentType;

enum bwfconference_state {
    BWFConferenceStateInitializing      = 0, // Just start join conference
    BWFConferenceStateWaitingForHost    = 1, // Host not start room yet
    BWFConferenceStateConnecting        = 2, // Room started, do connect
    BWFConferenceStateConnected         = 3, // Main stream connected
    BWFConferenceStateEnded             = 4, // Host end or self end
    
    BWFConferenceStateRoomAuto              = 100, // roomJoinMode is Auto
    BWFConferenceStateRoomFull              = 101, // Room is FULL, hold on
    BWFConferenceStateRoomLocked            = 102, // Room is Locked, hold on
    BWFConferenceStateRoomUnlocked          = 103, // Room is Unlocked = Auto
    BWFConferenceStateRoomEnableWaiting     = 104, // Room is Enable Waiting, hold on
    BWFConferenceStateRoomDisableWaiting    = 105, // Room is Disable Waiting = Auto
};
typedef enum bwfconference_state EnumBWFConferenceState;



#endif /* BWFCallCenterEnumDefine_h */
