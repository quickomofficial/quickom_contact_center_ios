//
//  BWFCallCenterFramework.h
//  BWFCallCenterFramework
//
//  Copyright © 2019 Beowulf. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for BWFCallCenterFramework.
FOUNDATION_EXPORT double BWFCallCenterFrameworkVersionNumber;

//! Project version string for BWFCallCenterFramework.
FOUNDATION_EXPORT const unsigned char BWFCallCenterFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <BWFCallCenterFramework/PublicHeader.h>



#import "BWFErrorDefine.h"
#import "BWFCallCenterError.h"
#import "BWFCallCenterProtocol.h"
#import "BWFCallCenterEnumDefine.h"
#import "BWFCallCenter.h"
#import "BWFCallSpeakerView.h"
#import "BWFCallMicrophoneView.h"
#import "BWFRecordEntity.h"
#import "BWFGroupChatEntity.h"
#import "BWFChatEntity.h"


//#import "BWFCallCenter_v2.h"

