//
//  BWFCallCenter_InternalUsage.h
//  BWFCallCenterFramework
//
//  Created by Thai Tran on 6/11/19.
//  Copyright © 2019 Kryptono LLC. All rights reserved.
//

#ifndef BWFCallCenter_h
#define BWFCallCenter_h


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <PushKit/PushKit.h>

#import "BWFRequestEntity.h"

#define BWF_APP_ROLE_WE          @"BWF_APP_ROLE_WE"
#define BWF_APP_ROLE_CALLCENTER  @"BWF_APP_ROLE_CALLCENTER"
#define BWF_APP_ROLE_CONFERENCE  @"BWF_APP_ROLE_CONFERENCE"

NS_ASSUME_NONNULL_BEGIN


/*!
 @abstract A block instance to rate a call quality or call center service
 @param satisfyFraction : a fraction evaluates the level of satisfaction of customer
 comment: comment of customer
 resultBlock: result of rating
 @discussion + 'satisfyFraction' must be between 0 and 1.
 + Assign to 'satisfyFraction' a negative value will result to 0
 + Assign to 'satisfyFraction' a greater-than-one value will result to 1
 
 + Result of rating will return in resultBlock indicating rating is successful or not
 */
typedef void(^RatingCompletionHandler)(CGFloat satisfyFraction, NSString * _Nullable comment, void(^resultBlock)(BOOL));

@protocol BWFCallCenterProtocol;
@protocol BWFCallCenterForRequesterProtocol;
@protocol BWFCallCenterForSupporterProtocol;
@protocol BWFConferenceProtocol;
@protocol BWFNewCCProtocol;

@interface BWFCallCenter : NSObject


/*!
 @abstract THE MOST IMPORTANT METHOD
 @param apiKey : a valid apiKey
 @discussion Call this method before doing any actions of this framework
 If this method is not call, the framework can't be used
 Please contact us to get the apiKey
 */
+ (BOOL)configureWithApiKey:(NSString *)apiKey;

/*!
 @abstract Delegate to receive callback for result or error
 @param callcenterDelegate : a delegate object
 @discussion + Set the delegate before calling other methods to receive result or error
 + Must call after `+configureWithApiKey:`
 */
+ (void)setCallCenterDelegate:(id<BWFCallCenterProtocol>)callcenterDelegate;
+ (void)setCallCenterForRequesterDelegate:(id<BWFCallCenterForRequesterProtocol>)callcenterForRequesterDelegate;
+ (void)setCallCenterForSupporterDelegate:(id<BWFCallCenterForSupporterProtocol>)callcenterForSupporterDelegate;

+ (void)setConferenceDelegate:(id<BWFConferenceProtocol>)conferenceDelegate;

+ (void)setNewCCDelegate:(id<BWFNewCCProtocol>)newCCDelegate;


/*!
 @abstract Start the framework
 @param identifier : an unique id to identify an user on your platform
 @discussion This is an asynchronous method and takes a while to finish
 + Implement protocol `BWFCallCenterProtocol` to receive result or error
 + If framework starts successfully, following method will be invoked
 - (void)bwfCallCenterDidStartWithIdentifier:
 + If framework failed to start, following method will be invoked
 - (void)bwfCallCenterFailedWithError:
 + Possible error codes: 10, 20, 21, 22, 200, 201
 @see For more detail, check
 + File 'BWFErrorDefine.h'
 + Protocol 'BWFCallCenterProtocol'
 + Class 'BWFError'
 */
+ (void)startAsSupporterWithIdentifier:(NSString *)identifier;
+ (void)startAsEnterpriseWithIdentifier:(NSString *)identifier
                                AppRole:(NSString *_Nullable)role;

+ (void)startAsRequesterWithIdentifier:(NSString *)identifier Alias:(NSString *_Nonnull) alias;
+ (void)startAsAnonymousRequesterWithAppRole:(NSString *_Nullable)role;


/*!
 @abstract Check if framework is started and ready to use or not
 
 */
+ (BOOL)isReady;


/*!
 @abstract Stop the framework
 @discussion + Stop the call center framework.
 + Once the framework is stopped, any action won't be operated. Call method '+ startWithIdentifier:' to start framework again
 + Implement protocol `BWFCallCenterProtocol` to receive error if it occurs
 - (void)bwfCallCenterFailedWithError:
 + Possible error codes: 10, 20
 @see For more detail, check
 + File 'BWFErrorDefine.h'
 + Protocol 'BWFCallCenterProtocol'
 + Class 'BWFError'
 */
+ (void)stop;

+ (void)endCurrentSession;

+ (void)clearAllUnEndedCallId;

/*!
 @abstract Get the chat identifier of current account.
 @discussion + Get the chat identifier of current logged in account.
 @see For more detail, check
 */
+ (NSString * _Nullable)getChatId;

+ (NSString * _Nullable)getCurrentCallConversationId;

+ (NSString * _Nullable)getCurrentCallQrcode;

/*!
 @abstract Get video views
 @param resultBlock : a result block that returns video preview view and video remote view
 @discussion Video preview view is view that display your moments
 Video remote view is view that display call partner's moments
 Param 'previewView' and 'remoteView' can be nil in following cases
 + No call is established and connected yet
 + Current call is audio only
 ONLY call this method when current call is in state Connected. Call is connected when the following method of protocol 'BWFCallingProtocol' is invoked
 + (void)bwfCalling_CallDidChangeToStateConnected
 Following error codes in 'EnumBWFCallingError' may be returned
 + BWFCallingError_Framework_Not_Configured: This error usually occurs in case the method '+ configureWithApiKey:' isn't called yet.
 @see enum 'EnumBWFCallingError', protocol 'BWFCallingProtocol' for more detail
 */
+ (void)requestVideoPreviewAndVideoRemoteView:(void(^)(UIView * _Nullable previewView, UIView * _Nullable remoteView))resultBlock;
+ (void)requestVideoPreviewView:(void(^)(UIView * _Nullable previewView))resultBlock;
+ (void)requestVideoRemoteView:(void(^)(UIView * _Nullable remoteView))resultBlock;
+ (void)cancelVideoPreviewAndVideoRemoteView;

/*!
 @abstract Get duration of current call at the moment
 @discussion Get actual duration of current call
 */
+ (NSInteger)durationOfCurrentCall;


/*!
 @abstract End current call
 */
+ (void)endCurrentCall;
+ (void)endSupporting;

+ (void)forceEndCurrentCall;

/*!
 @abstract Switch between front camera and rear camera
 @discussion This method will have no effect in following cases
 + No call is established and connected yet
 + Current call is audio only
 Following error codes in 'EnumBWFCallingError' may be returned
 + BWFCallingError_Framework_Not_Configured: This error usually occurs in case the method '+ configureWithApiKey:' isn't called yet.
 */
+ (void)switchPreviewCamera;

+ (void)switchPreviewCamera:(BOOL)useBackCamera;

+ (void)mirroringCamera:(BOOL)mirror;

+ (BOOL)isMirroringCamera;

+ (void)updatePreviewCameraOrientation;

+ (void)updateCameraFilter:(NSString *_Nullable)filterName;
+ (void)updateVideoBackground:(NSDictionary *_Nullable)dict Mode:(NSString *_Nonnull)backgroundMode;
+ (UIImage *_Nullable)getPreviewVirtualBackground:(NSDictionary *_Nullable)dict ForImage:(UIImage *_Nullable)image;

+ (BOOL)currentCameraStatus;    // NO: off, YES: on

+ (BOOL)currentCameraPosition;  // NO: front, YES: back

+ (void)setPreviewCameraState:(BOOL)state;

+ (void)turnPreviewCamera:(BOOL)on;

+ (void)updateCameraStatus:(BOOL) isOn;

+ (BOOL)toggleMicrophone;

+ (void)setMuteMicrophone:(BOOL)isMuted;

+ (BOOL)isRemoteVideoStatic;

+ (BOOL)isInAnyCallNow;

+ (void)havePendingCall:(void(^)(BOOL result))resultBlock;

+ (bool)checkLastCallHasTurnAvailable;

+ (void)sendNotepadData:(NSDictionary *)info;

+ (void)sendGroupInCallData:(NSDictionary *)info;



+ (void)endCurrentConversation;

+ (void)sendText:(NSString *)text
    toIdentifier:(NSString *)identifier
     resultBlock:(void(^)(NSString* _Nullable messageId))resultBlock;

+ (void)sendImage:(NSString *)url
     toIdentifier:(NSString *)identifier
      resultBlock:(void(^)(NSString* _Nullable messageId))resultBlock;


+ (void)sendText:(NSString *)text
    toIdentifier:(NSString *)identifier
withConversation:(NSString *)conversationId
         isGroup:(BOOL) isGroup
     resultBlock:(void(^)(NSString* _Nullable messageId))resultBlock;

+ (void)sendImage:(NSString *)url
     toIdentifier:(NSString *)identifier
 withConversation:(NSString *)conversationId
          isGroup:(BOOL) isGroup
      resultBlock:(void(^)(NSString* _Nullable messageId))resultBlock;

+ (void)sendImage:(NSString *)url
      ofMessageId:(NSString *) msgId
     toIdentifier:(NSString *)identifier
 withConversation:(NSString *)conversationId
          isGroup:(BOOL) isGroup
      resultBlock:(void(^)(NSString* _Nullable messageId))resultBlock;

+ (void)sendContent:(NSDictionary *)content
        ofMessageId:(NSString *)msgId
       toIdentifier:(NSString *)identifier
   withConversation:(NSString *)conversationId
            isGroup:(BOOL)isGroup
        resultBlock:(void(^)(NSString* _Nullable messageId))resultBlock;

/*!
 Chat One-on-One directly
 */
+ (void)startChatSessionIfNeededWith:(NSDictionary *_Nullable)inputChatAccountInfo completion:(void(^)(void))resultBlock;

+ (void)startChatSessionIfNeededWith:(NSDictionary *_Nullable)inputChatAccountInfo
                               alias:(NSString *_Nullable)alias
                          completion:(void(^)(void))resultBlock;

+ (void)sendText:(NSString *)text
        toChatId:(NSString *)identifier
     inputConvId:(NSString * _Nullable)inputConversationId
        metaInfo:(NSDictionary *_Nullable)metaInfo
     resultBlock:(void(^)(NSString* _Nullable messageId, NSString* _Nullable conversationId))resultBlock;

+ (void)sendImage:(NSString *)url
         toChatId:(NSString *)identifier
      inputConvId:(NSString * _Nullable)inputConversationId
         metaInfo:(NSDictionary *_Nullable)metaInfo
      resultBlock:(void(^)(NSString* _Nullable messageId, NSString* _Nullable conversationId))resultBlock;

+ (void)sendImage:(NSString *)url
      ofMessageId:(NSString *) msgId
         toChatId:(NSString *)identifier
      inputConvId:(NSString * _Nullable)inputConversationId
         metaInfo:(NSDictionary *_Nullable)metaInfo
      resultBlock:(void(^)(NSString* _Nullable messageId, NSString* _Nullable conversationId))resultBlock;

+ (void)sendContent:(NSDictionary *)content
        ofMessageId:(NSString *) msgId
           toChatId:(NSString *)identifier
        inputConvId:(NSString * _Nullable)inputConversationId
           metaInfo:(NSDictionary *_Nullable)metaInfo
        resultBlock:(void(^)(NSString* _Nullable messageId, NSString* _Nullable conversationId))resultBlock;
 
+ (void)sendSeenStatusForMessageId:(NSString *)messageId
                      toChatId:(NSString *)identifier;

+ (NSString * _Nullable)getOneOneConversationId:(NSString* )remoteChatId;

+ (NSString * _Nullable)getConnectedChatId:(NSString *)identifier;

+ (NSString *)getAnonymousConversationId;

/*!
 @abstract Send text message
 @param messageId: an unique identifier of the message
 identifier: an unique id to identify an user on your platform. In this case, it's an identifier of RECEIVER whom the message will be send to
 @discussion This is asynchronous method
 - Param 'messageId' can be nil in case unable to send message
 - Errors may be returned in case failure via following method of protocol 'BWFMessageProtocol'
 + Errors may be returned in case failure via following method of protocol 'BWFMessageProtocol'
 - (void)bwfMessageFailedWithError:
 + Possible error codes: 10, 11, 23, 202, 203
 @see For more detail, check
 + File 'BWFErrorDefine.h'
 + Protocol 'BWFMessageProtocol'
 + Class 'BWFError'
 */
+ (void)sendSeenStatusForMessageId:(NSString *)messageId toIdentifier:(NSString *)identifier;

+ (void)joinChatRoom:(NSString *)roomId Password:(NSString *)roomPassword LastActive:(NSDate * _Nullable)lastActive;

+ (void)loadChatRoom:(NSString *)roomId;

+ (void)leaveChatRoom:(NSString *)roomId;

+ (void)receiveHelpResponseFromPushkitOnBackground:(NSDictionary *)data;

@end

@interface BWFCallCenter (Requester)

+ (void)setNumberOfRetry:(NSInteger)numOfRetry; //Default is 1 (retry 1 time)

//
+ (void)requestVoiceCallSupportWithUserInfo:(NSDictionary * _Nullable)customInfo
                                    toGroup:(NSString * _Nonnull)groupName;
+ (void)requestVideoCallSupportWithUserInfo:(NSDictionary * _Nullable)customInfo
                                    toGroup:(NSString * _Nonnull)groupName;


//QR Code
+ (void)requestVoiceCallSupportViaQRCode:(NSString *)apiURL
                        inputChatAccount:(NSDictionary * _Nullable)inputChatAccountInfo
                            withUserInfo:(NSDictionary * _Nullable)customInfo;
+ (void)requestVideoCallSupportViaQRCode:(NSString *)apiURL
                        inputChatAccount:(NSDictionary * _Nullable)inputChatAccountInfo
                            withUserInfo:(NSDictionary * _Nullable)customInfo;
+ (void)requestChatSupportViaQRCode:(NSString *)apiURL
                            withUserInfo:(NSDictionary * _Nullable)customInfo;

+ (void)requestChatWithDisplayName:(NSString * _Nullable)name
                           toGroup:(NSString * _Nonnull)groupName;

+ (void)cancelRequestSupporter;



@end


@interface BWFCallCenter (Supporter)

+ (BOOL)isOnlineNow;
+ (void)goOnline;
+ (void)goOffline;

+ (BOOL)acceptIncomingRequest:(BWFRequestEntity * _Nonnull)request;
+ (void)denyIncomingRequest:(BWFRequestEntity * _Nonnull)request;

+ (void)acceptCallkitRequestWith:(NSString* _Nonnull)requestId UserChatId:(NSString * _Nonnull)userChatId;
+ (void)acceptCallkitRequestWith:(BWFRequestEntity *)request;

+ (void)denyCallkitRequestWith:(NSString* _Nonnull)requestId UserChatId:(NSString * _Nonnull)userChatId;
+ (void)denyCallkitRequestWith:(BWFRequestEntity *)request;

/*!
 @abstract Accept incoming call
 @discussion Call this method to accept an incoming call
 Implement protocol 'BWFCallingProtocol' to receive result or error
 Following error codes in 'EnumBWFCallingError' are returned in case failed
 + BWFCallingError_Framework_Not_Configured: This error usually occurs in case the method '+ configureWithApiKey:' isn't called yet.
 @see enum 'EnumBWFCallingError', protocol 'BWFCallingProtocol' for more detail
 */
+ (void)acceptIncomingCall;


/*!
 @abstract Deny incoming call
 @discussion Call this method to deny an incoming call
 Implement protocol 'BWFCallingProtocol' to receive result or error
 Following error codes in 'EnumBWFCallingError' are returned in case failed
 + BWFCallingError_Framework_Not_Configured: This error usually occurs in case the method '+ configureWithApiKey:' isn't called yet.
 @see enum 'EnumBWFCallingError', protocol 'BWFCallingProtocol' for more detail
 */
+ (void)denyIncomingCall;

+ (void)endSupporting;


@end

@interface BWFCallCenter (Conference)

/*!
 @abstract Allow Enterprise can have their own domain
 @param accApiDomain:  domain for account api
 @param confApiDomain: domain for conference api
 @param chatDomain: domain for chat socket
 @param signalPeerDomain: domain for peer one-one
 @param signalControllerDomain: domain for signal controller
 @param callConfigDomain: domain for call config
 */
+ (void)updateEnterpriseDomainWithAccApi:(NSString *_Nonnull)accApiDomain
                           ConferenceApi:(NSString *_Nonnull)confApiDomain
                              ChatDomain:(NSString *_Nonnull)chatDomain
                        SignalPeerDomain:(NSString *_Nonnull)signalPeerDomain
                  SignalControllerDomain:(NSString *_Nonnull)signalControllerDomain
                        CallConfigDomain:(NSString *_Nonnull)callConfigDomain;

+ (void)requestConferenceViaQRCode:(NSString *)alias withAuthenInfo:(NSDictionary* _Nullable)authenInfo UserInfo:(NSDictionary *_Nullable)userInfo;
+ (void)endConference;
+ (void)leaveConference;

+ (BOOL)isInConferencing;
+ (NSInteger)durationOfCurrentConference;

+ (void)raiseHand:(BOOL) raise;
+ (void)selectInterpreter:(NSString *_Nullable) interpreterId;
+ (void)updateName:(NSString *_Nonnull) newName;
+ (void)sendCustomMessage:(NSDictionary *) dict ToReceiver:(NSString *_Nullable)receiver;
+ (NSString *)sendChatMessage:(NSDictionary *) dict ToReceiver:(NSString *_Nullable)receiver;

+ (void)startLive;
+ (void)stopLive;

+ (void)requestNewContainer;



// Hosting
+ (void)startConferenceViaQRCode:(NSString *)alias withAuthenInfo:(NSDictionary* _Nullable)authenInfo UserInfo:(NSDictionary *_Nullable)userInfo Mode:(NSString *_Nonnull)startMode;
+ (void)updateRoomJoinMode:(NSString *_Nonnull)joinMode;
+ (void)invitePresenter:(NSString *_Nonnull)memberId;
+ (void)removePresenter:(NSString *_Nonnull)memberId;
+ (void)makeInterpreter:(NSString *_Nonnull)memberId Channel:(NSString *_Nonnull)channel;
+ (void)removeInterpreter:(NSString *_Nonnull)memberId;
+ (void)doPinMember:(NSString *_Nonnull)memberId Pin:(BOOL)pinning;
+ (void)pinVideo:(NSString *_Nonnull)memberId;
+ (void)stopVideoOfMember:(NSString *_Nonnull)memberId;
+ (void)askEnableVideoOfMember:(NSString *_Nonnull)memberId;
+ (void)stopAudioOfMember:(NSString *_Nonnull)memberId;
+ (void)askEnableAudioOfMember:(NSString *_Nonnull)memberId;
+ (void)kickMember:(NSString *_Nonnull)memberId;
+ (void)makeCoHost:(NSString *_Nonnull)memberId;
+ (void)removeCoHost:(NSString *_Nonnull)memberId;
+ (void)goInvisible;
+ (void)goVisible;

+ (void)startLiveStreamingOn:(NSString *_Nonnull)platform StreamUrl:(NSString *_Nullable)sUrl StreamKey:(NSString *_Nonnull)sKey;
+ (void)stopLiveStreamingOn:(NSString *_Nonnull)platform;

@end

@interface BWFCallCenter (NewCC)

// flow
+ (void)startCCCall;
+ (void)endCurrentCCCall;
+ (void)endCCSupporting;
+ (void)cancelCurrentCCCall;
+ (BOOL)acceptIncomingNewCC:(NSDictionary *)request;
+ (void)denyIncomingNewCC:(NSDictionary *)request;
+ (void)registerCallStatus:(NSString *)requestId;
+ (void)registerInvitingStatus:(NSString *)messageId;
+ (void)getOnlineSupporter:(NSDictionary *_Nonnull)listUser completion:(void(^)(NSDictionary * _Nonnull result))resultBlock;
+ (void)forwardToSupporter:(NSDictionary *_Nonnull) info;
+ (void)cancelForwardToSupporter:(NSDictionary *_Nonnull) info;

+ (void)inviteToSupporter:(NSDictionary *_Nonnull) info;
+ (void)cancelInviteToSupporter:(NSDictionary *_Nonnull) info;

// info
+ (BOOL)isNewCCReady;
+ (BOOL)isInCCCall;
+ (NSInteger)durationOfCurrentCCCall;
+ (void)updatePersonalInfo:(NSDictionary *)info;

// camera
+ (void)turnCamera_CCCall:(BOOL)on;
+ (void)switchPreviewCamera_CCCall;
+ (void)switchPreviewCamera_CCCall:(BOOL) useBackCamera;


// MARK: CCSDK

+ (void) ccsdk_getQrCodeShortDetail:(NSString *)alias
                   completion:(void(^)(NSDictionary * _Nullable result))resultBlock;

+ (void) ccsdk_verifyCall:(NSString *)alias CallType:(NSString *)callType
               completion:(void(^)(NSDictionary * _Nullable result))resultBlock;

+ (void) ccsdk_makeCall:(NSString *)alias
               CallType:(NSString *)callType
       inputChatAccount:(NSDictionary *_Nullable)inputChatAccountInfo
               UserInfo:(NSDictionary *)userInfo
                Payload:(NSDictionary *)payload;

+ (void) ccsdk_getAnyAccountInfo:(NSString *)chatId
                      completion:(void(^)(NSDictionary * _Nullable result))resultBlock;

+ (void) ccsdk_checkGroupChatWith:(NSString *_Nullable)phone
                            Email:(NSString *_Nullable) email
                             Name:(NSString *_Nonnull)name
                           QRCode:(NSString *_Nonnull)qrcode
                       completion:(void(^)(NSDictionary * _Nullable result))resultBlock;

+ (void) ccsdk_claimGroupChatWith:(NSString *_Nullable)phone
                            Email:(NSString *_Nullable) email
                             Name:(NSString *_Nonnull)name
                           QRCode:(NSString *_Nonnull)qrcode
                             Code:(NSString *_Nonnull)code
                       completion:(void(^)(NSDictionary * _Nullable result))resultBlock;

+ (void) ccsdk_getGroupChatDetail:(NSString *_Nullable)roomId
                           QRCode:(NSString *_Nullable)qrcode
                       completion:(void(^)(NSDictionary * _Nullable result))resultBlock;

+ (void) ccsdk_createGroupChatWith:(NSString *_Nullable)phone
                             Email:(NSString *_Nullable) email
                              Name:(NSString *_Nonnull)name
                            QRCode:(NSString *_Nonnull)qrcode
                        completion:(void(^)(NSDictionary * _Nullable result))resultBlock;

+ (void) ccsdk_refreshGroupChatWith:(NSString *_Nonnull)roomId
                         completion:(void(^)(NSDictionary * _Nullable result))resultBlock;

+ (void) ccsdk_updateLastSeenGroupChat:(NSString *_Nonnull)roomId
                          lastSeenTime:(double) lastSeen
                            completion:(void(^)(NSDictionary * _Nullable result))resultBlock;

+ (void) ccsdk_updateLastestMsgGroupChat:(NSString *_Nonnull)roomId
                          lastestMsgTime:(double) lastestTime
                              completion:(void(^)(NSDictionary * _Nullable result))resultBlock;

+ (void) ccsdk_pingKeepChatId:(NSString *_Nonnull)chatId
                   completion:(void(^)(NSDictionary * _Nullable result))resultBlock;

+ (void) ccsdk_sendGeneralPushTo:(NSArray *_Nonnull)chatIds
                            Type:(NSString *_Nonnull)type
                           Title:(NSString *_Nonnull)title
                         Message:(NSString *_Nonnull)message
                       ExtraInfo:(NSDictionary *_Nullable)extraInfo
                      completion:(void(^)(NSDictionary * _Nullable result))resultBlock;

+ (void) ccsdk_uploadPhoto:(UIImage *_Nonnull)image
              Conversation:(NSString *_Nonnull)conversationId
                    ChatId:(NSString *_Nonnull)userChatId
                completion:(void(^)(NSString * _Nullable result))resultBlock;

@end


NS_ASSUME_NONNULL_END


#endif /* BWFCallCenter_InternalUsage_h */
