//
//  BWFCallMicrophoneView.h
//  BWFCallCenterFramework
//
//  Copyright © 2019 Beowulf. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BWFCallMicrophoneView : UIView {
    BOOL disallow;
    BOOL disabled;
}

- (BOOL)isDisallow;
- (BOOL)isDisabled;

- (BOOL)isMuted;
- (void)setMutedState:(BOOL) isMute;

/*!
 @abstract Set image for microphone states on/off
 */
- (void)setImageForStateOn:(UIImage * _Nullable)img;
- (void)setImageForStateOff:(UIImage * _Nullable)img;

/*!
 @abstract Force microphone button to update state
 */
- (void)update;

/*!
 @abstract Disallow microphone button
 */
- (void)setDisallow:(BOOL) isDisallow;
- (void)setDisabled:(BOOL) isDisabled;

/*!
 @abstract Force turn on microphone
 */
- (void)forceTurn:(BOOL) isOn;



@end

NS_ASSUME_NONNULL_END
