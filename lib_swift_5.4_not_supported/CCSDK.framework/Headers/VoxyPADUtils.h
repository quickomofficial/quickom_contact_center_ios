//
//  VoxyPADUtils.h
//  VoxyPADFramework
//
//  Created by VoxyPAD
//  Copyright (c) 2015 VoxyPAD Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "MBTVKProgressHUD.h"

@interface VoxyPADUtils : NSObject
{
    
}

#ifdef BWF_SDK
#else
/*!
 @abstract Methods related to file
 @discussion These methods allow
             +bundleFile: to get file from main bundle
             +documentFile: to get file from disk
             +deleteFile: delete file from disk
             +extensionOfFile: get extension from file name
             +createDicrectoryInDocumentIfNeeded: create a directory in document directory in disk
             +getDocumentDirectory: get the document directory
             +getImageFromDiskAtPath: get image from disk at path
 */
+ (NSString*)bundleFile:(NSString*)file;
+ (NSString*)documentFile:(NSString*)file;
+ (BOOL)deleteFile:(NSString *)path;
+ (NSString *)extensionOfFile:(NSString *)path;
+ (BOOL)createDicrectoryInDocumentIfNeeded:(NSString *)dirName;
+ (NSString *)getDocumentDirectory;
+ (UIImage *)getImageFromDiskAtPath:(NSString *)path;

/*!
 @abstract Method related to play a sound
 @discussion Play a sound
 */
+ (void)playSound:(NSString *)name ext:(NSString *)ext;

/*!
 @abstract Show Alert
 */
+ (void)showInfoAlertWithTitle:(NSString *)title
                       content:(NSString *)content
                   buttonTitle:(NSString *)button;
+ (void)showInfoAlertWithTitle:(NSString *)title
                       content:(NSString *)content
                fromController:(UIViewController *)controller
                 button1_title:(NSString * _Nullable)button1
                 button1_block:(void(^ _Nullable)(void))block1
                 button2_title:(NSString * _Nullable)button2
                 button2_block:(void(^ _Nullable)(void))block2;
+ (void)showInfoAlertWithTitle:(NSString * _Nullable)title
                       content:(NSString * _Nullable)content
                fromController:(UIViewController *)controller
                 button1_title:(NSString * _Nullable)button1
                 button1_block:(void(^ _Nullable)(void))block1
                 button2_title:(NSString * _Nullable)button2
                 button2_block:(void(^ _Nullable)(void))block2
                 button3_title:(NSString * _Nullable)button3
                 button3_block:(void(^ _Nullable)(void))block3;


/*!
 @abstract Methods related to show HUD
 @discussion These methods allow to show or hide HUD on a specific view
 */
+ (void)showHUD:(NSString*)message inView:(UIView *)view;
+ (void)showHUD:(NSString *)message inView:(UIView *)view hideAfterDelay:(NSTimeInterval)time;
+ (void)showInfoHUD:(NSString*)message inView:(UIView *)view;
+ (void)showInfoHUD:(NSString *)message inView:(UIView *)view font:(UIFont *)font hideAfterDelay:(NSTimeInterval)time;

//+ (MBTVKProgressHUD *)showHorizontalProgressHUD:(NSString *)message inView:(UIView *)view;
//+ (MBTVKProgressHUD *)showAnnularProgressHUD:(NSString *)message inView:(UIView *)view;

+ (void)hideAllHudInView:(UIView *)view;

#endif

/*!
 @abstract RESTful API
 @discussion These methods allow to GET/POST using RESTful API synchronously or asynchronously
 */
+ (id)syncGET_RESTfulAPI_from:(NSString *)apiURL
                   uploadJSON:(NSData *)jsonData;
+ (id)syncGET_RESTfulAPI_from:(NSString *)apiURL
             uploadDictionary:(NSDictionary *)paramDictionary;

+ (id)syncPOST_RESTfulAPI_from:(NSString *)apiURL
                    uploadJSON:(NSData *)jsonData;
+ (id)syncPOST_RESTfulAPI_from:(NSString *)apiURL
              uploadDictionary:(NSDictionary *)paramDictionary;

//No http response code
+ (void)asyncGET_RESTfulAPI_from:(NSString *)apiURL
                      uploadJSON:(NSData *)jsonData
                      completion:(void(^)(id))completion;
+ (void)asyncGET_RESTfulAPI_from:(NSString *)apiURL
                uploadDictionary:(NSDictionary *)paramDictionary
                      completion:(void(^)(id))completion;
+ (void)asyncGET_RESTfulAPI_from:(NSString *)apiURL
                uploadDictionary:(NSDictionary *)paramDictionary
                    uploadHeader:(NSDictionary *)headerDictionary
                      completion:(void(^)(id))completion;
+ (void)asyncGET_RESTfulAPI_from:(NSString *)apiURL
                uploadDictionary:(NSDictionary *)paramDictionary
                    uploadHeader:(NSDictionary *)headerDictionary
                  completionFull:(void(^)(NSURLResponse*, id))completion;

+ (void)asyncPOST_RESTfulAPI_from:(NSString *)apiURL
                       uploadJSON:(NSData *)jsonData
                       completion:(void(^)(id))completion;
+ (void)asyncPOST_RESTfulAPI_from:(NSString *)apiURL
                       uploadJSON:(NSData *)jsonData
                     uploadHeader:(NSDictionary *)headerDictionary
                       completion:(void(^)(id))completion;
+ (void)asyncPOST_RESTfulAPI_from:(NSString *)apiURL
                 uploadDictionary:(NSDictionary *)paramDictionary
                       completion:(void(^)(id))completion;
+ (void)asyncPOST_RESTfulAPI_from:(NSString *)apiURL
                 uploadDictionary:(NSDictionary *)paramDictionary
                     uploadHeader:(NSDictionary *)headerDictionary
                       completion:(void(^)(id))completion;
+ (void)asyncPOST_RESTfulAPI_from:(NSString *)apiURL
                 uploadDictionary:(NSDictionary *)paramDictionary
                     uploadHeader:(NSDictionary *)headerDictionary
                   completionFull:(void(^)(NSURLResponse*, id))completion;

+ (void)asyncPOST_RESTfulAPI_from:(NSString *)apiURL
                       postString:(NSString *)postString
                     uploadHeader:(NSDictionary *)headerDictionary
                   completionFull:(void(^)(NSURLResponse*, id))completion;

+ (void)asyncGET_RESTfulAPI_from:(NSString *)apiURL
                      postString:(NSString *)postString
                    uploadHeader:(NSDictionary *)headerDictionary
                  completionFull:(void(^)(NSURLResponse*, id))completion;

+ (void)asyncPOSTForm_RESTfulAPI_from:(NSString *)apiURL
                           postString:(NSString *)postString
                         uploadHeader:(NSDictionary *)headerDictionary
                       completionFull:(void(^)(NSURLResponse*, id))completion;

+ (void)asyncPUT_RESTfulAPI_from:(NSString *)apiURL
                uploadDictionary:(NSDictionary *)paramDictionary
                    uploadHeader:(NSDictionary *)headerDictionary
                  completionFull:(void(^)(NSURLResponse*, id))completion;

+ (void)asyncDELETE_RESTfulAPI_from:(NSString *)apiURL
                   uploadDictionary:(NSDictionary *)paramDictionary
                       uploadHeader:(NSDictionary *)headerDictionary
                     completionFull:(void(^)(NSURLResponse*, id))completion;

//Has http response code
//+ (void)asyncGET_RESTfulAPI_from:(NSString *)apiURL
//                      uploadJSON:(NSData *)jsonData
//  completionWithHttpResponseCode:(void(^)(NSInteger httpResponseCode, id data))completion;
//+ (void)asyncGET_RESTfulAPI_from:(NSString *)apiURL
//                uploadDictionary:(NSDictionary *)paramDictionary
//  completionWithHttpResponseCode:(void(^)(NSInteger httpResponseCode, id data))completion;
//+ (void)asyncGET_RESTfulAPI_from:(NSString *)apiURL
//                uploadDictionary:(NSDictionary *)paramDictionary
//                    uploadHeader:(NSDictionary *)headerDictionary
//  completionWithHttpResponseCode:(void(^)(NSInteger httpResponseCode, id data))completion;
//
//+ (void)asyncPOST_RESTfulAPI_from:(NSString *)apiURL
//                       uploadJSON:(NSData *)jsonData
//   completionWithHttpResponseCode:(void(^)(NSInteger httpResponseCode, id data))completion;
//+ (void)asyncPOST_RESTfulAPI_from:(NSString *)apiURL
//                       uploadJSON:(NSData *)jsonData
//                     uploadHeader:(NSDictionary *)headerDictionary
//   completionWithHttpResponseCode:(void(^)(NSInteger httpResponseCode, id data))completion;
//+ (void)asyncPOST_RESTfulAPI_from:(NSString *)apiURL
//                 uploadDictionary:(NSDictionary *)paramDictionary
//   completionWithHttpResponseCode:(void(^)(NSInteger httpResponseCode, id data))completion;
//+ (void)asyncPOST_RESTfulAPI_from:(NSString *)apiURL
//                 uploadDictionary:(NSDictionary *)paramDictionary
//                     uploadHeader:(NSDictionary *)headerDictionary
//   completionWithHttpResponseCode:(void(^)(NSInteger httpResponseCode, id data))completion;


/*!
 @abstract Methods related to do utilities
 @discussion
 */
+ (BOOL)isNotIphone3G;

/*!
 @abstract Get current device locale
 @discussion
 */
+ (NSString *)currentLocale;

/*!
 @abstract Resign current keyboard responder
 @discussion
 */
+ (void)resignKeyboardResponder;

@end
