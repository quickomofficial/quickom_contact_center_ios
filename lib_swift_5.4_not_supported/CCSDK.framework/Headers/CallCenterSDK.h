//
//  CallCenterSDK.h
//  CCSDK
//
//  Created by Kinh Tran on 31/8/21.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

#define error_framework_notstarted      @"Framework not started"

#define error_input_invalidUrl          @"Input url is invalid"
#define error_input_invalidId           @"Input userId is invalid"
#define error_input_invalidEmail        @"Input email is invalid"
#define error_input_invalidPhone        @"Input phone number is invalid"

#define error_callcenter_notExist       @"Call center not exist"
#define error_callcenter_notAllowCall   @"Call center not allow to call"
#define error_callcenter_notAllowChat   @"Call center not allow to chat"
#define error_callcenter_requestfirst   @"Must call requestCallCenterInfo first"

@protocol CallCenterSDKProtocol;

typedef enum callOption : NSUInteger {
    NORMAL,
    CALL,
} CallOption;

typedef enum errorCode : NSUInteger {
    None = 0,
    InvalidUrl = 10100,
    InvalidId = 10101,
    InvalidEmail = 10102,
    InvalidPhone = 10103,
    CallCenterNotExist = 20100,
    CallCenterNotAllowCall = 20101,
    CallCenterNotAllowChat = 20102,
    CallCenterMustRequestFirst = 20103,
    FrameworkNotStarted = 90100,
} ErrorCode;

@interface CallCenterSDK : NSObject

+ (CallCenterSDK *)session;

+ (void)setCCSDKDelegate:(id<CallCenterSDKProtocol>)delegate;

/// Start SDK.
///
/// - Description: Call this method once and before any other methods.
+ (void) startSDK;

/// Set desire language
///
/// - Description: Set a desire language for display
/// - Return: true if languageCode is supported, false if not supported
+ (BOOL) setDesiredLanguage:(NSString *)languageCode;

/// Get supported language
///
/// - Description: provide a list of supported language by the SDK
/// - For example: ["en", "vi"]
+ (NSArray *) supportedLanguages;

/// Call this method to check if there is a call is running
///
/// - Returns: true if call is running (even just make call), false if no call is running
+ (BOOL) isInCall;

/// Request call center info from given `url`.
///
/// - Warning: Invalid url may also give empty info.
/// - Parameter url: The call center url copied from link or scanned from QR Code
/// - Returns: Implement your code in the completion block.
///
/// # Sample info  #
/// ```
///     [
///         "name" : "call center label",
///         "allowChat" : true,
///         "allowCall" : true
///     ]
/// ```
/// ```
///     [
///         "error" : 10100,
///         "errorMessage" : "Input url is invalid"
///     ]
/// ```
/// ```
///     [
///         "error" : 20100,
///         "errorMessage" : "Call center not exist"
///     ]
/// ```
+ (void) requestCallCenterInfoFrom:(NSString *_Nonnull)url
                        completion:(void(^)(NSDictionary * _Nullable result))resultBlock;

/// Make request to call center with given `url`.
///
/// - Description: Check the call center info and offer user options to make decisions or call directly
///
/// - Warning: Invalid url will return error.
/// - Parameter url: The call center url copied from link or scanned from QR Code
/// - Parameter userId: Custom id for any purpose
/// - Parameter email: customer's email address
/// - Parameter phone: customer's phone number
/// - Parameter claimCode: used to claim old chat history
/// - Parameter callOption: 'NORMAL' to offer options to make call/chat, 'CALL' to call directly
/// - Returns: Check for error in completion block if any
/// # Sample info  #
///
/// ```     Success
///     [
///
///     ]
/// ```
///
/// ```     Error
///     [
///         "error" : 20100,
///         "errorMessage" : "call center not exist"
///     ]
/// ```
/// ```
///     [
///         "error" : 20101,
///         "errorMessage" : "call center not allow to call",
///     ]
/// ```
/// ```
///     [
///         "error" : 20102,
///         "errorMessage" : "call center not allow to chat",
///     ]
/// ```
+ (void) startCallCallCenterWith:(NSString *_Nonnull)url
                         UserID:(NSString *_Nullable)userId
                           Name:(NSString *_Nullable)name
                          Email:(NSString *_Nullable)email
                          Phone:(NSString *_Nullable)phone
                      ClaimCode:(NSString *_Nullable)claimCode
                      CallOption:(CallOption) callOption
                     completion:(void(^)(NSDictionary * _Nullable result))resultBlock;

/// Make call to call center with given `url`.
///
/// - Warning: Invalid url will return error.
/// - Parameter url: The call center url copied from link or scanned from QR Code
/// - Parameter userId: Custom id for any purpose
/// - Parameter email: customer's email address
/// - Parameter phone: customer's phone number
/// - Parameter claimCode: used to claim old chat history
/// - Returns: Check for error in completion block if any
/// # Sample info  #
///
/// ```     Success
///     [
///
///     ]
/// ```
///
/// ```     Error
///     [
///         "error" : 20100,
///         "errorMessage" : "call center not exist"
///     ]
/// ```
/// ```
///     [
///         "error" : 20101,
///         "errorMessage" : "call center not allow to call",
///     ]
/// ```
/// ```
///     [
///         "error" : 20102,
///         "errorMessage" : "call center not allow to chat",
///     ]
/// ```
+ (void) makeCallCallCenterWith:(NSString *_Nonnull)url
                         UserID:(NSString *_Nullable)userId
                           Name:(NSString *_Nullable)name
                          Email:(NSString *_Nullable)email
                          Phone:(NSString *_Nullable)phone
                      ClaimCode:(NSString *_Nullable)claimCode
                     completion:(void(^)(NSDictionary * _Nullable result))resultBlock;

/// Make chat to call center with given `url`.
///
/// - Warning: Invalid url will return error.
/// - Parameter url: The call center url copied from link or scanned from QR Code
/// - Parameter userId: Custom id for any purpose
/// - Parameter name: Name of caller
/// - Parameter email: customer's email address
/// - Parameter phone: customer's phone number
/// - Parameter claimCode: used to claim old chat history
/// - Returns: Check for error in completion block if any
/// # Sample info  #
///
/// ```     Success
///     [
///
///     ]
/// ```
///
/// ```     Error
///     [
///         "error" : 20100,
///         "errorMessage" : "call center not exist"
///     ]
/// ```
/// ```
///     [
///         "error" : 20101,
///         "errorMessage" : "call center not allow to call",
///     ]
/// ```
/// ```
///     [
///         "error" : 20102,
///         "errerrorMessageor" : "call center not allow to chat",
///     ]
/// ```
+ (void) makeChatCallCenterWith:(NSString *_Nonnull)url
                         UserID:(NSString *_Nullable)userId
                           Name:(NSString *_Nullable)name
                          Email:(NSString *_Nullable)email
                          Phone:(NSString *_Nullable)phone
                      ClaimCode:(NSString *_Nullable)claimCode
                     completion:(void(^)(NSDictionary * _Nullable result))resultBlock;

/// Check if give 'str' is a valid email.
///
/// - Parameter str: The string to check
/// - Returns: YES if 'str' is valid email, NO if not.
+ (BOOL)isValidEmail:(NSString *)str;

/// Check if give 'str' is a valid phone number.
///
/// - Description: A valid phone number is begin with '0' and followed by digits in range [0-9]. Or begin with '+' and followed by digits in range [0-9].
///
/// - Parameter str: The string to check
/// - Returns: YES if 'str' is valid phone number, NO if not.
+ (BOOL)isValidPhoneNumber:(NSString *)str;

/// Check if give 'str' is a valid call center url.
///
/// - Parameter str: The string to check
/// - Returns: alias of call center if 'str' is valid call center url, NULL if not.
+ (NSString *)isValidCallCenterUrl:(NSString *)str;

@end

@protocol CallCenterSDKProtocol <NSObject>

- (void)callCenterSDK_didMakeCallWithInfo:(NSDictionary<NSString*,id> *_Nonnull)info;

- (void)callCenterSDK_didMakeChatWithInfo:(NSDictionary<NSString*,id> *_Nonnull)info;

@end

NS_ASSUME_NONNULL_END
