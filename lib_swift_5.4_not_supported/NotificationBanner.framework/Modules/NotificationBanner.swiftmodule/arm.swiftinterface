// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.4 (swiftlang-1205.0.26.9 clang-1205.0.19.55)
// swift-module-flags: -target armv7-apple-ios10.0 -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -Onone -module-name NotificationBanner
import Foundation
import MarqueeLabel
@_exported import NotificationBanner
import SnapKit
import Swift
import UIKit
extension UIWindow {
  public var winWidth: CoreGraphics.CGFloat {
    get
  }
  public var winHeight: CoreGraphics.CGFloat {
    get
  }
}
@objc @_inheritsConvenienceInitializers @_hasMissingDesignatedInitializers @objcMembers open class StatusBarNotificationBanner : NotificationBanner.BaseNotificationBanner {
  @objc override public var bannerHeight: CoreGraphics.CGFloat {
    @objc get
    @objc set(newValue)
  }
  @objc public convenience init(title: Swift.String, style: NotificationBanner.BannerStyle = .info, colors: NotificationBanner.BannerColorsProtocol? = nil)
  @objc public convenience init(attributedTitle: Foundation.NSAttributedString, style: NotificationBanner.BannerStyle = .info, colors: NotificationBanner.BannerColorsProtocol? = nil)
  @objc public init(customView: UIKit.UIView)
  @objc required dynamic public init?(coder aDecoder: Foundation.NSCoder)
  @objc deinit
}
extension StatusBarNotificationBanner {
  public func applyStyling(titleColor: UIKit.UIColor? = nil, titleTextAlign: UIKit.NSTextAlignment? = nil)
}
@objc @_hasMissingDesignatedInitializers @objcMembers open class NotificationBanner : NotificationBanner.BaseNotificationBanner {
  @objc public var subtitleLabel: MarqueeLabel.MarqueeLabel? {
    get
  }
  @objc public init(title: Swift.String? = nil, subtitle: Swift.String? = nil, leftView: UIKit.UIView? = nil, rightView: UIKit.UIView? = nil, style: NotificationBanner.BannerStyle = .info, colors: NotificationBanner.BannerColorsProtocol? = nil)
  @objc public convenience init(attributedTitle: Foundation.NSAttributedString, attributedSubtitle: Foundation.NSAttributedString? = nil, leftView: UIKit.UIView? = nil, rightView: UIKit.UIView? = nil, style: NotificationBanner.BannerStyle = .info, colors: NotificationBanner.BannerColorsProtocol? = nil)
  @objc public init(customView: UIKit.UIView)
  @objc required dynamic public init?(coder aDecoder: Foundation.NSCoder)
  @objc deinit
}
extension NotificationBanner {
  public func applyStyling(cornerRadius: CoreGraphics.CGFloat? = nil, titleFont: UIKit.UIFont? = nil, titleColor: UIKit.UIColor? = nil, titleTextAlign: UIKit.NSTextAlignment? = nil, subtitleFont: UIKit.UIFont? = nil, subtitleColor: UIKit.UIColor? = nil, subtitleTextAlign: UIKit.NSTextAlignment? = nil)
}
extension String {
  public func height(forConstrainedWidth width: CoreGraphics.CGFloat, font: UIKit.UIFont) -> CoreGraphics.CGFloat
}
@objc public enum BannerPosition : Swift.Int {
  case bottom
  case top
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
@objc open class FloatingNotificationBanner : NotificationBanner.GrowingNotificationBanner {
  public init(title: Swift.String? = nil, subtitle: Swift.String? = nil, titleFont: UIKit.UIFont? = nil, titleColor: UIKit.UIColor? = nil, titleTextAlign: UIKit.NSTextAlignment? = nil, subtitleFont: UIKit.UIFont? = nil, subtitleColor: UIKit.UIColor? = nil, subtitleTextAlign: UIKit.NSTextAlignment? = nil, leftView: UIKit.UIView? = nil, rightView: UIKit.UIView? = nil, style: NotificationBanner.BannerStyle = .info, colors: NotificationBanner.BannerColorsProtocol? = nil, iconPosition: NotificationBanner.GrowingNotificationBanner.IconPosition = .center)
  @objc public init(customView: UIKit.UIView)
  public func show(queuePosition: NotificationBanner.QueuePosition = .back, bannerPosition: NotificationBanner.BannerPosition = .top, queue: NotificationBanner.NotificationBannerQueue = NotificationBannerQueue.default, on viewController: UIKit.UIViewController? = nil, edgeInsets: UIKit.UIEdgeInsets = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8), cornerRadius: CoreGraphics.CGFloat? = nil, shadowColor: UIKit.UIColor = .black, shadowOpacity: CoreGraphics.CGFloat = 1, shadowBlurRadius: CoreGraphics.CGFloat = 0, shadowCornerRadius: CoreGraphics.CGFloat = 0, shadowOffset: UIKit.UIOffset = .zero, shadowEdgeInsets: UIKit.UIEdgeInsets? = nil)
  @objc required dynamic public init?(coder aDecoder: Foundation.NSCoder)
  override public init(title: Swift.String? = super, subtitle: Swift.String? = super, leftView: UIKit.UIView? = super, rightView: UIKit.UIView? = super, style: NotificationBanner.BannerStyle = super, colors: NotificationBanner.BannerColorsProtocol? = super, iconPosition: NotificationBanner.GrowingNotificationBanner.IconPosition = super, sideViewSize: CoreGraphics.CGFloat = super)
  @objc deinit
}
@objc public protocol BannerColorsProtocol {
  @objc func color(for style: NotificationBanner.BannerStyle) -> UIKit.UIColor
}
@_hasMissingDesignatedInitializers public class BannerColors : NotificationBanner.BannerColorsProtocol {
  @objc public func color(for style: NotificationBanner.BannerStyle) -> UIKit.UIColor
  @objc deinit
}
@objc @_hasMissingDesignatedInitializers open class GrowingNotificationBanner : NotificationBanner.BaseNotificationBanner {
  public enum IconPosition {
    case top
    case center
    public static func == (a: NotificationBanner.GrowingNotificationBanner.IconPosition, b: NotificationBanner.GrowingNotificationBanner.IconPosition) -> Swift.Bool
    public func hash(into hasher: inout Swift.Hasher)
    public var hashValue: Swift.Int {
      get
    }
  }
  @objc override public var bannerHeight: CoreGraphics.CGFloat {
    @objc get
    @objc set(newValue)
  }
  @objc public var subtitleLabel: UIKit.UILabel? {
    get
  }
  public init(title: Swift.String? = nil, subtitle: Swift.String? = nil, leftView: UIKit.UIView? = nil, rightView: UIKit.UIView? = nil, style: NotificationBanner.BannerStyle = .info, colors: NotificationBanner.BannerColorsProtocol? = nil, iconPosition: NotificationBanner.GrowingNotificationBanner.IconPosition = .center, sideViewSize: CoreGraphics.CGFloat = 24.0)
  @objc required dynamic public init?(coder aDecoder: Foundation.NSCoder)
  @objc deinit
}
extension GrowingNotificationBanner {
  public func applyStyling(cornerRadius: CoreGraphics.CGFloat? = nil, titleFont: UIKit.UIFont? = nil, titleColor: UIKit.UIColor? = nil, titleTextAlign: UIKit.NSTextAlignment? = nil, subtitleFont: UIKit.UIFont? = nil, subtitleColor: UIKit.UIColor? = nil, subtitleTextAlign: UIKit.NSTextAlignment? = nil)
}
@objc public enum QueuePosition : Swift.Int {
  case back
  case front
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
@objc @objcMembers open class NotificationBannerQueue : ObjectiveC.NSObject {
  @objc public static var `default`: NotificationBanner.NotificationBannerQueue
  @objc public var numberOfBanners: Swift.Int {
    @objc get
  }
  @objc public init(maxBannersOnScreenSimultaneously: Swift.Int = 1)
  @objc public func removeAll()
  @objc public func dismissAllForced()
  @objc override dynamic public init()
  @objc deinit
}
public enum BannerHaptic {
  case light
  case medium
  case heavy
  case none
  public static func == (a: NotificationBanner.BannerHaptic, b: NotificationBanner.BannerHaptic) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
@objc @_inheritsConvenienceInitializers open class BannerHapticGenerator : ObjectiveC.NSObject {
  open class func generate(_ haptic: NotificationBanner.BannerHaptic)
  @objc override dynamic public init()
  @objc deinit
}
@objc public enum BannerStyle : Swift.Int {
  case danger
  case info
  case customView
  case success
  case warning
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
public protocol NotificationBannerDelegate : AnyObject {
  func notificationBannerWillAppear(_ banner: NotificationBanner.BaseNotificationBanner)
  func notificationBannerDidAppear(_ banner: NotificationBanner.BaseNotificationBanner)
  func notificationBannerWillDisappear(_ banner: NotificationBanner.BaseNotificationBanner)
  func notificationBannerDidDisappear(_ banner: NotificationBanner.BaseNotificationBanner)
}
@objc @_hasMissingDesignatedInitializers @objcMembers open class BaseNotificationBanner : UIKit.UIView {
  @objc public static var BannerWillAppear: Foundation.Notification.Name
  @objc public static var BannerDidAppear: Foundation.Notification.Name
  @objc public static var BannerWillDisappear: Foundation.Notification.Name
  @objc public static var BannerDidDisappear: Foundation.Notification.Name
  @objc public static var BannerObjectKey: Swift.String
  weak public var delegate: NotificationBanner.NotificationBannerDelegate?
  @objc final public let style: NotificationBanner.BannerStyle
  @objc public var bannerHeight: CoreGraphics.CGFloat {
    @objc get
    @objc set(newValue)
  }
  @objc public var titleLabel: UIKit.UILabel? {
    get
  }
  @objc public var duration: Swift.Double {
    @objc get
    @objc set(value)
  }
  @objc public var autoDismiss: Swift.Bool {
    @objc get
    @objc set(value)
  }
  @objc public var transparency: CoreGraphics.CGFloat {
    @objc get
    @objc set(value)
  }
  public var haptic: NotificationBanner.BannerHaptic
  @objc public var dismissOnTap: Swift.Bool
  @objc public var dismissOnSwipeUp: Swift.Bool
  @objc public var onTap: (() -> Swift.Void)?
  @objc public var onSwipeUp: (() -> Swift.Void)?
  @objc public var bannerQueue: NotificationBanner.NotificationBannerQueue
  @objc public var animationDuration: Swift.Double
  @objc public var isDisplaying: Swift.Bool
  @objc override dynamic open var backgroundColor: UIKit.UIColor? {
    @objc get
    @objc set(newValue)
  }
  @objc required dynamic public init?(coder aDecoder: Foundation.NSCoder)
  @objc deinit
  @objc public func show(queuePosition: NotificationBanner.QueuePosition = .back, bannerPosition: NotificationBanner.BannerPosition = .top, queue: NotificationBanner.NotificationBannerQueue = NotificationBannerQueue.default, on viewController: UIKit.UIViewController? = nil)
  @objc public func resetDuration()
  @objc public func dismiss(forced: Swift.Bool = false)
  @objc public func remove()
  @objc override dynamic public init(frame: CoreGraphics.CGRect)
}
