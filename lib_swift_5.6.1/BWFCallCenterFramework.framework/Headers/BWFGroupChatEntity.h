//
//  BWFGroupChatEntity.h
//  BWFCallCenterFramework
//
//  Created by Thai Tran on 11/3/20.
//  Copyright © 2020 Kryptono LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface BWFGroupChatEntity : NSObject <NSCoding>

@property(nonatomic, strong) NSString* companyId;

@property(nonatomic, strong) NSString* lastSeenMessageId;
@property(nonatomic, strong) NSDate* lastSeenDate;
@property(nonatomic, strong) NSString* groupId;
@property(nonatomic, strong) NSString* groupName;
@property(nonatomic, strong) NSString* qrCodeAlias;

@property(nonatomic, strong) NSString* _Nullable callRequestId;


@end

NS_ASSUME_NONNULL_END
