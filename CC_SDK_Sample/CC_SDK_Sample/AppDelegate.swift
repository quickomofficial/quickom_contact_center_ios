//
//  AppDelegate.swift
//  CC_SDK_Sample
//
//  Created by Kinh Tran on 26/8/21.
//

import Foundation
import UIKit
import PushKit
import CCSDK

let kDeviceTokenVOIP = "kDeviceTokenVOIP"
let kDeviceTokenNormal = "kDeviceTokenNormal"

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window:UIWindow?
    var pushRegistry : PKPushRegistry? = nil

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.rootViewController = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController()
        self.window?.makeKeyAndVisible()
        
        // register for VOIP Push
        pushRegistry = PKPushRegistry(queue: DispatchQueue.main)
        pushRegistry?.delegate = self
        pushRegistry?.desiredPushTypes = [.voIP]
        
        return true
    }

    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return .portrait
    }
}

extension AppDelegate : PKPushRegistryDelegate {
    func pushRegistry(_ registry: PKPushRegistry, didUpdate pushCredentials: PKPushCredentials, for type: PKPushType) {
        
        if pushCredentials.token.count == 0 {
            NSLog("Voip push token is nil")
            return
        }
        
        let token = pushCredentials.token.map { String(format: "%02.2hhx", $0) }.joined()
        UserDefaults.standard.set(token, forKey: kDeviceTokenVOIP)
        
        let normalToken = UserDefaults.standard.string(forKey: kDeviceTokenNormal)
        
        NSLog("DevicePushTokenVOIP = %@" + token)
        
        if CallCenterSDK.isSdkLoggedIn() == true {
            CallCenterSDK.updatePushToken(withVoipToken: token, normalToken: normalToken, enterprise: "quickom_sdk_app") { result in
                
            }
        }
    }
    
    func pushRegistry(_ registry: PKPushRegistry, didReceiveIncomingPushWith payload: PKPushPayload, for type: PKPushType, completion: @escaping () -> Void) {
        
        if CallCenterSDK.isSdkLoggedIn() == false {
            return
        }
        
        let payloadDict = payload.dictionaryPayload
        let logStr = String.init(format: "Receive pushkit noti with content: %@", payloadDict)
        Macro.Log(logStr)
        NSLog("%@", logStr)
        var dataString = payloadDict["data"] as? String ?? ""
        if dataString.count == 0 {
            let aps = payloadDict["aps"] as? [String : Any] ?? [:]
            dataString = aps["alert"] as? String ?? ""
        }
        
        CallCenterSDK.handlePushkitPayload(dataString)
    }
}
