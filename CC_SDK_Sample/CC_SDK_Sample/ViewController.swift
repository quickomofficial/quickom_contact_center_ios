//
//  ViewController.swift
//  CC_SDK_Sample
//
//  Created by Kinh Tran on 26/8/21.
//

import UIKit
import CCSDK
import CountryPickerView

class ViewController: UIViewController {
    let cpv = CountryPickerView()
    
    
    // GUI
    let scrollView = UIScrollView()
    let bgButton = UIButton()
    
    let labelTitle = UILabel()
    
    let labelUserInfo = UILabel()
    let tfInputName = UITextField()
    let tfInputContact = UITextField()
    let tfInputId = UITextField()
    
    let labelInstruction = UILabel()
    let tfInputUrl = UITextField()
    
    let btnRequest = UIButton(type: .custom)
    let btnForceCall = UIButton(type: .custom)
    
    let labelCallCenter = UILabel()
//    let btnMakeCall = UIButton(type: .custom)
//    let btnMakeChat = UIButton(type: .custom)
    
    let labelLoginInfo = UILabel()
    let tfInputUsername = UITextField()
    let tfInputPassword = UITextField()
    let labelLoggedIn = UILabel()
    
    let btnLogin = UIButton(type: .custom)
    let btnLogout = UIButton(type: .custom)
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

        self.initViews()
        
        self.layoutViews()
        
        self.startCCSDK()
        
        self.btnRequest_tap()
        
        if (self.tfInputUsername.text?.count ?? 0) > 0 &&
            (self.tfInputPassword.text?.count ?? 0) > 0 {
            self.btnLogin_tap()
        }
        
        self.observeNotification(selector: #selector(keyboardWillShow(notification:)),
                                 name: UIResponder.keyboardWillShowNotification.rawValue,
                                 object: nil)
        self.observeNotification(selector: #selector(keyboardWillHide(notification:)),
                                 name: UIResponder.keyboardWillHideNotification.rawValue,
                                 object: nil)
    }
    
    private func initViews() {
        self.view.backgroundColor = .white
        
        self.scrollView.backgroundColor = .clear
        self.scrollView.showsVerticalScrollIndicator = false
        self.scrollView.showsHorizontalScrollIndicator = false
        self.view.addSubview(self.scrollView)
        
        self.bgButton.backgroundColor = .clear
        self.bgButton.addTarget(self, action: #selector(bgButton_tap), for: .touchUpInside)
        self.scrollView.addSubview(self.bgButton)
        
        self.labelTitle.backgroundColor = .clear
        self.labelTitle.text = "Call Center Framework Sample"
        self.labelTitle.textColor = .black
        self.labelTitle.textAlignment = .center
        self.labelTitle.font = UIFont.boldSystemFont(ofSize: 20)
        self.scrollView.addSubview(self.labelTitle)
        
        self.labelUserInfo.backgroundColor = .clear
        self.labelUserInfo.text = "Enter caller info"
        self.labelUserInfo.textColor = .black
        self.labelUserInfo.textAlignment = .left
        self.labelUserInfo.font = UIFont.systemFont(ofSize: 14)
        self.scrollView.addSubview(self.labelUserInfo)
        
        self.tfInputName.backgroundColor = .clear
        self.tfInputName.placeholder = "Caller name"
        self.tfInputName.changePlaceholderColor(UIColor.gray)
        self.tfInputName.text = "Landlord"
        self.tfInputName.textColor = .blue
        self.tfInputName.textAlignment = .left
        self.tfInputName.layer.cornerRadius = 8.0
        self.tfInputName.layer.borderWidth = 0.5
        self.tfInputName.layer.borderColor = UIColor.blue.cgColor
        self.tfInputName.delegate = self
        self.tfInputName.leftViewMode = .always
        self.tfInputName.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 10))
        self.tfInputName.rightViewMode = .always
        self.tfInputName.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 10))
        self.scrollView.addSubview(self.tfInputName)
        
        self.tfInputContact.backgroundColor = .clear
        self.tfInputContact.placeholder = "Caller contact (email/phone)"
        self.tfInputContact.changePlaceholderColor(UIColor.gray)
        self.tfInputContact.text = "0123456789"
        self.tfInputContact.textColor = .blue
        self.tfInputContact.textAlignment = .left
        self.tfInputContact.layer.cornerRadius = 8.0
        self.tfInputContact.layer.borderWidth = 0.5
        self.tfInputContact.layer.borderColor = UIColor.blue.cgColor
        self.tfInputContact.delegate = self
        self.tfInputContact.leftViewMode = .always
        self.tfInputContact.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 10))
        self.tfInputContact.rightViewMode = .always
        self.tfInputContact.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 10))
        self.scrollView.addSubview(self.tfInputContact)
        
        self.tfInputId.backgroundColor = .clear
        self.tfInputId.placeholder = "Caller custom identifier"
        self.tfInputId.changePlaceholderColor(UIColor.gray)
        self.tfInputId.text = "1223334444"
        self.tfInputId.textColor = .blue
        self.tfInputId.textAlignment = .left
        self.tfInputId.layer.cornerRadius = 8.0
        self.tfInputId.layer.borderWidth = 0.5
        self.tfInputId.layer.borderColor = UIColor.blue.cgColor
        self.tfInputId.delegate = self
        self.tfInputId.leftViewMode = .always
        self.tfInputId.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 10))
        self.tfInputId.rightViewMode = .always
        self.tfInputId.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 10))
        self.scrollView.addSubview(self.tfInputId)
        
        self.labelInstruction.backgroundColor = .clear
        self.labelInstruction.text = "Enter call center url"
        self.labelInstruction.textColor = .black
        self.labelInstruction.textAlignment = .left
        self.labelInstruction.font = UIFont.systemFont(ofSize: 14)
        self.scrollView.addSubview(self.labelInstruction)
        
        self.tfInputUrl.backgroundColor = .clear
        self.tfInputUrl.placeholder = "Call center url"
        self.tfInputUrl.changePlaceholderColor(UIColor.gray)
        self.tfInputUrl.text = ""
        self.tfInputUrl.textColor = .blue
        self.tfInputUrl.textAlignment = .left
        self.tfInputUrl.layer.cornerRadius = 8.0
        self.tfInputUrl.layer.borderWidth = 0.5
        self.tfInputUrl.layer.borderColor = UIColor.blue.cgColor
        self.tfInputUrl.delegate = self
        self.tfInputUrl.leftViewMode = .always
        self.tfInputUrl.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 10))
        self.tfInputUrl.rightViewMode = .always
        self.tfInputUrl.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 10))
        self.tfInputUrl.text = "https://cc.quickom.com/call/35311658829067124"
//        self.tfInputUrl.text = "https://cc.quickom.com/call/12901652079488758"
//        self.tfInputUrl.text = "https://cc.quickom.com/call/18101657855248850"
        self.scrollView.addSubview(self.tfInputUrl)
        
        self.labelCallCenter.backgroundColor = .clear
        self.labelCallCenter.text = ""
        self.labelCallCenter.textColor = .purple
        self.labelCallCenter.textAlignment = .left
        self.labelCallCenter.font = UIFont.boldSystemFont(ofSize: 16)
        self.scrollView.addSubview(self.labelCallCenter)
        
        self.btnRequest.backgroundColor = .white
        self.btnRequest.setTitle("Request", for: .normal)
        self.btnRequest.setTitleColor(.black, for: .normal)
        self.btnRequest.layer.masksToBounds = true
        self.btnRequest.layer.borderWidth = 0.5
        self.btnRequest.layer.borderColor = UIColor.black.cgColor
        self.btnRequest.addTarget(self, action: #selector(btnRequest_tap), for: .touchUpInside)
        self.scrollView.addSubview(self.btnRequest)
        
        self.btnForceCall.backgroundColor = .red
        self.btnForceCall.setTitle("Force Call", for: .normal)
        self.btnForceCall.setTitleColor(.white, for: .normal)
        self.btnForceCall.layer.masksToBounds = true
        self.btnForceCall.addTarget(self, action: #selector(btnForceCall_tap), for: .touchUpInside)
        self.scrollView.addSubview(self.btnForceCall)
        
        
        self.labelLoginInfo.backgroundColor = .clear
        self.labelLoginInfo.text = "Enter login info"
        self.labelLoginInfo.textColor = .black
        self.labelLoginInfo.textAlignment = .left
        self.labelLoginInfo.font = UIFont.systemFont(ofSize: 14)
        self.scrollView.addSubview(self.labelLoginInfo)
        
        self.tfInputUsername.backgroundColor = .clear
        self.tfInputUsername.placeholder = "Username/Email"
        self.tfInputUsername.changePlaceholderColor(UIColor.gray)
        if let savedUsername = UserDefaults.standard.string(forKey: "SAVED_USERNAME") {
            self.tfInputUsername.text = savedUsername
        }
        self.tfInputUsername.textColor = .blue
        self.tfInputUsername.textAlignment = .left
        self.tfInputUsername.layer.cornerRadius = 8.0
        self.tfInputUsername.layer.borderWidth = 0.5
        self.tfInputUsername.layer.borderColor = UIColor.blue.cgColor
        self.tfInputUsername.delegate = self
        self.tfInputUsername.leftViewMode = .always
        self.tfInputUsername.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 10))
        self.tfInputUsername.rightViewMode = .always
        self.tfInputUsername.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 10))
        self.scrollView.addSubview(self.tfInputUsername)
        
        self.tfInputPassword.backgroundColor = .clear
        self.tfInputPassword.placeholder = "Password"
        self.tfInputPassword.changePlaceholderColor(UIColor.gray)
        if let savedPassword = UserDefaults.standard.string(forKey: "SAVED_PASSWORD") {
            self.tfInputPassword.text = savedPassword
        }
        self.tfInputPassword.textColor = .blue
        self.tfInputPassword.textAlignment = .left
        self.tfInputPassword.isSecureTextEntry = true
        self.tfInputPassword.layer.cornerRadius = 8.0
        self.tfInputPassword.layer.borderWidth = 0.5
        self.tfInputPassword.layer.borderColor = UIColor.blue.cgColor
        self.tfInputPassword.delegate = self
        self.tfInputPassword.leftViewMode = .always
        self.tfInputPassword.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 10))
        self.tfInputPassword.rightViewMode = .always
        self.tfInputPassword.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 10))
        self.scrollView.addSubview(self.tfInputPassword)
        
        self.labelLoggedIn.backgroundColor = .clear
        self.labelLoggedIn.text = "Logged in"
        self.labelLoggedIn.textColor = .green
        self.labelLoggedIn.textAlignment = .left
        self.labelLoggedIn.font = UIFont.boldSystemFont(ofSize: 14)
        self.scrollView.addSubview(self.labelLoggedIn)
        self.labelLoggedIn.isHidden = true
        
        self.btnLogin.backgroundColor = .blue
        self.btnLogin.setTitle("Login", for: .normal)
        self.btnLogin.setTitleColor(.white, for: .normal)
        self.btnLogin.layer.masksToBounds = true
        self.btnLogin.addTarget(self, action: #selector(btnLogin_tap), for: .touchUpInside)
        self.scrollView.addSubview(self.btnLogin)
        
        self.btnLogout.backgroundColor = .white
        self.btnLogout.setTitle("Logout", for: .normal)
        self.btnLogout.setTitleColor(.red, for: .normal)
        self.btnLogout.layer.masksToBounds = true
        self.btnLogout.layer.borderWidth = 0.5
        self.btnLogout.layer.borderColor = UIColor.red.cgColor
        self.btnLogout.addTarget(self, action: #selector(btnLogout_tap), for: .touchUpInside)
        self.scrollView.addSubview(self.btnLogout)

    }

    private func layoutViews() {
        self.scrollView.frame = CGRect(x: 0, y: 0, width: self.view.width, height: self.view.height)
        self.bgButton.frame = CGRect(x: 0, y: 0, width: self.view.width, height: self.view.height)
        self.labelTitle.frame = CGRect(x: 20, y: 50, width: self.view.width - 2*20, height: 60)
        
        self.labelUserInfo.frame = CGRect(x: 20, y: self.labelTitle.bottom + 20, width: self.view.width - 2*20, height: 30)
        self.tfInputName.frame = CGRect(x: 20, y: self.labelUserInfo.bottom, width: self.view.width - 2*20, height: 40)
        self.tfInputContact.frame = CGRect(x: 20, y: self.tfInputName.bottom + 10, width: self.view.width - 2*20, height: 40)
        self.tfInputId.frame = CGRect(x: 20, y: self.tfInputContact.bottom + 10, width: self.view.width - 2*20, height: 40)
        
        self.labelInstruction.frame = CGRect(x: 20, y: self.tfInputId.bottom + 20, width: self.view.width - 2*20, height: 30)
        self.tfInputUrl.frame = CGRect(x: 20, y: self.labelInstruction.bottom, width: self.view.width - 2*20, height: 40)
        
        self.labelCallCenter.frame = CGRect(x: 20, y: self.tfInputUrl.bottom + 10, width: self.view.width - 2*20, height: 40)
        
        let buttonWidth = CGFloat(140)
        self.btnRequest.frame = CGRect(x: self.view.width/2 - 20 - buttonWidth, y: self.labelCallCenter.bottom + 20, width: buttonWidth, height: 40)
        self.btnForceCall.frame = CGRect(x: self.view.width/2 + 20, y: self.labelCallCenter.bottom + 20, width: buttonWidth, height: 40)
        
        self.btnRequest.layer.cornerRadius = self.btnRequest.height/2
        self.btnForceCall.layer.cornerRadius = self.btnForceCall.height/2
        
        self.labelLoginInfo.frame = CGRect(x: 20, y: self.btnForceCall.bottom + 20, width: self.view.width - 2*20, height: 30)
        self.tfInputUsername.frame = CGRect(x: 20, y: self.labelLoginInfo.bottom, width: self.view.width - 2*20, height: 40)
        self.tfInputPassword.frame = CGRect(x: 20, y: self.tfInputUsername.bottom + 10, width: self.view.width - 2*20, height: 40)
        
        self.labelLoggedIn.frame = CGRect(x: 20, y: self.tfInputPassword.bottom + 10, width: 100, height: 40)
        
        self.btnLogin.frame = CGRect(x: self.view.width/2 - 20 - buttonWidth, y: self.labelLoggedIn.bottom + 10, width: buttonWidth, height: 40)
        self.btnLogout.frame = CGRect(x: self.view.width/2 + 20, y: self.labelLoggedIn.bottom + 10, width: buttonWidth, height: 40)
        
        self.btnLogin.layer.cornerRadius = self.btnLogin.height/2
        self.btnLogout.layer.cornerRadius = self.btnLogout.height/2
        
        
//        self.btnMakeCall.frame = CGRect(x: self.view.width/2 - 20 - buttonWidth, y: self.labelCallCenter.bottom + 20, width: buttonWidth, height: 40)
//        self.btnMakeChat.frame = CGRect(x: self.view.width/2 + 20, y: self.labelCallCenter.bottom + 20, width: buttonWidth, height: 40)
        
        
        
//        self.btnMakeCall.layer.cornerRadius = self.btnMakeCall.height/2
//        self.btnMakeChat.layer.cornerRadius = self.btnMakeChat.height/2
        
        
        self.scrollView.contentSize = CGSize(width: self.view.width, height: self.btnLogin.bottom + 10)
    }
    
    
}

extension ViewController : UITextFieldDelegate {
    @objc func keyboardWillShow(notification : Notification) {
        if let userInfo = notification.userInfo {
            let keyboardHeight = (userInfo[Constant.NotificationNameKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.height
            
            let dh = keyboardHeight
            var xBottom = CGFloat(0)
            if Macro.hasNotch {
                xBottom = CGFloat(30)
            }
            var f = self.scrollView.frame
            f.size.height = self.view.height - dh
            self.scrollView.frame = f
        }
    }
    
    @objc func keyboardWillHide(notification : Notification) {
        var f =  self.scrollView.frame
        f.size.height = self.view.height
        self.scrollView.frame = f
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == self.tfInputUrl {
            self.labelCallCenter.text = ""
//            self.btnMakeCall.isHidden = true
//            self.btnMakeChat.isHidden = true
        }
        
        return true
    }
    
    @objc private func bgButton_tap() {
        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
}

extension ViewController {
    @objc private func startCCSDK() {
        CallCenterSDK.start()
        CallCenterSDK.setCCSDKDelegate(self)
        CallCenterSDK.setDesiredLanguage("vi")
    }
    
    @objc private func btnRequest_tap() {
        if let validUrl = tfInputUrl.text, validUrl.count > 0 {
            CallCenterSDK.requestCallCenterInfo(from: validUrl) {[weak self] result in
                guard let `self` = self,
                      let info = result as? [String : Any]
                else { return }
                
                if let errorMessage = info["errorMessage"] as? String {
//                    self.btnMakeCall.isHidden = true
//                    self.btnMakeChat.isHidden = true
                    DispatchQueue.main.async {
                        self.showAlert(errorMessage)
                    }
                }
                
                if let callCenterName = info["name"] as? String {
                    self.labelCallCenter.text = "Call Center: " + callCenterName
                }
//                let allowCall = (info["allowCall"] as? Bool) ?? false
//                let allowChat = (info["allowChat"] as? Bool) ?? false
                
//                self.btnMakeCall.isHidden = !allowCall
//                self.btnMakeChat.isHidden = !allowChat
            }
        }
        else {
            DispatchQueue.main.async {
                self.showAlert("Please enter call center link")
            }
        }
    }
    
    @objc private func btnForceCall_tap() {
        var code : String? = nil
        if let validUrl = tfInputUrl.text, validUrl.count > 0 {
            let alias = CallCenterSDK.isValidCallCenterUrl(validUrl)
            let userId = self.tfInputId.text
            var email : String? = nil
            var phone : String? = nil
            if let contact = self.tfInputContact.text {
                if CallCenterSDK.isValidEmail(contact) {
                    email = contact
                }
                if CallCenterSDK.isValidPhoneNumber(contact) {
                    phone = contact
                }
            }
            
            var claimInfo = ["alias":alias]
            if userId != nil { claimInfo["userId"] = userId }
            if email != nil { claimInfo["email"] = email }
            if phone != nil { claimInfo["phoneNumber"] = phone }
            code = self.getClaimCode(With: claimInfo)
            
            CallCenterSDK.startCallCallCenter(with: validUrl, userID: self.tfInputId.text, name: self.tfInputName.text, email: email, phone: phone, claimCode: code, callOption: NORMAL) { [weak self] result in
                guard let `self` = self,
                      let info = result as? [String : Any]
                else { return }
                if let errorMessage = info["errorMessage"] as? String, errorMessage.count > 0 {
                    DispatchQueue.main.async {
                        self.showAlert(errorMessage)
                    }
                }
            }
//            CallCenterSDK.makeCallCallCenter(with: validUrl, userID: self.tfInputId.text, name: self.tfInputName.text, email: email, phone: phone, claimCode: code) { [weak self] result in
//                guard let `self` = self,
//                      let info = result as? [String : Any]
//                else { return }
//                if let errorMessage = info["errorMessage"] as? String, errorMessage.count > 0 {
//                    DispatchQueue.main.async {
//                        self.showAlert(errorMessage)
//                    }
//                }
//            }
        }
        else {
            DispatchQueue.main.async {
                self.showAlert("Please enter call center link")
            }
        }
    }
    
    @objc private func btnLogin_tap() {
        if let validUsername = self.tfInputUsername.text, validUsername.count > 0,
           let validPassword = self.tfInputPassword.text, validPassword.count > 0 {
            CallCenterSDK.loginSDK(withUser: validUsername, password: validPassword) { [weak self] result in
                guard let `self` = self
                else { return }
                let info = result as? [String : Any]
                if let errorMessage = info?["errorMessage"] as? String, errorMessage.count > 0 {
                    DispatchQueue.main.async {
                        self.labelLoggedIn.isHidden = true
                        self.showAlert(errorMessage)
                    }
                }
                else {
                    let token = UserDefaults.standard.string(forKey: kDeviceTokenVOIP)
                    let normalToken = UserDefaults.standard.string(forKey: kDeviceTokenNormal)
                    
                    CallCenterSDK.updatePushToken(withVoipToken: token, normalToken: normalToken, enterprise: "quickom_sdk_app") { result in
                        NSLog("updatePushToken with result = %@", result ?? [:])
                    }
                    DispatchQueue.main.async {
                        self.labelLoggedIn.isHidden = false
                        UserDefaults.standard.set(validUsername as AnyObject, forKey: "SAVED_USERNAME")
                        UserDefaults.standard.set(validPassword as AnyObject, forKey: "SAVED_PASSWORD")
                    }
                }
            }
        }
        else {
            DispatchQueue.main.async {
                self.showAlert("Please enter username/email and password")
            }
        }
    }
    
    @objc private func btnLogout_tap()  {
        CallCenterSDK.logoutSDK()
        self.tfInputUsername.text = ""
        self.tfInputPassword.text = ""
        self.labelLoggedIn.isHidden = true
        UserDefaults.standard.removeObject(forKey: "SAVED_USERNAME")
        UserDefaults.standard.removeObject(forKey: "SAVED_PASSWORD")
    }
    
    private func showAlert(_ str : String) {
        // create the alert
        let alert = UIAlertController(title: "Message", message: str, preferredStyle: UIAlertController.Style.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
}

extension ViewController : CallCenterSDKProtocol {
    func callCenterSDK_didMakeCall(withInfo info: [String : Any]) {
        NSLog("callCenterSDK_didMakeCall - with info = %@", info)
        if let code = info["code"] as? String, code.count > 0 {
            self.saveClaimCode(code, With: info)
        }
    }
    
    func callCenterSDK_didMakeChat(withInfo info: [String : Any]) {
        NSLog("callCenterSDK_didMakeChat - with info = %@", info)
        if let code = info["code"] as? String, code.count > 0 {
            self.saveClaimCode(code, With: info)
        }
    }
    
    func saveClaimCode(_ code:String, With info:[String:Any]) {
        var key : String? = nil
        guard let alias = info["alias"] as? String, alias.count > 0
        else { return }
        // PhoneNumber may contain prefix +84 or 0 so trim that first
        if let phoneNumber = info["phoneNumber"] as? String, phoneNumber.count > 0 {
            var fixPhoneNumber = phoneNumber
            fixPhoneNumber = fixPhoneNumber.headingTrim(CharacterSet(charactersIn: "0"))
            fixPhoneNumber = fixPhoneNumber.replacingOccurrences(of: "+84", with: "")
            key = alias + "_" + fixPhoneNumber
        } else if let email = info["email"] as? String, email.count > 0 {
            key = alias + "_" + email.lowercased()
        } else if let userId = info["userId"] as? String, userId.count > 0 {
            key = alias + "_" + userId.lowercased()
        }
        if let validKey = key {
            UserDefaults.standard.setValue(code, forKey: validKey)
        }
    }
    
    func getClaimCode(With info:[String : Any]) -> String? {
        var key : String? = nil
        guard let alias = info["alias"] as? String, alias.count > 0
        else { return nil }
        // PhoneNumber may contain prefix +84 or 0 so trim that first
        if let phoneNumber = info["phoneNumber"] as? String, phoneNumber.count > 0 {
            var fixPhoneNumber = phoneNumber
            fixPhoneNumber = fixPhoneNumber.headingTrim(CharacterSet(charactersIn: "0"))
            fixPhoneNumber = fixPhoneNumber.replacingOccurrences(of: "+84", with: "")
            key = alias + "_" + fixPhoneNumber
        } else if let email = info["email"] as? String, email.count > 0 {
            key = alias + "_" + email.lowercased()
        } else if let userId = info["userId"] as? String, userId.count > 0 {
            key = alias + "_" + userId.lowercased()
        }
        if let validKey = key {
            if let savedCode = UserDefaults.standard.value(forKey: validKey) as? String {
                return savedCode
            }
        }
        return nil
    }
}

extension UIView {
    var width : CGFloat {
        return self.frame.size.width
    }
    
    var height : CGFloat {
        return self.frame.size.height
    }
    
    var left : CGFloat {
        return self.frame.origin.x
    }
    
    var right : CGFloat {
        return self.frame.origin.x + self.frame.size.width
    }
    
    var top : CGFloat {
        return self.frame.origin.y
    }
    
    var bottom : CGFloat {
        return self.frame.origin.y + self.frame.size.height
    }
    
    var center : CGPoint {
        return CGPoint(x: self.frame.origin.x + self.frame.size.width/2, y: self.frame.origin.y + self.frame.size.height/2)
    }
}
